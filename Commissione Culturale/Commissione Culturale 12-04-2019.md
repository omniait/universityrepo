# Commissione Culturale 12-04-2019

## Ordine Del Giorno

1. [Comunicazioni](#Comunicazioni)
2. [Prossimo aperitivo](#Prossimo aperitivo)
3. [Banchini](#Banchini)
4. [Volantinaggio](#Volantinaggio)
5. [Giornalino](#Giornalino)

## Comunicazioni

Nessuna comunicazione particolare.

## Prossimo aperitivo

L’aperitivo passato ha avuto successo, al di là delle difficoltà avute riguardo il meteo, c’è stato un guadagno ed una buona presenza.

Alcuni sono andati via senza giocare, la jam session ha avuto successo anche senza gruppi esterni.

Si evidenzia un problema di pubblicizzazione, legati anche all’assenza dei banchini e al fatto di non aver fatto alcun giro nelle aule. 

Le regole dei giochi erano comunque sconosciute a gran parte dei presenti, rendendo difficile una partita “fluida”, inoltre la jam session e la musica alta hanno reso più difficoltoso il gioco.

Un altro problema è stato ovviamente il tempo (la pioggia, maledetto Zeus!) che ci ha privati del giardino.

Pertanto se riprendiamo il tema giochi dobbiamo:

* Creare una lista prima dei giochi, pubblicando le regole dei giochi prima dell’aperitivo
* Trovare una buona soluzione per la musica ed in generale l’aspetto del volume, le persone devono essere in grado di comunicare facilmente

Sicuramente si farà un altro aperitivo a tema giochi da tavolo/carte per sfruttare comunque il fatto che i giochi siano già pronti e l’esperienza, indicativamente come periodo pensiamo verso l’estate.

Riguardo il tema del prossimo aperitivo si evidenzia che è effettivamente difficile organizzare un aperitivo ecologico/green, sarebbe interessante lavorare sul multi-etnico portando quindi gastronomia internazionale ed in caso gastronomia di varie regioni d’Italia.

È sorta anche la proposta di organizzare un aperitivo con tema legato al vaporwave, sarebbe sicuramente particolare.

A questo punto rimangono in discussione i temi: **multi-etnico e vaporwave**.

Sul **giorno preciso** dell’aperitivo siamo d’accordo per il **23 Maggio 2019**, raggiunto dopo una discussione sia sul giorno della settimana sia sul periodo.

Per l’aspetto musica se si va sul multi-etnico Anna avrebbe anche una band da portare che è anche invogliata.

Simone conosce volendo altri gruppi musicali da Viareggio, quindi abbastanza vicini.

Problema del budget, abbiamo al massimo 50€ se riescono comunque a portare gente con trattazione “al ribasso”.

Invitiamo una discussione sui temi, sfruttando la mailing list e il coordinamento.

## Banchini

Questa settimana sono saltati, le disponibilità sul google doc sono a posto, Lamia ha evidenziato i turni che si potrebbero fare.

Si è discusso sugli orari, in particolare seppur alle 8:30 è stato raro fare banchini, rimane il momento migliore perché si riescono a prendere tutti gli studenti che entrano a lezione.

Il caffè è stato comprato ed è disponibile, la macchinetta del caffè funziona, che materiale si può esporre?

## Volantinaggio

Anna vuole ricominciare a far uscire volantini informativi periodicamente per dare visibilità ai problemi, come sono stati affrontati, come li stiamo affrontando da rappresentanti.

I volantini possono aiutare la comunicazione dalla lista agli studenti ma richiedono tempo sia di creazione, di stampa e di applicazione alle bacheche/porte.

Si potrebbe costruire un volantino sulla situazione delle aule, diventerebbe un buon atto di denuncia, inoltre si evidenzia che è un problema con soluzione protratta nel tempo per cui c’è per l’appunto il tempo materiale per prepararlo, inoltre possiamo sfruttare la pausa di Pasqua.

Portiamo questa idea in coordinamento, vediamo a chi affidare il compito e se costituire una commissione ad-hoc.

Si mostrano anche dei problemi chiari sulla comunicazione tramite Facebook o Instagram (in generale i Social Network), da una parte manca chi scrive post ma anche gente che guardi questi eventuali post.

Questo fa nascere un problema che ci riguarda direttamente: può sembrare che noi come rappresentanti non stiamo lavorando, ciò rovina la nostra immagine e reputazione come lista, sono giunte testimonianze negative riguardo al nostro “non-operato”.

Visti ed evidenziati i problemi sopra parliamo seriamente in coordinamento a riguardo della comunicazione per attuare delle soluzioni al più presto.

## Giornalino

Manca il tempo per discuterne, se ne occuperà Simone. La commissione viene sciolta per oggi.