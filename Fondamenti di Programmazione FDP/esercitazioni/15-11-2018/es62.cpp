#include <iostream>

using namespace std;

int** chained_matrix(int rows, int cols);

int main(int argc, char const *argv[])
{
    int rows, cols;    
    int **array;

    cout << "Input the number of rows of the 2D matrix" << endl;
    cin >> rows;
    cout << "Input the number of cols of the 2D matrix" << endl;
    cin >> cols;

    array = chained_matrix(rows, cols);

    return 0;
}

int** chained_matrix(int nrows, int ncols)
{
    // Create the matrix
    int **array = new int *[nrows];
    for (int i = 0; i < nrows; ++i)
        array[i] = new int[ncols];
    // Fill the matrix
    int last_number = 0;

    for(int i = 0; i < nrows; i++)
    {
        for(int j = 0; j < ncols; j++)
        {
            if (i % 2 == 0) 
            {
                array[i][j] = last_number++;
            }
            else 
            {
                // for less occult and less obscure reasons i had to put -1
                array[i][ncols-j-1] = last_number++;
            }
            
        }   
    }
    // Pretty print the array
    for (int i = 0; i < nrows; i++)
    {
        for (int j = 0; j < ncols; j++)
        {
            cout << array[i][j] << "\t";
        }
        cout << "\n";
    }

    return array;
}