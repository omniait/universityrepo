#include <iostream>
//#include "esempio.hpp"

using namespace std;

const int nrows = 3;
const int ncols = 3;

int* mult(int[nrows][ncols], int[ncols]);
void pretty_matrix_print(int[nrows][ncols]);
void pretty_vector_print(int[], int);

int main(int argc, char const *argv[])
{
    int matrix[nrows][ncols];
    int vector[ncols];

    cout << "Input 9 numbers of the matrix" << endl;

    for (int i = 0; i < nrows; i++)
    {
        for (int j = 0; j < ncols; j++)
        {
            cout << i << " : " << j << endl;
            cin >> matrix[i][j];
        }
    }
    cout << "Input 3 numbers of the vector" << endl;

    for (int i = 0; i < ncols; i++)
    {
        cout << i << " : " << endl;
        cin >> vector[i];
        
    }

    pretty_matrix_print(matrix);
    pretty_vector_print(vector, ncols);

    pretty_vector_print(mult(matrix, vector), ncols);

    return 0;
}

int* mult(int matrix[][ncols], int vector[])
{  
    int sum = 0;
    for(int i = 0; i < nrows; i++)
    {
        for(int j = 0; j < ncols; j++)
        {
            sum += matrix[i][j];
        }
        vector[i] = sum;
        sum = 0;
    }

    return vector;
}

void pretty_matrix_print(int array[nrows][ncols])
{
    for (int i = 0; i < nrows; i++)
    {
        for (int j = 0; j < ncols; j++)
        {
            cout << array[i][j] << "\t";
        }
        cout << "\n";
    }
}

void pretty_vector_print(int array[], int length)
{
    for(int i = 0; i < length; i++)
    {
        cout << array[i] << endl;
    }   
}