#include <iostream>

using namespace std;

const int nrows = 3;
const int ncols = 3;

int sum(int[nrows][ncols]);
void pretty_matrix_print(int[nrows][ncols]);

int main(int argc, char const *argv[])
{
    int array[nrows][ncols];
    
    cout << "input 9 numbers" << endl;

    for(int i = 0; i < nrows; i++)
    {
        for(int j = 0; j < ncols; j++)
        {
            cout << i << " : " << j << endl;
            cin >> array[i][j];
        }   
    }

    pretty_matrix_print(array);

    cout << sum(array) << endl;

    return 0;
}

int sum(int array[][ncols])
{
    int result = 0;

    for (int i = 0; i < nrows; i++)
    {
        result += array[i][i];
        if (i != 2-i) 
        {
            result += array[i][2 - i];
        }
    }

    return result;
}

void pretty_matrix_print(int array[nrows][ncols])
{
    for(int i = 0; i < nrows; i++)
    { 
        for(int j = 0; j < ncols; j++)
        {
            cout << array[i][j] << "\t";
        }
        cout << "\n";
    }
    
}