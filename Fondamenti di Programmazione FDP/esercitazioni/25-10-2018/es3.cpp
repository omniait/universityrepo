#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    int vec1[] = {2,9,4};
    int vec2[] = {3,7,0,1};
    
    int vec3[7];

    /*
    funzionerebbe con i vettori belli
    questi sono brtt
    copy(vec1, 3, vec3);
    copy(vec2, 4, vec3 + 3);
    */

    cout << "vec1: ";
    
    for(size_t i = 0; i < 3; i++)
    {
        cout << vec1[i] << "-";
        vec3[i] = vec1[i];
    }
    cout << endl;

    cout << "vec2: ";
    
    for(size_t i = 0; i < 4; i++)
    {
        cout << vec2[i] << "-";
        vec3[i + 3] = vec2[i];
    }
    cout << endl;

    cout << "vec3: ";
    for(size_t i = 0; i < 7; i++)
    {
        cout << vec3[i] << "-";
    }
    cout << endl;

    return 0;
}
