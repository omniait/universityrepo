#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    // array vars
    const int L = 10;
    int vec[L];
    int current_el;

    // variables for answers
    // bool even;
    int sum = 0;
    int max = -1024;
    int max_index;

    /**input
     * per la massima efficenza si può fare tutto qui 
     * nello stesso ciclo
     * anche se fa schifo
     * alloco la variabile current_el per non accedere
     * ogni volta al vettore, che è un'operazione costosa
    */
    for(size_t i = 0; i < L; i++)
    {
        cin >> current_el;
        sum += current_el;
        vec[i] = current_el;
        if(current_el > max)
        {
            max_index = i;
            max = current_el;
        }
    }
    
    cout << "The sum is: ";
    cout << (sum % 2 == 0 ? "EVEN" : "ODD") << endl;

    cout << "Biggest number is in: " << max_index << " Position" << endl;

    return 0;
}
