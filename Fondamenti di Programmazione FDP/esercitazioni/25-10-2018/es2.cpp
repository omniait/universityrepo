#include <iostream>
#include <cstring>

using namespace std;

int main(int argc, char const *argv[])
{
    const int L = 40;
    int real_lenght;
    char word[L + 1];

    bool palindrome = true;

    cout << "Input a word with at maximum " << L << " characters" << endl;
    cin >> word;

    real_lenght = strlen(word);

    /**
     * Per costruire un ciclo che finisce ad un certo evento
     * Possiamo usare un for con una condizione aggiuntiva
     * oppure usare un while, propongo entrambe le soluzioni
    */
    // for
    for(size_t i = 0; (i < (real_lenght/2)) && palindrome; i++)
    {
        if (word[i] != word[real_lenght -i -1])
            palindrome = false;
            //cout << "Found different character at:" << i << "-" << L-i-1 << endl;
    }
    cout << "The word is: ";
    cout << (palindrome ? "Palindrome" : "Not Palindrome") << endl;

    // while
    int i = 0;
    palindrome = true;

    while( i < (real_lenght/2)) && palindrome)
    {
        if (word[i] != word[real_lenght -i -1])
            palindrome = false;
        i++;        
    }
    
    cout << "The word is: ";
    cout << (palindrome ? "Palindrome" : "Not Palindrome") << endl;

    return 0;
}
