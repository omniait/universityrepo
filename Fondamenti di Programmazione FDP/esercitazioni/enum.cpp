#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    enum Roba { Val1, Val2, Val3, Val4 /*etc.*/};

    // Assign by enum value
    Roba var1 = Val1;
    // Assign by enum index
    Roba var2 = Roba(0);

    cout << var1 << endl;
    cout << var2 << endl;

    switch (var1)
    {
        case Val1:
            break;
        case Val2:
            break;    
        default:
            break;
    }    
    /*
    while(true){
        cout << "." << "-";
    }
    */

    return 0;
}
