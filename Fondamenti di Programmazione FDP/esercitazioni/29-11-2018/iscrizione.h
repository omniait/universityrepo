struct elem
{
    char nome[31];
    int pettorale;
    elem *pun;
};

void init(elem*& List);
void visualizza(elem*& List);
void aggiungi(elem*& List, const char nome[], int pettorale);
char *cerca(elem *List, const char nome[]);
void elimina(elem *List, const char nome[]);