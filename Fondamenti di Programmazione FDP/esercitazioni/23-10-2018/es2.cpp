#include <iostream>

using namespace std;

void unlimitedPower(int b, int e);

int main(int argc, char const *argv[])
{
    int b, e, r = 1;

    cout << "Inserisci la base: ";
    cin >> b;
    cout << "Inserisci l'esponente: ";
    cin >> e;

    
    if (e < 0) 
        cout << "Errore, esponente negativo" << endl;
    else 
        if (e == 0) 
            if (b == 0)
                cout << "Risultato indefinito" << endl; 
            else
                cout << b << endl;
        else
        {
            for (int i = 0; i < e; i++)
                r = r * b;
            cout << r << endl;
        }

    return 0;
}