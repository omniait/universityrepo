#include <iostream>

using namespace std;

int main()
{
    int l, h;

    cout << "Inserisci il lato" << endl;
    cin >> l;
    cout << "Inserisci l'altezza" << endl;
    cin >> h;

    
    for(size_t i = 0; i < h; i++)
    {
        for(size_t j = 0; j < l; j++)
        {
            cout << "* ";
        }    
        cout << endl;
    }

    
    for(size_t i = 0; i < l * 2 - 1; i++)
    {
        cout << "_";
    }
    cout << endl;

    for(size_t i = 0; i < h; i++)
    {
        for(size_t j = 0; j < l; j++)
        {
            if (i == 0 || j == 0 || i == h-1 || j == l-1)
            {
                cout << "* ";
            }
            else {
                cout << "  ";
            }
        }    
        cout << endl;
    }

    return 0;
}
