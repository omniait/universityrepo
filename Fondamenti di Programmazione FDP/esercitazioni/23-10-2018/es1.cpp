#include <iostream>

using namespace std;

/**
 * printMenu - Stampa il menu a terminale 
 * minimum - Tra due numeri prende il minimo
 * maximum - Tra due numeri prende il massimo
 * average - Calcola la media aritmetica tra due numeri
 * handleInput - Gestisci l'input via reference
*/ 
void printMenu();
void minimum(int a, int b);
void maximum(int a, int b);
void average(int a, int b);
void handleInput(int &a, int &b);

int main()
{
    bool running = true;
    // enum Choices {Exit, Minimum, Maximum, Average};
    // Choices choice;
    int input;
    int a, b;

    while(running)
    {
        printMenu();
        cout << "Fai una scelta: ";
        cin >> input;
        //choice = choice{input];

        switch (input)
        {
            case 1:
                handleInput(a, b);
                minimum(a, b);
                break;
            case 2:
                handleInput(a, b);
                maximum(a, b);
                break;
            case 3:
                handleInput(a, b);
                average(a, b);
                break;
            case 0:
                running = false;
                break;
            default:
                cout << "Scelta non valida" << endl;
                break;
        }
    }
}

void printMenu()
{
    cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|" << endl;
    cout << "| Esercitazione numero 1 del 23 - 10 - 2018 |" << endl;
    cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|" << endl;
    cout << "| 1. Trova il minimo tra due interi         |" << endl;
    cout << "| 2. Massimo tra due interi                 |" << endl;
    cout << "| 3. Media aritmetica tra due interi        |" << endl;
    cout << "| 0. Esci dal programma                     |" << endl;
    cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|" << endl;
}
void handleInput(int &a, int &b)
{
    cout << "Inserisci i due numeri" << endl;
    cin >> a;
    cin >> b;
}

void minimum(int a, int b)
{
    int min;
    min = a > b ? b : a;
    cout << "Il minimo tra " << a << "/" << b << " e': " << min << endl;    
}
void maximum(int a, int b)
{
    int max;
    max = a < b ? b : a;
    cout << "Il massimo tra " << a << "/" << b << " e': " << max << endl;
}
void average(int a, int b)
{
    float avg;
    avg = (float)(a + b)/2;
    cout << "La media e' " << avg << endl;
}

