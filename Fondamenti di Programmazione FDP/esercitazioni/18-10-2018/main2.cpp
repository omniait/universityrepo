#include <iostream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
    int input;
    cout << "Inserisci un numero intero \n";

    cin>>input;
    
    
    if (input > 0) 
        cout<<"P \n";
    else if(input == 0) 
        cout<<"Z \n";    
    else 
        cout<<"N \n";

    string output = input > 0 ? "P" : input == 0 ? "Z" : "N";

    cout << output << endl;

    return 0;
}
