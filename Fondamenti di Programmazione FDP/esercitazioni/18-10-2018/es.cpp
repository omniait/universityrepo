#include <iostream>

// in output: multiplo di 4, o di 2 ma non di 4 o non multiplo di 2

int main(int argc, char const *argv[])
{
    /* code */
    int input;
    std::string output;

    std::cout << "Inserisci un numero" << std::endl; 
    std::cin >> input;

    
    if (input > 0) {
        if (input % 2 == 0) {
            output = "Multiplo di 2";
            
            if (input % 4 == 0) {
                output = "Multiplo di 4 (quindi multiplo anche di 2)";
            }
            
        }
        else {
            output = "Non è multiplo di 2 (quindi neanche di 4)";
        }
        
    }
    else {
        output = "Scemo idiota serve un numero positivo, " 
        "sono un programma stupido per cui devi riavviarmi a mano";
    }
    
    std::cout<<output<<std::endl;

    return 0;
}
