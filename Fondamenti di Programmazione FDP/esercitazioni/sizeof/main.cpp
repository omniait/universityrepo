#include <iostream>

using namespace std;

main(int argc, char const *argv[])
{
    cout << "char "<< sizeof(char) * 8 << endl;
    cout << "int " << sizeof(int) * 8 << endl;
    cout << "float " << sizeof(float) * 8 << endl;
    cout << "double " << sizeof(double) * 8 << endl;
    cout << "long int " << sizeof(long int) * 8 << endl;
    cout << "long long int " << sizeof(long long int) * 8 << endl;
    cout << "8=========================================D" << endl;

    return 0;
}
