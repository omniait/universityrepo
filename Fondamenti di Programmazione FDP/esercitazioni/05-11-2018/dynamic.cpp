#include <stdio.h>

main(int argc, char const *argv[])
{
    int* pointer = new int;
    
    /** 
     * pointer that points to a int memory cell 
     * which has 10 as value
    */
    *pointer = 10;

    // print pointer memory address
    printf("%i \n", pointer);
    // print the content in memory
    printf("%i \n", *pointer);
    // print the memory address of pointed memory
    printf("%i \n", &pointer);

    // delete the pointer
    delete pointer;

    return 0;
}
