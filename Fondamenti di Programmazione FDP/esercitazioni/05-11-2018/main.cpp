#include <stdio.h>

int i = 100;
// Scope examples

void iDontLikeSandItsCoarseAndRoughAndIrritatingAndItGetsEverywhere();
int helloThereGeneralKenobi();

int main(int argc, char const *argv[])
{
    iDontLikeSandItsCoarseAndRoughAndIrritatingAndItGetsEverywhere();
    printf("%i \n", i);
    int i, j;
    i = 1;
    j = 5;

    printf("%i, \t %i \n", i, j);
    {
        printf("%i, \t %i \n", i, j);
        int i = 10;
        j = 50;
        printf("%i, \t %i \n", i, j);
    }

    printf("%i, \t %i \n", i, j);

    
    for(size_t i = 0; i < 5; i++)
    {
        printf("%i", helloThereGeneralKenobi());
    }
    printf("\n");

    return 0;
}

void iDontLikeSandItsCoarseAndRoughAndIrritatingAndItGetsEverywhere() 
{
    int i = 2;
    printf("%i \n", i);
}

int helloThereGeneralKenobi()
{
    static int aFineAdditionToMyCollection = 0;
    return aFineAdditionToMyCollection++;
}