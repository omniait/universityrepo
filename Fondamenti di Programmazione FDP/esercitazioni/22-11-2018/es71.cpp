#include <iostream>
using namespace std;
int main()
{
    int n;
    cout << "Inserisci quanti numeri vuoi:" << endl;
    cin >> n;
    if (n < 1)
    {
        cout << "Numero non valido" << endl;
        return 0;
    }
    // Alloca vettore
    int *v = new int[n];
    
    // Inizializza vettore
    cout << "Inserisci " << n << " numeri:" << endl;
    for (int i = 0; i < n; i++)
        cin >> v[i];
    
    int max = 0;
    for (int i = 0; i < n; i++)
        max = (v[i] > max) ? v[i] : max;
    
    cout << max << endl;
    
    for (int i = 0; i < n; i++)
        v[i] += max;
        //cout << v[i] << endl;

    int mul = v[0];
    for (int i = 1; i < n; i++)
        mul *= v[i];
    
    cout << "Risultato del calcolo:" << endl;
    cout << mul << endl;

    delete[] v;
    return 0;
}