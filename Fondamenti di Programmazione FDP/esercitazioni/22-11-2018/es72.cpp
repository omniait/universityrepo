#include <iostream>
#include <list>
#include <iterator>
using namespace std;

//function for printing the elements in a list
void showlist(list<int> g)
{
    list<int>::iterator it;
    for (it = g.begin(); it != g.end(); ++it)
        cout << '\t' << *it;
    cout << '\n';
}

int main()
{
    list <int> my_list;
    list<int>::iterator itr;

    my_list.push_front(50);
    showlist(my_list);
    
    my_list.push_back(60);
    my_list.push_front(70);

    showlist(my_list);
    
    itr = my_list.begin();
    advance(itr, 1);

    my_list.erase(itr);
    showlist(my_list);

    return 0;
}
