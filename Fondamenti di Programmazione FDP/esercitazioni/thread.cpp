// thread example
#include <iostream> // std::cout
#include <thread>   // std::thread
#include <random>
#include <time.h>
#include <unistd.h>
#include <future>

void dummyTask()
{
    std::cout<<"Hello from task"<<std::endl;
}

void printCrap()
{
    while(true)
    {
        std::cout << rand() % 2 + 0;
        //sleep(1);
        if (rand() % 5 == 0)
            std::cout << " "; 
        else    
            std::cout << "";

        //std::cout<< (rand() % 2 == 0) ? " " : "";
    }
}

int main()
{
    srand(time(0));
    
    // Async test
    //std::future<void> task1 = std::async(dummyTask);
    //std::future<void> task2 = std::async(dummyTask);

    std::future<void> task1 = std::async(printCrap);
    std::future<void> task2 = std::async(printCrap);
    

    task1.get();
    task2.get();

    /*
    // Classic Threads
    std::thread first(printCrap);
    std::thread second(printCrap);

    first.join();
    second.join();
   */

    return 0;
}