# C++ Reference

## C++ string

```c++
#include<string>
std::string
```

### Iterators

| Method          | Description                                            |
| --------------- | ------------------------------------------------------ |
| ``str.begin()`` | Returns an iterator to the beginning of the string.    |
| ``str.end()``   | Returns an iterator to the past-the-end of the string. |

### Size

| Methods             | Description            |
| ------------------- | ---------------------- |
| ``str.size()`` | Return the number of bytes in the string (same as ``length()``). |
| ``str.length()``     | Return the number of bytes in the string (same as ``size()``). |
| ``str.max_size()`` | The maximum length the string can reach.                     |
| ``str.resize(size_t n, char c)`` | Resizes the string to a length of n characters. <br />If string is reduced it gets truncated. <br />c is filler character if string is expanded. |
| ``str.clear()``      | Clean up string making it empty. |
| ``str.empty()`` | Test if string is empty (``length == 0``). |

### Element access

| Method               | Description                               |
| -------------------- | ----------------------------------------- |
| ``str[size_t n]``    | Get nth character, same as ``at()``       |
| ``str.at(size_t n)`` | Get nth character, same as ``operator[]`` |

### Modifiers

| Method                                                       | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ``str+=str2``                                                | Append to string, same as ``append()``                       |
| ``str.append(string str2)``                                  | Append to string, same as ``operator+=``                     |
| ``str.push_back(char c)``                                    | Append a character to string.                                |
| ``str.assign(string str2)``                                  | Copies str2 to str, same as ``operator=``.                   |
| ``str.assign(string str2, size_t f, size_t n)``              | Goes to f-th character and copies n characters.              |
| ``str.assign(size_t n, char c)``                             | Makes n characters in the string.                            |
| ``str.assign(str2.begin(), str2.end())``                     | Copies str2 to str using iterators, <br />can use operators to make substring. |
| ``str.insert(size_t p, string str2)``                        | Adds str2 content to str from p-th character.                |
| ``str.insert(size_t p, string str2, size_t subpos, size_t sublen)`` | Adds the str2’s substring content from subpos to subpos+sublen to str from p-th character. |
| ``str.insert(size_t p, string str2, size_t n)``              | Adds the str2’s substring content from subpos to str from p-th character. |
| ``str.erase(size_t pos, size_t len)``                        | Erases content of str from pos to pos+len.                   |
| ``str.replace(size_t pos, size_t len, string str2)``         | Replaces str content from pos to len with str2 content.      |
| ``str.replace(size_t pos, size_t len, string str2, size_t subpos, size_t sublen)`` | Replaces str content from pos to len with the str2’s substring from subpos to subpos + sublen content. |
| ``str.swap(string str2)``                                    | Swaps str and str2.                                          |

### Operations

| Method                                            | Description                                                  |
| ------------------------------------------------- | ------------------------------------------------------------ |
| ``str.c_str()``                                   | Returns a cstring.                                           |
| ``str.copy(char* s, size_t len, size_t pos = 0)`` | Copies len characters from pos of str to s;<br />s is a cstring. |
| ``str.find(string str2, size_t pos = 0)``         | Searches first occurence of str2 in str from pos.<br />Returns the position of the first character |
| ``str.find(char c, size_t pos = 0)``              | Searches first occurence of c in str from pos. <br />Returns the position of the first character |
| ``str.rfind(string str2, size_t pos = npos)``        | Searches the string for the last occurrence<br /> of the sequence specified by its arguments. <br/> When pos is specified, the search only includes <br />strings that begin at or before position pos, <br />ignoring any possible match beginning after pos. |
|``str.substring(size_t pos = 0, size_t len = npos)``|  Returns a newly constructed string object with its value initialized to a copy of a substring of this object.<br/> The substring is the portion of the object that starts at character position pos and spans len characters <br />(or until the end of the string, whichever comes first). |

## C string

```c++
#include <string.h>	
```

### Copying

| Function| Description|
| --------------------------------------------------------- | ------------------------------------------------------------ |
| ``strcpy(char* destination, char* source);`` | Copies the C string pointed <br />by source into the array pointed by destination,<br />including the terminating null character. <br/> To avoid overflow: the size of the array pointed <br />by destination shall be long enough to contain the same C string as source <br />(including the terminating null character), <br />and should not overlap in memory with source. |
| ``strncpy(char* destination, char* source, size_t num);`` | Copies the first num characters of source to destination. <br />If the end of the source C string is found <br />before num characters have been copied, <br />destination is padded with zeros <br />until a total of num characters have been written to it. <br/> No null-character is implicitly appended at the end <br />of destination if source is longer than num. <br />Thus, destination shall not be considered <br />a null terminated C string. |

### Concatenation

| Function                                                  | Description                                                  |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| ``strcat(char* destination, char* source);``              | Appends a copy of the source string to the destination string. <br />The terminating null character in destination is overwritten <br />by the first character of source, and a null-character <br />is included at the end of the new string <br />formed by the concatenation of both in destination. |
| ``strncat(char* destination, char* source, size_t num);`` | Appends the first num characters of source to destination, <br/>plus a terminating null-character. <br/> If the length of the C string in source is less than num, <br />only the content up to the terminating null-character is copied. |

### Comparison and length

| Function                                          | Description                                                  |
| ------------------------------------------------- | ------------------------------------------------------------ |
| ``strcmp(char* str1, char* str2 );``              | Compares the C string str1 to the C string str2.<br />Returns 0 if contents are equal. |
| ``strncmp(char* str1, char* str2, size_t num );`` | Compares up to num characters of the C string str1 to those of the C string str2.<br />Returns 0 if contents are equal. |
| ``strlen(char* str);``                            | Returns the length of the string.                            |

### Searching

| Function                                        | Description                                                  |
| ----------------------------------------------- | ------------------------------------------------------------ |
| ``char* strchr(char* str, char character);``    | Returns a pointer to the first occurrence of character<br />in the string str or NULL if not found. |
| ``strcspn(char* str1, char* str2 );``           | Scans str1 for the first occurrence of any of the characters<br />that are part of str2, returning the number of characters <br />of str1 read before this first occurrence.<br/> The search includes the terminating null-characters. <br />Therefore, the function will return the length of str1<br />if none of the characters of str2 are found in str1. |
| ``char* strpbrk(char* str1, char* str2 );``     | Returns a pointer to the first occurrence in str1 <br />of any of the characters that are part of str2, <br />or a null pointer if there are no matches. |
| ``char* strrchr(char* str, char character);``   | Returns a pointer to the last occurrence<br />of character in the string str.<br/>The terminating null-character is considered <br />part of the C string. Therefore, <br />it can also be located to retrieve a pointer <br />to the end of a string. |
| ``strspn(char* str1, char* str2);``             | Returns the length of the initial portion of str1<br />which consists only of characters that are part of str2. |
| ``char* strstr (char* str1, char* str2);``      | Returns a pointer to the first occurrence of str2 in str1, <br />or a null pointer if str2 is not part of str1. |
| ``char* strtok (char* str, char* delimiters);`` | If a token is found, <br />a pointer to the beginning of the token.<br/>Otherwise, a null pointer. |

## Double Linked List

![img](assets/DLL1.png)

```c++
struct Node { 
    int data;
    struct Node* next;
    struct Node* prev;
};
// one higher scope
Node *head, *tail;
```

### Add to head

![img](assets/DLL_add_front1.png)

```c++
// checks must be already performed
void push_front(int new_data)
{
    /* 1. allocate node */
    struct Node *new_node = new Node;
    /* 2. put in the data  */
    new_node->data = new_data;
    /* 3. Make next of new node as head and previous as nullptr */
    new_node->next = head;
    new_node->prev = nullptr;
    /* 4. change prev of head node to new node (if it exists) */
    if (head != nullptr)
        head->prev = new_node;
    /* if list is empty then head and tail are the same */
    else
        tail = new_node;
    /* 5. always move the head to point to the new node */
    head = new_node;
}
```



### Add to tail

![img](assets/DLL_add_end1.png)

```c++
// checks must be already performed
void push_back(int new_data)
{
    /* 1. allocate node */
    struct Node *new_node = new Node;
    /* 2. put in the data  */
    new_node->data = new_data;
    /* 3. Make prev of new node as tail and next as nullptr */
    new_node->next = nullptr;
    new_node->prev = tail;
    /* 4. change next of tail node to new node (if it exists) */
    if (tail != nullptr)
        tail->next = new_node;
    /* if list is empty then head and tail are the same */
    else
        head = new_node;
    /* 5. move the tail to point to the new node */
    tail = new_node;
}
```

### Find an element

```c++
Node *seek(int data_to_search)
{
    bool found = false;
    struct Node *node = head;
    while (node != nullptr && !found)
    {
        if(node->data == data_to_search)
            found = true;
        else
            node = node->next;
    }
    /* if it doesn't exist returns nullptr */
    return node;
}
```

### Add after given node

![dll_add_middle](assets/DLL_add_middle1.png)

```c++
/* Given a node as prev_node, insert a new node after the given node */
void push_after(struct Node *prev_node, int new_data)
{
    /*1. check if the given prev_node is NULL */
    if (prev_node == nullptr)
    {
        cout << "The given previous node cannot be NULL" << endl;
    }
    else
    {
        /* 2. allocate new node */
        struct Node *new_node = new Node;
        /* 3. put in the data  */
        new_node->data = new_data;
        /* 4. Make next of new node as next of prev_node */
        new_node->next = prev_node->next;
        /* 5. Make the next of prev_node as new_node */
        prev_node->next = new_node;
        /* 6. Make prev_node as previous of new_node */
        new_node->prev = prev_node;
        /* 7. Change previous of new_node's next node */
        if (new_node->next != nullptr)
            new_node->next->prev = new_node;
    }
}
```

### Add before given node

```c++
/* Given a node as next_node, insert a new node after the given node */
void push_before(struct Node *next_node, int new_data)
{
    /*1. check if the given prev_node is NULL */
    if (next_node == NULL)
    {
        cout << "The given next node cannot be NULL" << endl;
    }
    else
    {
        /* 2. allocate new node */
        struct Node *new_node = new Node;
        /* 3. put in the data  */
        new_node->data = new_data;
        /* 4. Make prev of new node as prev of next_node */
        new_node->prev = next_node->prev;
        /* 5. Make the prev of next_node as new_node */
        next_node->prev = new_node;
        /* 6. Make next_node as next of new_node */
        new_node->next = next_node;
        /* 7. Change next of new_node's previous node */
        if (new_node->prev != NULL)
            new_node->prev->next = new_node;
        /* 8. If the prev of new_node is NULL, it will be the new head node */
        else
            head = new_node;
    }
}
```

### Print the list

#### Print onward, my minions!

```c++
void print_list_forward()
{
    cout << "Onward, my minions! To the end of the exam!" << endl;
    struct Node *node = head;
    while (node != nullptr)
    {
        cout << node->data;
        node = node->next;
    }
}
```

#### Print fall back, my minions!

```c++
void print_list_backward()
{
    cout << "Fallback, my minions! We've failed and we must retreat!" << endl;
    struct Node *node = tail;
    while (node != nullptr)
    {
        cout << node->data;
        node = node->prev;
    }
    cout << "I'll be Bach!" << endl;
}
```

### Delete a node

#### Starting picture of list

![img](assets/Delete_lincked_list.jpg)

#### Delete the head

![](assets/Delete_lincked_list2.jpg)

#### Delete a middle node

![](assets/Delete_lincked_list3.jpg)

#### Delete the tail

![](assets/Delete_lincked_list4.jpg)

```c++
void delete_node(struct Node *del)
{
    if(del != nullptr)
    {
        /* 1. If node to be deleted is head node */
        if (head == del)
            head = del->next;
        /* 2. If node to be deleted is tail node */
        if (tail == del)
            tail = del->prev;
        /* 3. Change next only if node to be deleted is NOT the last node */
        if (del->next != nullptr)
            del->next->prev = del->prev;
        /* 4. Change prev only if node to be deleted is NOT the first node */
        if (del->prev != nullptr)
            del->prev->next = del->next;
        /* 5. Finally, free the memory occupied by del*/
        delete del;
    }
    else
    {
        cout << "Can't delete nullptr" << endl;
    }
}
```

#### Delete All

```c++
/* Free all list items */
void delete_all()
{
    /* allocate current and next node */
	struct Node *current, *next;
	/* 1. start from head */
    current = head;
	/* 2. loop until it reaches the tail */
	while (current != NULL)
	{
        /* 3. save next node */
		next = current->next;
        /* 4. free current node */
		delete current;
        /* 5. move current */
		current = next;
	}
    /* 6. clean up head and tail */
	head = tail = NULL;
}
```



## Dynamic array

```c++
// 1 dimension
int len = 10;
int * array = new array[len];
// 2 dimensions
int n_rows, n_cols;
n_rows = n_cols = 10;
int ** matrix = new int *[n_rows];
for (int i = 0; i < n_rows; i++)
{
    matrix[i] = new row[n_cols];
}
```

## Operator Overloading

### File Header

```c++
// constructor overload
ClassName(const ClassName&);
// copy operator =
ClassName& operator=(const ClassName&);
// +, -, *, /, %, ++, --, etc
ClassName operator+(type);
// !
ClassName& operator!();
// +=, -=, /=, *=
ClassName& operator+=(type);
// input & output
friend istream &operator>>(istream&, ClassName&);
friend ostream& operator<<(ostream&, const ClassName&);
// type conversion
operator new_type() const;
```

### File Cpp

```c++
// constructor overload
ClassName::ClassName(const ClassName& source)
{
    data = source.data;
}
// copy operator =
ClassName& ClassName::operator=(const ClassName &source) 
{
    data = source.data;
    return *this;
}
// +, -, *, /, %, ++, --, etc
ClassName::ClassName operator+(type parameter)
{
    // Note: can access this->data
    ClassName new_object;
    // ...
    return new_object;
}
// !
ClassName& operator!()
{
    // Note: can access this->data
    data;
    // ...
    return *this;    
}
// +=, -=, /=, *=
ClassName::ClassName& operator+=(type parameter)
{
    // Note: can access this->data
    data;
    // ...
    return *this;
}
// input & output
ostream &operator<<(ostream &os, const ClassName &CN) 
{ 
    os << CN.data;
    return os;            
}
istream &operator>>(istream  &is, ClassName &CN) 
{ 
    is >> CN.data;
    return is;        
}
// type conversion
ClassName::operator new_type() const 
{
    new_type result;
	// ...
    return result;
}
```

## Debugging

### Rubber duck

Talk to your standard rubber duck explaining how the program should work, the rubber duck will answer telepathically to you the problem in your code.

When the rubber duck fails, use more ducks! (or go on)

### GDB - Terminal

```shell
# compile
g++ main.cpp -g -Wall -Werror -o main.out
# start debugger
gdb main.out
# set breakpoint at line 10
break 10
# run the program
run
# examine variables
# print variabile once
print var_name
# print variable every time it changes
watch var_name
# goes to the next line of code
step
# run 'till the next breakpoint
continue
# quits (of course)
quit
```

