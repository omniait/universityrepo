#include <iostream>
#include <random>
#include <time.h>
#include <unistd.h>

using namespace std;

int main(int argc, char const *argv[])
{
    int n = 10;
    int b_array[n];
    int s_array[n];
    //int array[4] = {1, 2, 3, 4};

    /* initialize random seed: */
    srand(time(NULL));

    for(int i = 0; i < n; i++)
    {
        /* generate secret number between 0 and 99: */
        b_array[i] = rand() % 99;
        s_array[i] = b_array[i];
        usleep(1000);
    }
    
    cout << "Before sorting:" << endl;
    for(int i = 0; i < n; i++)
    {
        cout << b_array[i] << "\t";
    }
    cout << endl;
    
    // Bubble Sort
    int temp;
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            if (b_array[j] > b_array[j + 1]) 
            {
                temp = b_array[j];
                b_array[j] = b_array[j + 1];
                b_array[j + 1] = temp;
            }
        }
    }

    // Selection Sort asc.
    // int m = 10000;
    // int p;
    // int temp;

    for(int i = 0; i < n - 1; i++)
    {
        for(int j = i + 1; j < n; j++)
        {
            if (s_array[j] < s_array[i])
            {
                temp = s_array[i];
                s_array[i] = s_array[j];
                s_array[j] = temp;
            }
        }
    }

    cout << "After sorting:" << endl;
    for (int i = 0; i < n; i++)
    {
        cout << s_array[i] << "\t";
    }
    cout << endl;

    return 0;
}
