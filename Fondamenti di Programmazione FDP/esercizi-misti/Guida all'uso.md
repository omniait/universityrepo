# Guida all'uso

La struttura delle cartelle è la seguente:

```
├───aiuti
├───esercizi
└───soluzioni
```

La cartella esercizi contiene i file con le specifiche di ogni esercizio, vanno fatti in sequenza tenendo un cronometro dall'inizio della lettura delle specifiche finché non è finito il programma (finché non funziona, abbellimenti a parte).

È permesso usare internet e l'ambiente di sviluppo preferito, questi esercizi sono pensati per avere una guida nel capire come ragionare quindi ogni aiuto è lecito.

La cartella aiuti contiene parte della ricerca online destinata a dare una mano nello svolgere degli esercizi, va aperta solo se si incontrano difficoltà e stalli nell'esecuzione degli esercizi.

La cartella soluzioni va aperta solo ad esercizio finito, ovviamente, serve solo come riferimento, non è un "va fatto così, hai sbagliato tutto se non è identico" perché una frase simile non ha senso nella programmazione.

Buon divertimento e buona fortuna!