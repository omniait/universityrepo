#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    // Dichiaro le variabili: 
    // l := Length, h := Height
    int l, h;
    // a := Surface Area
    long int a;
    // p := Perimeter
    long int p;
    // d := Diagonal
    float d;

    // Prendo l'input
    cout << "Inserire la lunghezza del rettangolo \n";
    cin >> l;
    cout << "Inserire l'altezza del rettangolo \n";
    cin >> h;

    // Calcolo i risultati
    a = l * h;
    p = 2 * l + 2 * h;

    d = sqrt((float)(l*l) + (float)(h*h));
    
    // Output dei risultati
    cout << "Area: " << a << endl;
    cout << "Perimeter: " << p << endl;
    cout << "Diagonal: " << d;

    return 0;
}