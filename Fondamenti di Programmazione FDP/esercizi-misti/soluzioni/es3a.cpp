#include <iostream>
#include <math.h>

using namespace std;

// le costanti per convenzione hanno la prima lettera maiuscola
const int Max = 100;
const int Min = 0;
const int Tries = 10;

int guess_a_number(int min, int max)
{
    int guess;
    // explicit typecast e approssimazione con "round to int" rint()
    guess = rint((double)(max + min) / 2);
    return guess;
}

int main()
{
    // all'inizio il gioco non è finito, ovviamente
    bool game_over = false;
    // variabile che contiene il tentativo
    int guess;
    // ci servono delle copie locali per modificare i valori min e max ad ogni tentativo
    int min = Min, max = Max;
    // input dell'utente
    char input;
    // per il ciclo
    int i = 0;

    cout << "Pensa un numero a caso tra " << Min << " e " << Max << endl;
    cout << "Io cerchero` di indovinarlo" << endl;

    guess = guess_a_number(min, max);

    // cerca di farlo con un for!
    // tradotto è: finché il gioco non è finito (come dire game_over == false) E finché i è minore di Tries
    // se non mettessimo la variabile game_over il gioco finirebbe solo dopo il numero prestabilito di tentativi
    while (!game_over && i < Tries)
    {
        cout << "Il numero a cui hai pensato e` " << guess << "? (S, s)" << endl;
        cout << "Oppure e` minore (L, l) o maggiore? (G, g)" << endl;
        cin >> input;
        // la funzione tolower() ci permette di trasformare i caratteri
        input = tolower(input);
        // input = s, l, g

        switch (input)
        {
        case 's':
            cout << "Ho vinto!" << endl;
            game_over = true;
            break;
        case 'l':
            cout << "Hai detto che il numero e` minore, fammi pensare..." << endl;
            max = guess;
            guess = guess_a_number(min, max);
            break;
        case 'g':
            cout << "Hai detto che il numero e` maggiore, fammi pensare..." << endl;
            min = guess;
            guess = guess_a_number(min, max);
            break;
        default:
            cout << "Non ho capito" << endl;
            // decremento i perché il tentativo di indovinare è saltato
            i--;
            break;
        }
        i++;
    }

    if (!game_over)
    {
        cout << "Ho perso! :(" << endl;
    }

    return 0;
}
