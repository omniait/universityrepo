#include <iostream>

#include <time.h>
#include <stdlib.h>

using namespace std;

// dichiarazione enum
// indico il valore minimo per il passaggio di stato
enum Temperature {Low = 0, Medium = 51, High = 91, Critical = 151};

int get_random(int min, int max)
{
    return rand() % max + min;
}

void print_menu()
{
    cout << "____________________________ \n";
    cout << "Opzioni disponibili \n";
    cout << "a. Controlla le temperature \n";
    cout << "b. Chiudi il programma \n";
}

void print_status(int *temp_array, int l)
{
    // cambio i valori
    for(int i = 0; i < l; i++)
    {
        temp_array[i] = get_random(20, 160);
    }

    // enum che rappresenta l'output
    Temperature out;
    // variabile d'appoggio per risparmiare accessi alla memoria
    int real_temp;

    cout << "I sensori hanno queste temperature: \n";

    for(int i = 0; i < l; i++)
    {
        real_temp = temp_array[i];
        // real_temp < 51
        if (real_temp < Medium) 
        {
            out = Low;
        }
        // real_temp < 91
        else if(real_temp < High) 
        {
            out = Medium;
        }
        // real_temp < 151
        else if (real_temp < Critical)
        {
            out = High;
        }
        // real_temp >= 151
        else 
        {
            out = Critical;
        }
        // stampo lo stato usando uno switch
        cout << "S " << i+1 << ": ";
        switch (out)
        {
            case Low:
                cout << "Bassa \n";
                break;
            case Medium:
                cout << "Media \n";
                break;
            case High:
                cout << "Alta \n";
                break;
            case Critical:
                cout << "CRITICA \n";
                break;
            default:
                break;
        }
    }
}

int main()
{
    // dichiaro ma non inizializzo né alloco
    int* temp_array;
    // lunghezza array
    int l;
    // finché il programma è in esecuzione
    bool running = true;

    char input;

    srand(time(NULL));

    cout << "Quanti sensori hai?" << endl;
    cin >> l;
    temp_array = new int(l);

    while(running)
    {
        print_menu();    
        cin >> input;
        
        input = tolower(input);

        switch (input)
        {
            case 'a':
                print_status(temp_array, l);
                break;
            case 'b':
                running = false;
                break;
            default:
                break;
        }
    }
    delete[] temp_array;

    return 0;
}
