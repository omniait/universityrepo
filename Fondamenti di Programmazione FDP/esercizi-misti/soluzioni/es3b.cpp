#include <iostream>
#include <math.h>
// Librerie per il random
#include <stdlib.h> /* srand, rand */
// Prende il tempo attuale dall'inizio dell'epoca (unix epoch)
// serve a settare il seed del random in modo che sia più casuale
#include <time.h>   /* time */

using namespace std;

// le costanti per convenzione hanno la prima lettera maiuscola
const int Max = 50;
const int Min = 0;
const int Tries = 5;

// estrai un nuovo numero
int guess_a_number(int min, int max)
{
    int guess;
    // explicit typecast e approssimazione con "round to int" rint()
    guess = rint((double)(max + min) / 2);
    return guess;
}
// le prossime linee di codice sono dei "magheggi" per ottenere la sleep sia su windows
// che su unix
#ifdef __linux__
    #include <unistd.h> 
#elif _WIN32
    #include <windows.h>
#else
    #error "OS not supported!"
#endif

    // sleep for tot milliseconds
void sleepcp(int milliseconds)
{
    #ifdef __linux__
        nanosleep(milliseconds * 1000);
    #elif _WIN32
        Sleep(milliseconds);
    #else
        #error "OS not supported!"
    #endif
}

// fa perdere tempo, letteralmente
void fake_thinking()
{
    cout << "Sto pensando";
    sleepcp(500);
    cout << ".";
    sleepcp(500);
    cout << ".";
    sleepcp(500);
    cout << ".";
    sleepcp(500);
    cout << ".";
    sleepcp(500);
    cout << endl;
}

int main()
{
    // all'inizio il gioco non è finito, ovviamente
    bool game_over = false;
    // variabile che contiene il tentativo
    int guess;
    // ci servono delle copie locali per modificare i valori min e max ad ogni tentativo
    int min = Min, max = Max;
    // input dell'utente
    char input;
    // per il ciclo
    int i = 0;

    // parte b - sfida
    srand(time(NULL));
    int secret = rand() % Max + Min; // range Min -> Max

    cout << "Pensa un numero a caso tra " << Min << " e " << Max << endl;
    cout << "Io cerchero` di indovinarlo" << endl;

    sleepcp(1000);
    cout << "Ok ho fatto!" << endl;
    sleepcp(500);

    guess = guess_a_number(min, max);

    // cerca di farlo con un for!
    // tradotto è: finché il gioco non è finito (come dire game_over == false) E finché i è minore di Tries
    // se non mettessimo la variabile game_over il gioco finirebbe solo dopo il numero prestabilito di tentativi
    while (!game_over && i < Tries)
    {
        cout << "Il numero a cui hai pensato e` " << guess << "?" << endl;
        cout << "Oppure e` minore o maggiore?" << endl;
        sleepcp(500);

        if (guess == secret) 
        {
            cout << "Ho vinto! Il numero era: " << secret << "! \nMa ho anche perso! :S" << endl;
            game_over = true;
        }
        else if(guess > secret) 
        {
            cout << "Hai detto che il numero e` minore, fammi pensare..." << endl;
            fake_thinking();
            max = guess;
            guess = guess_a_number(min, max);
        }
        else 
        {
            cout << "Hai detto che il numero e` maggiore, fammi pensare..." << endl;
            fake_thinking();
            min = guess;
            guess = guess_a_number(min, max);
        }
        i++;
    }

    if (!game_over)
    {
        cout << "Ho perso! Sono stato battuto da me stesso, quindi ho vinto! :)" << endl;
    }
    return 0;
}
