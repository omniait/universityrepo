#include <iostream>

using namespace std;

struct Node
{
    int data;
    struct Node *next;
    struct Node *prev;
};
Node *head, *tail;

// checks already performed
void push_front(int new_data)
{
    /* 1. allocate node */
    struct Node *new_node = new Node;
    /* 2. put in the data  */
    new_node->data = new_data;
    /* 3. Make next of new node as head and previous as NULL */
    new_node->next = head;
    new_node->prev = NULL;
    /* 4. change prev of head node to new node (if it exists) */
    if (head != NULL)
        head->prev = new_node;
    /* if list is empty then head and tail are the same */
    else
        tail = new_node;
    /* 5. always move the head to point to the new node */
    head = new_node;
}

// checks already performed
void push_back(int new_data)
{
    /* 1. allocate node */
    struct Node *new_node = new Node;
    /* 2. put in the data  */
    new_node->data = new_data;
    /* 3. Make prev of new node as tail and next as NULL */
    new_node->next = NULL;
    new_node->prev = tail;
    /* 4. change next of tail node to new node (if it exists) */
    if (tail != NULL)
        tail->next = new_node;
    /* if list is empty then head and tail are the same */
    else
        head = new_node;
    /* 5. move the tail to point to the new node */
    tail = new_node;
}

void print_list_forward()
{
    struct Node *node = head;
    while (node != NULL)
    {
        cout << node->data << " <-> ";
        node = node->next;
    }
    cout << endl;
}

void print_list_backward()
{
    struct Node *node = tail;
    while (node != NULL)
    {
        cout << node->data << " <-> ";
        node = node->prev;
    }
    cout << endl;
}

void delete_node(struct Node *del)
{
    if (del != NULL)
    {
        /* 1. If node to be deleted is head node */
        if (head == del)
            head = del->next;
        /* 2. If node to be deleted is tail node */
        if (tail == del)
            tail = del->prev;
        /* 3. Change next only if node to be deleted is NOT the last node */
        if (del->next != NULL)
            del->next->prev = del->prev;
        /* 4. Change prev only if node to be deleted is NOT the first node */
        if (del->prev != NULL)
            del->prev->next = del->next;
        /* 5. Finally, free the memory occupied by del*/
        delete del;
    }
    else
    {
        cout << "Can't delete NULL" << endl;
    }
}

/* Given a node as prev_node, insert a new node after the given node */
void push_after(struct Node *prev_node, int new_data)
{
    /*1. check if the given prev_node is NULL */
    if (prev_node == NULL)
    {
        cout << "The given previous node cannot be NULL" << endl;
    }
    else
    {
        /* 2. allocate new node */
        struct Node *new_node = new Node;
        /* 3. put in the data  */
        new_node->data = new_data;
        /* 4. Make next of new node as next of prev_node */
        new_node->next = prev_node->next;
        /* 5. Make the next of prev_node as new_node */
        prev_node->next = new_node;
        /* 6. Make prev_node as previous of new_node */
        new_node->prev = prev_node;
        /* 7. Change previous of new_node's next node */
        if (new_node->next != NULL)
            new_node->next->prev = new_node;
        else
            tail = new_node;
    }
}

/* Given a node as next_node, insert a new node after the given node */
void push_before(struct Node *next_node, int new_data)
{
    /*1. check if the given prev_node is NULL */
    if (next_node == NULL)
    {
        cout << "The given next node cannot be NULL" << endl;
    }
    else
    {
        /* 2. allocate new node */
        struct Node *new_node = new Node;
        /* 3. put in the data  */
        new_node->data = new_data;
        /* 4. Make prev of new node as prev of next_node */
        new_node->prev = next_node->prev;
        /* 5. Make the prev of next_node as new_node */
        next_node->prev = new_node;
        /* 6. Make next_node as next of new_node */
        new_node->next = next_node;
        /* 7. Change next of new_node's previous node */
        if (new_node->prev != NULL)
            new_node->prev->next = new_node;
        /* 8. If the prev of new_node is NULL, it will be the new head node */
        else
            head = new_node;
    }
}

/* Free all list items */
void delete_all()
{
    /* allocate current and next node */
    struct Node *current, *next;
    /* 1. start from head */
    current = head;
    /* 2. loop until it reaches the tail */
    while (current != NULL)
    {
        /* 3. save next node */
        next = current->next;
        /* 4. free current node */
        delete current;
        /* 5. move current */
        current = next;
    }
    /* 6. clean up head and tail */
    head = tail = NULL;
}

/* Search a node and returns it */
Node *seek(int data_to_search)
{
    /* Boolean to interrupt while loop when the node is found */
    bool found = false;
    /* Starts from the head */
    struct Node *node = head;
    /* Keep searching until the node is found or the list is finished */
    while (node != NULL && !found)
    {
        /* If we've found the node we can stop */
        if (node->data == data_to_search)
        {
            found = true;
        }
        /* Else keep searching */
        else
        {
            node = node->next;
        }
    }
    /*  If the list is empty, head is NULL so returns NULL
		If the node cannot be found and the list exists
		it will return NULL because tail->next is always NULL
		and the while loop will end at tail
		If the node has been found it's inside node, so return it */
    return node;
}

int main()
{
    cout << "Print 1: expected \n"
         << endl;
    print_list_forward();

    cout << "Add 0 to head" << endl;
    push_front(0);
    cout << "Print 2: expected \n0" << endl;
    print_list_forward();

    cout << "Add 1 to tail" << endl;
    push_back(1);

    cout << "Print 3: expected \n0 <-> 1" << endl;
    print_list_forward();

    cout << "Add 2 to head" << endl;
    push_front(2);

    cout << "Print 4: expected \n2 <-> 0 <-> 1" << endl;
    print_list_forward();

    cout << "Print 5: expected \n1 <-> 0 <-> 2" << endl;
    print_list_backward();

    cout << "Remove first element" << endl;
    delete_node(head);

    cout << "Print 6: expected \n0 <-> 1" << endl;
    print_list_forward();

    cout << "Remove last element" << endl;
    delete_node(tail);

    cout << "Print 6: expected \n0" << endl;
    print_list_forward();

    cout << "Add 1 to head" << endl;
    push_front(1);
    cout << "Add 2 to head" << endl;
    push_front(2);
    cout << "Add 3 to tail" << endl;
    push_back(3);

    cout << "Print 7: expected \n2 <-> 1 <-> 0 <-> 3" << endl;
    print_list_forward();

    cout << "Remove 0" << endl;
    delete_node(seek(0));
    cout << "Print 8f: expected \n2 <-> 1 <-> 3" << endl;
    print_list_forward();
    cout << "Print 8b: expected \n3 <-> 1 <-> 2" << endl;
    print_list_backward();

    cout << "Remove 6 (non existing element)" << endl;
    delete_node(seek(6));
    cout << "Print 9a: expected \n2 <-> 1 <-> 3" << endl;
    print_list_forward();
    cout << "Print 9b: expected \n3 <-> 1 <-> 2" << endl;
    print_list_backward();

    cout << "Insert 9 after 2" << endl;
    push_after(head, 9);

    cout << "Insert 4 after 3" << endl;
    push_after(seek(3), 4);

    cout << "Insert 5 after 9" << endl;
    push_after(seek(9), 5);

    cout << "Print 10: expected \n2 <-> 9 <-> 5 <-> 1 <-> 3 <-> 4" << endl;
    print_list_forward();

    cout << "Insert 4 after 10 (non existing element)" << endl;
    push_after(seek(10), 4);

    cout << "Print 11a: expected \n2 <-> 9 <-> 5 <-> 1 <-> 3 <-> 4" << endl;
    print_list_forward();

    cout << "Print 11b: expected \n4 <-> 3 <-> 1 <-> 5 <-> 9 <-> 2" << endl;
    print_list_backward();

    cout << "Add 6 before 2" << endl;
    push_before(head, 6);

    cout << "Add 7 before 4" << endl;
    push_before(tail, 7);

    cout << "Add 8 before 5" << endl;
    push_before(seek(5), 8);

    cout << "Add 10 before 50 (non existing element)" << endl;
    push_before(seek(50), 10);

    cout << "Print 12a: expected \n6 <-> 2 <-> 9 <-> 8 <-> 5 <-> 1 <-> 3 <-> 7 <-> 4" << endl;
    print_list_forward();

    cout << "Print 12b: expected \n4 <-> 7 <-> 3 <-> 1 <-> 5 <-> 8 <-> 9 <-> 2 <-> 6" << endl;
    print_list_backward();

    cout << "Delete all nodes" << endl;
    delete_all();

    cout << "Print 13: expected" << endl;
    print_list_forward();

    cout << "Insert 1 to head" << endl;
    push_front(1);

    cout << "Print 14: expected \n1" << endl;
    print_list_forward();

    return 0;
}