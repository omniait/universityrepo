#include <iostream>
#include <math.h> // sqrt()

using namespace std;

// Per convenzione si usa il CamelCase con la prima lettera maiuscola
class Rectangle
{
private:
    int length, height;

public:
    Rectangle(int l, int h)
    {
        this->length = l;
        this->height = h;
    };
    int get_surface_area()
    {
        return length * height;
    };
    int get_perimeter()
    {
        return 2 * length + 2 * height;
    };
    float get_diagonal()
    {
        return sqrt((float)(length * length) + (float)(height * height));
    }

    // il distruttore rimane vuoto e non implementato perché nessun dato è allocato dinamicamente
    //~Rectangle();
};

int main()
{
    Rectangle Rect = Rectangle(2, 4);

    cout << "Il perimetro del rettangolo e`: " << Rect.get_perimeter() << endl;
    cout << "L'area del rettangolo e`: " << Rect.get_surface_area() << endl;
    cout << "La diagonale del rettangolo e`: " << Rect.get_diagonal() << endl;

    // seconda parte
    int n;
    Rectangle** Rects;
    
    int temp_l, temp_h;

    cout << "Quanti rettangoli vuoi creare?" << endl;
    cin >> n;

    // allocazione dinamica
    Rects = new Rectangle*[n];

    cout << "Inserisci i dati di ogni rettangolo: " << endl;
    
    // creazione
    for(int i = 0; i < n; i++)
    {
        cout << "Rettangolo n. " << i << endl;
        cout << "Inserisci lunghezza: " << endl;
        cin >> temp_l;
        cout << "Inserisci altezza: " << endl;
        cin >> temp_h;
        Rects[i] = new Rectangle(temp_l, temp_h);
    }
    
    // visualizza dati
    
    for(int i = 0; i < n; i++)
    {
        cout << "Rettangolo n. " << i << endl;
        cout << "Il perimetro del rettangolo e`: \t" << Rects[i]->get_perimeter() << endl;
        cout << "L'area del rettangolo e`: \t\t" << Rects[i]->get_surface_area() << endl;
        cout << "La diagonale del rettangolo e`: \t" << Rects[i]->get_diagonal() << endl;
    }

    // matrix[i][j];

    cout << Rects << endl;

    // libera memoria
    for(int i = 0; i < n; i++)
    {
        delete[] Rects[i];
    }
    delete[] Rects;

    return 0;
}
