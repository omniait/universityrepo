#include <iostream>
using namespace std;

int main()
{
    cout << "Tipi fondamentali: \n";
    cout << "_______________________________________________________________ \n";
    cout << "Il tipo int occupa:\t" << sizeof(int) << "bytes \t" << sizeof(int) * 8 << "bit \n";
    cout << "Il tipo char occupa:\t" << sizeof(char) << "bytes \t" << sizeof(char) * 8 << "bit \n";
    cout << "Il tipo float occupa:\t" << sizeof(float) << "bytes \t" << sizeof(float) * 8 << "bit \n";
    cout << "Il tipo double occupa:\t" << sizeof(double) << "bytes \t" << sizeof(double) * 8 << "bit \n";
    cout << "Il tipo bool occupa:\t" << sizeof(bool) << "bytes \t" << sizeof(bool) * 8 << "bit \n";
    cout << "_______________________________________________________________ \n";

    cout << "Tipi fondamentali con modificatori: \n";
    cout << "_______________________________________________________________ \n";
    cout << "Il tipo short int occupa:\t" << sizeof(short int) << "bytes \t" << sizeof(short int) * 8 << "bit \n";
    cout << "Il tipo long int occupa:\t" << sizeof(long int) << "bytes \t" << sizeof(long int) * 8 << "bit \n";
    cout << "Il tipo long long int occupa:\t" << sizeof(long long int) << "bytes \t" << sizeof(long long int) * 8 << "bit \n";
    cout << "_______________________________________________________________ \n";

    cout << "Tipi particolari: \n";
    cout << "_______________________________________________________________ \n";
    cout << "Il tipo int* (puntatore) occupa:\t" << sizeof(int*) << "bytes \t" << sizeof(int*) * 8 << "bit \n";
    cout << "Il tipo int** (puntatore a puntatore) occupa:\t" << sizeof(int**) << "bytes \t" << sizeof(int**) * 8 << "bit \n";
    cout << "Il tipo int[2] occupa:\t" << sizeof(int[2]) << "bytes \t" << sizeof(int[2]) * 8 << "bit \n";
    cout << "Il tipo int[4] occupa:\t" << sizeof(int[4]) << "bytes \t" << sizeof(int[4]) * 8 << "bit \n";
    cout << "Il tipo int[8] occupa:\t" << sizeof(int[8]) << "bytes \t" << sizeof(int[8]) * 8 << "bit \n";

    // questo è perché sono state aggiunte le stringhe vere e proprie
    char two_char[3] = {'h', 'e', '\0'};
    char four_char[5] = {'h', 'e', 'l', 'l', '\0'};
    char eight_char[9] = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', '\0'};

    cout << "Il tipo char[2] occupa:\t" << sizeof(char[2]) << "bytes \t" << sizeof(char[2]) * 8 << "bit \n";
    cout << "Il tipo char[4] occupa:\t" << sizeof(char[4]) << "bytes \t" << sizeof(char[4]) * 8 << "bit \n";
    cout << "Il tipo char[8] occupa:\t" << sizeof(char[8]) << "bytes \t" << sizeof(char[8]) * 8 << "bit \n";
    cout << "Il tipo char(2 caratteri reali) occupa:\t" << sizeof(two_char) << "bytes \t" << sizeof(two_char) * 8 << "bit \n";
    cout << "Il tipo char(4 caratteri reali) occupa:\t" << sizeof(four_char) << "bytes \t" << sizeof(four_char) * 8 << "bit \n";
    cout << "Il tipo char(8 caratteri reali) occupa:\t" << sizeof(eight_char) << "bytes \t" << sizeof(eight_char) * 8 << "bit \n";

    cout << "Il tipo string(Nessun carattere) occupa:\t" << sizeof(string) << "bytes \t" << sizeof(string) * 8 << "bit \n";
    cout << "Il tipo string(2 caratteri reali) occupa:\t" << sizeof(string("he")) << "bytes \t" << sizeof(string("he")) * 8 << "bit \n";
    cout << "Il tipo string(4 caratteri reali) occupa:\t" << sizeof(string("hell")) << "bytes \t" << sizeof(string("hell")) * 8 << "bit \n";
    cout << "Il tipo string(8 caratteri reali) occupa:\t" << sizeof(string("hello wo")) << "bytes \t" << sizeof(string("hello wo")) * 8 << "bit \n";

    cout << "Il tipo double[2] occupa:\t" << sizeof(double[2]) << "bytes \t" << sizeof(double[2]) * 8 << "bit \n";
    cout << "Il tipo double[4] occupa:\t" << sizeof(double[4]) << "bytes \t" << sizeof(double[4]) * 8 << "bit \n";
    cout << "Il tipo double[8] occupa:\t" << sizeof(double[8]) << "bytes \t" << sizeof(double[8]) * 8 << "bit \n";
    cout << "_______________________________________________________________ \n";
    
    return 0;
}