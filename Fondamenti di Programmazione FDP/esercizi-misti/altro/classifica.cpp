#include <iostream>
#include <string>

#include <vector>
#include <iterator>
// random
#include <random>

using namespace std;

enum outcome {Defeat = 0, Draw = 1, Win = 3};

// team node for teams list
/*
struct team_node
{
    Team this_team;

    team_node * next;
    team_node * prev;
};
*/

/*
struct match_node
{
    outcome match_outcome_team1;

    team_node * team1;
    team_node * team2;

    match_node * next;
    match_node * prev;
};
*/

// file header
class Team
{
private:
    string name;
    int points;
    // match_node * matches_head;
    // match_node * matches_tail;

public:
    Team(string in_name);
    string get_name();
    void print_info();
    void new_match(int new_points);
    // ~Team();
};

Team::Team(string in_name)
{
    name = in_name;
    points = 0;
}

void Team::new_match(int new_points)
{
    points += new_points;
}

string Team::get_name()
{
    return name;
}

void Team::print_info()
{
    cout << "Team Name: " << name << "\t \t \t";
    cout << "Points: " << points << endl;
}

int get_random(int min, int max)
{
    return rand() % max + min;
}

void new_match(Team & team1, Team & team2, int match_outcome_team1)
{
    switch(match_outcome_team1)
    {
        case 0:
            team1.new_match(Defeat);
            team2.new_match(Win);
            break;
        case 1:
            team1.new_match(Draw);
            team2.new_match(Draw);
            break;
        case 2:
            team1.new_match(Win);
            team2.new_match(Defeat);
            break;
        default:
            break;
    }
}

void print_classification(vector<Team> teams)
{
    for(int i = 0; i < teams.size(); i++)
    {
        teams[i].print_info();
    }
}

int main()
{
    vector<Team> teams;
    string team_name;
    const int N_Teams = 4;

    // 4 squadre
    for(int i = 0; i < N_Teams; i++)
    {
        cout << "Inserisci nome squadra n." << i+1 << endl;
        cin >> team_name;
        Team temp_team(team_name);

        teams.push_back(temp_team);
    }


    default_random_engine generator;
    // 0 -> 3
    uniform_int_distribution<int> team_distribution(0, N_Teams - 1);
    // 0, 1, 2
    uniform_int_distribution<int> match_distribution(0, 2);

    generator.seed();

    // 10 partite
    cout << "Simulo le partite" << endl;
    for(int i = 0; i < 10; i++)
    {
        int team1 = team_distribution(generator);
        int team2 = team_distribution(generator);
        int match_outcome = match_distribution(generator);

        //cout << team1 << "\n" << team2 << "\n" << match_outcome << "\n";

        match_distribution.reset();
        team_distribution.reset();
        new_match(teams[team1], teams[team2], match_outcome);
    }
    
    print_classification(teams);

    return 0;
}
