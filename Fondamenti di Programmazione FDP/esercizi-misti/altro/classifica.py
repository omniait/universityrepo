class Match():
    def __init__(self, team1, team2, team1_outcome):
        self.team1 = team1
        self.team2 = team2
        self.team1_outcome = team1_outcome

class Team():
    def __init__(self, name):
        self.name = name
        self.matches = []
        self.points = 0

    def get_name(self):
        return self.name

    def new_match(self, other_team, outcome):
        self.matches.append(self.name, other_team, outcome)


def main():
    pass

if __name__ == "__main__":
    main()
