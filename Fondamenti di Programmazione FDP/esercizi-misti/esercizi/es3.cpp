/** Requisiti
 * Prima Parte:
 *  In questo programma andiamo ad attuare l'algoritmo di ricerca binaria attraverso un gioco.
 *  Il giocatore (umano) pensa ad un numero compreso tra 0 e 100 estremi inclusi, il computer deve indovinarlo.
 *  Al fine di indovinarlo il computer deve usare l'algoritmo di ricerca binaria, quindi il primo numero che darà
 *  in output sarà 50 ((100+0)/2), il giocatore a questo punto dice al computer se il numero che ha pensato è
 *  maggiore o minore, il gioco va avanti finché o il computer indovina oppure finché esaurisce 10 tentativi
 * Seconda Parte:
 *  Gestire il caso dei numeri con la virgola, per esempio 12.5 deve venire approssimato a 13.
 *  Cerca dove possibile di eseguire il "refactoring", ovvero cerca di creare delle funzioni dove possibile (e dove ha senso farlo!).
 *  Generalizza i parametri prestabiliti, ovvero il range del numero (0 -> 100) e il numero di tentativi.
 * Svolto l'esercizio chiediti e risponditi come mai è possibile usare la ricerca binaria in questo caso.
 * Sfida:
 *  Crea un nuovo programma su questo modello nel quale è il computer a giocare contro sé stesso, è quindi necessario
 *  usare la libreria random
**/
