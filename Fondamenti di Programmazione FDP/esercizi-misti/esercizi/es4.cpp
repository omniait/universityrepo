/** Requisiti
 *  Prima Parte:
 *   In questo programma ci sono un array e degli stati in un enum.
 *   L'utente deve poter essere in grado di operare sull'array 
 *   di una lunghezza scelta dall'utente stesso
 *   L'enum può avere i seguenti stati: "Low, Medium, High, Critical".
 *   L'array rappresenta una sequenza di sensori di temperatura posti su dei componenti elettronici,
 *   per simulare il cambio di temperature usa una funzione random con range 20 -> 160.
 *   La relazione tra la temperatura reale e quella dimostrata dall'enum è questa:
 *   0 -> 50 = Low
 *   51 -> 90 = Medium
 *   91 -> 150 = High
 *   151 -> inf = Critical
 *   Permetti all'utente di visualizzare lo stato dei sensori o di chiudere il programma (ricorda di de-allocare!)
**/
