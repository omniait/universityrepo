/** Requisiti
 * Prima Parte:
 *  Scrivi un programma che prende come input
 *  due numeri interi, che sono la lunghezza e l'altezza di un rettangolo.
 *  Scrivi in output:
 *  Perimetro, Area e Diagonale
 *  NB: Gestire in un modo controllato la diagonale (scriverla quindi come float)
 * Seconda Parte:
 *  Aggiungi un controllo sull'input: i valori non devono essere negativi
 * Sfide Aggiuntive:
 *  Scrivi una tua funzione che calcola la radice quadrata 
 *  da un metodo già esistente (per esempio bisezione e tangente)
 *  Scrivi una tua funzione di approssimazione (simile alla rint del math.h) 
 *  per scrivere la diagonale come numero intero
**/

int approx(float);

main(int argc, char const *argv[])
{
    /* code */
    return 0;
}

int approx(float a)
{
    a = (int)(a * 1000) / 1000;
    return a;
}