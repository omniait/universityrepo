/** Requisiti
 * Prima Parte:
 *  Scrivi un programma che prende mette in output la memoria occupata in byte e bit
 *  dei tipi fondamentali del C++.
 * Seconda Parte:
 *  Sperimenta con la sizeof su array di int, di char e di double, stringhe (tipo string), 
 *  puntatori e puntatori a puntatori
 * Svolto l'esercizio poniti e rispondi alle seguenti domande:
 * 1. Perché il puntatore ha lunghezza di 4 oppure 8 bytes?
 * 2. Se hai un computer a 64bit e ti capita di avere un puntatore a 4 byte quale è il motivo?
 * 3. Quale può essere un motivo del perché la string è allocata staticamente? (domanda soggettiva)
**/
