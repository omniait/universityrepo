/** Requisiti
 *  Prima Parte:
 *   Crea una classe, rappresentante un rettangolo, con all'interno due variabili private
 *   rappresentanti lunghezza e altezza, il programma deve dare in output, attraverso dei metodi
 *   della classe, l'area, il perimetro e la diagonale del rettangolo.
 *   L'oggetto ha l=2 e h=4.
 *   In pratica è l'esercizio 1 con le classi
 *  Seconda Parte:
 *   NB: Questa parte è più complicata del previsto e sicuramente non capita all'esame ma è utile come esercizio.
 *   Alloca dinamicamente in un array n classi (in base all'input dell'utente), gestendo la loro creazione
 *   e stampando ogni volta perimetro, area e diagonale.
**/ 
