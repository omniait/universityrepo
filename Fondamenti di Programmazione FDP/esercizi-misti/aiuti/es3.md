Finalmente possiamo usare delle funzioni.
Una funzione è una parte di codice che viene chiamata ed eseguita solo in certi casi, il ``main()`` è una funzione e viene chiamata all'inizio del programma.
Per creare una funzione dobbiamo prima sapere se e cosa ci restituisce, per esempio una funzione che calcola la media aritmetica può restituirci un ``float`` perché la media tra 1 e 2 è 1.5.
Come creare quindi una funzione?
La struttura è la seguente:
```C++

tipo nome_funzione(tipo parametro, tipo parametro, ...)
{
    // fai cose
    return risultato;
}
```
I parametri sono delle variabili esterne alla funzione, che vengono ricopiate nello spazio di memoria della funzione (se non sono passati per reference, ma non è ancora il momento per questo).
Nell'esempio della media aritmetica abbiamo due parametri, il primo ed il secondo numero (1 e 2), implementarla a questo punto è semplicissimo:
```C++
float average(int a, int b)
{
    return (float)((a+b)/2);
}
```
Perché ho dovuto fare l'explicit typecast?

Alcune funzioni non devono dare nessun valore, in questo caso la funzione assume il tipo ``void``.

```C++
void greet(string name)
{
    cout << "Hello " << name << "!" << endl;
}
```

La libreria ``random`` ci serve per generare numeri casuali in un determinato intervallo (range).
Prima di poterlo generare è però necessario impostare il "seed", perché è ciò che permette all'algoritmo pseudo-casuale di generare numeri pseudo-casuali.

Prova a settare il seed con la funzione ``srand()`` ad una costante, per esempio 5, e a lanciare più volte un programma che stampa un numero casuale, potrai notare che il numero è sempre lo stesso.
```rand()``` è ciò che ci restituisce il numero vero e proprio, questo numero però prende tutto il range di valori assumibili da un ``int`` (ricorda dall'esercizio 2 quanto occupa in memoria un ``int``) e quindi per limitarlo usiamo l'operatore modulo che fa la divisione intera restituendo il resto, che sarà sempre compreso tra 0 ed il divisore - 1.

A questo punto è facile estrarre un numero a caso:

```C++
srand(time(NULL));
int random_number = rand() % Max + Min;
```