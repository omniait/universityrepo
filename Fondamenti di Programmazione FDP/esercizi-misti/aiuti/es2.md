La funzione ``sizeof(variable_name)`` o ``sizeof(type)``  ci dà come risultato la memoria occupata in bytes dalla variabile o dal tipo specificato.
I tipi fondamentali del C++ standard attuale sono:
* Numerici:
  1. ``int``
  2. ``short``
  3. ``float``
  4. ``double``
* Altri:
  1. ``char (int)``
  2. ``bool (short)``
* Modificatori:
  1. ``unsigned`` - Non agisce sulla memoria allocata 
  2. ``long``
  3. ``long long``

Esistono anche altri tipi di dati/variabili ma non fanno parte dei tipi fondamentali.

Esempio:
```C++
sizeof(int);        // = 4
sizeof(long int);   // = 8
```
Tieni a mente che 8 bit formano 1 byte.

NB: In base all'architettura del processore (32 o 64 bit) e del compilatore il puntatore assumerà come spazio in memoria quello più adatto a sé stesso: per indicare univocamente un indirizzo di memoria in un sistema a 64 bit sono necessari, appunto, 64 bit, questo significa che il puntatore occupa 64 bit ovvero 8 byte.

