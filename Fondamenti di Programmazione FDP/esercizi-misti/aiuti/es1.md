In questo caso è necessario usare un "explicit type cast".

Per esempio:

```C++
int a;
float b;

a = 5;

b = a / 2;
cout << b << endl; // output = 2

b = (float)(a/2);
cout << b << endl; // output = 2.5
```

Al posto del float è possibile usare il tipo double, la differenza sta nella rappresentazione interna alla memoria e quindi non ci riguarda.

Per molti calcoli matematici la libreria math.h ha già delle funzioni definite all'interno.
Ad esempio:
```C++
#include <math.h>
/* ... */

square_root = sqrt(5.6); // 2.366
power = pow(2,10); // 1024
round_up = ceil(1.1) // 2
round_down = floor(1.9) // 1
round_nearest = rint(1.5) // 2
round_nearest = rint(1.4) // 1
```

È disponibile la lista di tutte le funzioni a questo link: http://www.cplusplus.com/reference/cmath/