/*
Un incrocio stradale è regolato da un Semaforo. Le auto che
giungono all’incrocio si sistemano su due corsie: la prima è
riservata alle auto che vogliono svoltare a destra, mentre la
seconda è riservata alle auto che vogliono svoltare a sinistra.
Un’auto viene univocamente identificata dalla propria targa,
ovvero da una sequenza di 7 caratteri alfanumerici. In ogni istante, il semaforo può trovarsi in uno dei
seguenti tre stati: rosso, verde per corsia destra, verde per corsia sinistra. 
*/

#include "compito.h"
#include <string.h>

Semaforo::Semaforo()
{
	head = NULL;
	tail = NULL;
	light = Red;
}

Semaforo::~Semaforo()
{
	Car * temp = head;
	Car * to_delete;
	
	while(temp->next != NULL)
	{
		to_delete = temp;
		temp = temp->next;
		
		delete to_delete;
	}
	delete temp;
	
	head = NULL;
	tail = NULL;
}

void Semaforo::arrivo(char t[], char dest)
{
	// controllo targa e destinazione
	if((dest == 'D' || dest == 'S') && strlen(t) == 7)
	{
		// controllo accodamento
		if(!(light == GreenSx && dest == Sx) && !(light == GreenDx && dest == Dx) || light == Red)
		{
			// esiste già
			bool found = false;
			// primo in coda
			if(head == NULL && tail == NULL)
			{
				Car * new_car = new Car;
				strcpy(new_car->t, t);
				new_car->d = Dir(dest);
			
				new_car->next = NULL;
				new_car->prev = NULL;
			
				head = new_car;
				tail = new_car;
			}
			else
			{
				// controllo se esiste già la macchina
				Car * temp = head;
	
				while(temp->next != NULL && !found)
				{
					if(!strcmp(temp->t, t))
					{
						found = true;
						//cout << "found" << endl;
					}
				
					temp = temp->next;
				}

				// non esiste, inserimento new_car
				if(!found)
				{
					Car * new_car = new Car;
					strcpy(new_car->t, t);
					new_car->d = Dir(dest);
			
					new_car->next = NULL;
				
					tail->next = new_car;
					new_car->prev = tail;

					tail = new_car;		
				}
			}
		}
	}
	//scorri();
}

void Semaforo::cambiaStato()
{
	// Red -> GreenDx -> GreenSx -> Red
	switch(light)
	{
		case Red:
			light = GreenDx;
			break;
		case GreenDx:
			light = GreenSx;
			break;
		case GreenSx:
			light = Red;
			break;
		default:
			break;
	}
	scorri();
}

void Semaforo::delete_node(Car * del)
{
	if(head != NULL && del != NULL && tail != NULL)
	{
		if(head == del)
		{
			head = del->next;
		}
		if(tail == del)
		{
			tail = del->prev;
		}
		
		if (del->next != NULL) 
        	del->next->prev = del->prev; 

		if (del->prev != NULL)
			del->prev->next = del->next;
	
		delete del;
	}
}

void Semaforo::scorri()
{
	Car * temp = head;
	Car * to_delete;
	
	while(temp != NULL)
	{
		if(light == GreenDx && temp->d == Dx || light == GreenSx && temp->d == Sx)
		{
			to_delete = temp;
			
			delete_node(to_delete);
		}
		temp = temp->next;
	}
}

ostream &operator<<(ostream &os, const Semaforo &s)
{
	os<<"<";
	switch(s.light)
	{
		case Red:
			os<<"Rosso";
			break;
		case GreenDx:
			os<<"Verde Destra";
			break;
		case GreenSx:
			os<<"Verde Sinistra";
			break;
		default:
			break;
	}
	os<<"> \n";

	Car * temp = s.head;
	
	if(s.head != NULL && s.tail != NULL)
	{
		os<<"[";
		
		while(temp != NULL)
		{
			//os << "ciclo" << endl;
			if(temp->d == Dx)
			{
				os<<temp->t;
				os<<",";			
			}
				
			temp = temp->next;
		}
		os<<endl;
		os<<"[";
	
		temp = s.head;
		while(temp != NULL)
		{
			if(temp->d == Sx)
			{
				os<<temp->t;
				os<<",";			
			}
				
			temp = temp->next;
		}
	}
	else
	{
		os<<"[ \n[ \n";
	}
	
	return os;
}

int Semaforo::cambiaCorsia(char new_d)
{
	return 1;
}

Semaforo::operator int()
{
	return 1;
}


