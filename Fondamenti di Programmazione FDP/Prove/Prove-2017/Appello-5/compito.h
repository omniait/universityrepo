#include <iostream>

using namespace std;

enum Dir 
{
	Dx = 'D', 
	Sx = 'S'
};

enum Status
{
	Red, 
	GreenDx, 
	GreenSx
};

struct Car
{
	char t[7];
	Dir d;
	
	Car * next;
	Car * prev;
};

class Semaforo
{
private:
	Status light;
	Car * head;
	Car * tail;
	
public:
	Semaforo();
	~Semaforo();
	
	void arrivo(char[], char);
	void cambiaStato();
	void delete_node(Car*);
	void scorri();
	friend ostream &operator<<(ostream &os, const Semaforo &s);

	int cambiaCorsia(char);
	operator int();
};
