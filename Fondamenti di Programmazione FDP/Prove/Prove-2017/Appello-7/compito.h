#include <iostream>

using namespace std;

class Pianoforte
{
private:
    char *array;
    unsigned array_length;
public:
    Pianoforte(int n_ottave);
    ~Pianoforte();

    friend ostream& operator<<(ostream&, const Pianoforte&);

    void accordo(char hand, unsigned key);
};


