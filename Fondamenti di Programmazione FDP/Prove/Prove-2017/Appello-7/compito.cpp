#include "compito.h"


Pianoforte::Pianoforte(int n_ottave)
{
    if (n_ottave > 0) 
    {
        array_length = n_ottave * 12;
        array = new char[array_length];

        for(int i = 0; i < array_length; i++)
        {
            array[i] = '-';
        }
        
    }    
}
Pianoforte::~Pianoforte()
{
    delete[] array;
}

ostream &operator<<(ostream &os, const Pianoforte &p)
{
    for(int i = 0; i < p.array_length; i++)
        os << p.array[i];
    
    os << "\n";
    return os;
}

void Pianoforte::accordo(char hand, unsigned key)
{
    bool hand_is_occupied = false;
    
    for(int i = 0; i < array_length && !hand_is_occupied; i++)
    {
        if(array[i] != '-')
            hand_is_occupied = true;
    }
    
    if (!hand_is_occupied) {
        for(int i = 0; i < 5; i++)
        {
            if (array[key + i*2 == '-']) 
            {
                array[key + i*2] = hand;
            }        
        }
    }
    
    
}