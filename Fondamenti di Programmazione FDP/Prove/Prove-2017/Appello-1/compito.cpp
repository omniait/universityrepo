#include "compito.h"
//#include <iostream>
#include <string.h>

using namespace std;

GestoreApp::GestoreApp()
{
    //this->head = NULL;
    head = NULL;
}

GestoreApp::~GestoreApp()
{
    // il chiudi tutte funge già da distruttore
    chiudi_tutte();
}

GestoreApp& operator+=(GestoreApp& ga, const char nome[21])
{
    app *node;
    node = ga.head;
    bool found = false;

    // scorro la lista per controllare se esiste già
    while (node != NULL && !found)
    {
        if (!strcmp(nome, node->nome)) 
        {
            found = true;
        }   

        node = node->next;
    }
    
    if (!found) 
    {
        app *temp = new app;
        temp->next = NULL;
        temp->prev = NULL;

        strcpy(temp->nome, nome);

        if (ga.head == NULL) 
        {
            ga.head = temp;
        }
        else
        {
            temp->next = ga.head;
            temp->next->prev = temp;
            temp->prev = NULL;
            ga.head = temp;
        }
        cout << "Aggiunto " << temp->nome <<" nella lista" << endl;
    }
    
    //cout << "finita aggiunta" << endl;
    delete node;

    return ga;
}

GestoreApp &operator-=(GestoreApp &ga, const char nome[21])
{
    // controlla se la lista è vuota
    if (ga.head != NULL)
    {
        // controlla se l'app è in cima
        if (strcmp(ga.head->nome, nome) == 0)
        {
            // variabile per eliminare
            app *temp = ga.head;
            // nuova testa
            ga.head = ga.head->next;
            // cancella la vecchia testa
            delete temp;
            // assicura che il puntatore della testa sia corretto
            ga.head->prev = NULL;
        }
        else
        {
            app *node = ga.head;
            bool found = false;

            while (node != NULL && !found)
            {
                if (strcmp(node->nome, nome) == 0) 
                {
                    found = true;
                    //cout << "Found node" << endl;
                    app *prev_node = node->prev;
                    app *next_node = node->next;

                    prev_node->next = next_node;
                    next_node->prev = prev_node;

                }
                else
                {
                    node = node->next;
                }
            }
            delete node;
        }
    }
    return ga;
}

void GestoreApp::foreground(char nome[21])
{
    // controlla se la lista è vuota
    if (head != NULL) 
    {
        // controlla se l'app è in cima
        if (strcmp(head->nome, nome) != 0) 
        {
            // variabili per scorrere
            app *temp = head;
            // se ho trovato il nodo giusto diventa true
            bool found = false;

            // scorro la lista
            while(temp != NULL && !found)
            {
                if (strcmp(temp->nome, nome) == 0)
                    found = true;
                else
                    temp = temp->next;
            }
            // se esiste l'app sostituisco
            // supposto che non ci sia ordine d'importanza
            // tra le applicazioni in background
            // swappo semplicemente le due applicazioni
            if (found) 
            {
                // variabile di swap
                //app *swap_temp = head;
                char swap_name[21];
                strcpy(swap_name, head->nome);

                // swap dei dati
                strcpy(head->nome, temp->nome);
                strcpy(temp->nome, swap_name);
                //delete swap_temp;
            }
            // se lo elimino si bugga
            //delete temp;
        }
    }
}

ostream& operator<<(ostream &os, const GestoreApp& ga)
{
    if (ga.head == NULL) {
        os << "[]" << endl;
    }
    else 
    {
        app *temp;
        temp = ga.head;
        os << "[" << temp->nome << "] \t";
        temp = temp->next;

        while(temp != NULL)
        {
            os << temp->nome << "\t";
            temp = temp->next;
        }
    }

    return os;
}

void GestoreApp::chiudi_tutte()
{
    // esegui solo se ci sono app in esecuzione
    if (head != NULL)
    {
        app *node = head;
        app *temp;

        while (node != NULL)
        {
            temp = node;
            node = temp->next;

            delete temp;
        }
        head = NULL;
        delete node;
    }
}