#include<iostream>

using namespace std;

struct app
{
    // dati
    char nome[21];

    // puntatori
    app *next;
    app *prev;
};

class GestoreApp
{
    private:
    app *head;
    //app *tail;

    public:
    // Costruttore
    GestoreApp();
    // Distruttore
    ~GestoreApp();

    // Output
    friend ostream& operator<<(ostream &os, const GestoreApp &ga);

    // Aggiunta app
    friend GestoreApp& operator+=(GestoreApp &ga, const char nome[21]);
    // Elimina app
    friend GestoreApp& operator-=(GestoreApp &ga, const char nome[21]);

    // Foreground
    void foreground(char nome[21]);
    // Chiudi tutte
    void chiudi_tutte();
};