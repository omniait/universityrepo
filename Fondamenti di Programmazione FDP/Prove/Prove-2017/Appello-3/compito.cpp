#include<string.h>
#include<iostream>
#include "compito.h"

using namespace std;


ProntoSoccorso::ProntoSoccorso()
{
	head = NULL;
	tail = NULL;
}

ProntoSoccorso::~ProntoSoccorso()
{
	// controllo che ci sia qualcosa da distruggere
	if (head != nullptr && tail != nullptr)
	{
		Paziente *node = head;
		Paziente *temp;

		while (node != nullptr)
		{
			temp = node;
			node = temp->next;

			delete temp;
		}
		head = nullptr;
		tail = nullptr;
		delete node;
	}
}

void ProntoSoccorso::ricovero(char* nome, Codice priority)
{
	/* Implementazione 3, la mia preferita
	*/
	Paziente *temp = new Paziente;
	temp->next = NULL;
	temp->prev = NULL;

	strcpy(temp->nominativo, nome);
	temp->codice = priority;

	if (head == NULL) 
	{
		temp->prev = NULL;
		head = temp;
		tail = temp;
	}
	else 
	{
		tail->next = temp;
		temp->prev = tail;
		temp->next = NULL;

		tail = temp;
	}
	
	/* Implementazione 2
	Paziente* t=tail;
	tail=new Paziente();

	strcpy(tail->nominativo, nome);
	tail->codice = priority;	
	
	if (head == NULL) 
		head = tail;
	else   
		t->next = tail;
	*/
	/* Implementazione 1
	Paziente paziente* = new Paziente;
	strcpy(paziente->nominativo, nome);
	paziente->codice = priority;
	
	paziente->next = testa;
	testa = paziente;
	*/
}

ostream &operator<<(ostream &os, const ProntoSoccorso &ps)
{
	Paziente* node = ps.head;
	
	// controlla se ci sono pazienti
	if (ps.head != NULL && ps.tail != NULL)
	{
		while(node != NULL)
		{	
			
			os << "Paziente: " << node->nominativo << "\n";

			os << "Codice ";
			switch (node->codice)
			{
				case BIANCO:
					os << "Bianco";
					break;
				case VERDE:
					os << "Verde";
					break;
				case GIALLO:
					os << "Giallo";
					break;
				case ROSSO:
					os << "Rosso";
					break;
				default:
					break;
			}
			os << "\n";
			
			//os << "Codice: " << node->codice << "\n";
			// semplice controllo di debug
			//os << "Indirizzo Next: " << node->next << "\n";
			
			node = node->next;
		}
	}
	else
	{
		os << "Non ci sono pazienti \n";
	}
	return os;
}

void ProntoSoccorso::delete_node(Paziente *node)
{
	// cout << "elimina";
	// essendo double linked list non ho necessità 
	// di scorrere niente
	Paziente *prev_node = node->prev;
	Paziente *next_node = node->next;

	// devo controllare se il nodo è head e/o tail
	// il nodo è unico
	if (head == tail)
	{
		head = NULL;
		tail = NULL;
	} 
	// il nodo è in cima
	else if (node == head)
	{
		head = next_node;
		head->prev = NULL;
	}
	// il nodo è in fondo
	else if (node == tail) 
	{
		tail = prev_node;
		tail->next = NULL;
	} 
	// il nodo è in mezzo
	else
	{
		prev_node->next = next_node;
		next_node->prev = prev_node;
	}	

	delete node;
}

int ProntoSoccorso::prossimo(char*& nome)
{
	// valore di return
	int r = 1;

	// flag per interrompere i cicli
	bool found = false;

	// nodo di scorrimento della lista
	Paziente *node = head;

	// non ci sono pazienti
	if(head == NULL)
	{
		r = 0;
	}
	// c'è un solo paziente
	else if(head == tail)
	{
		nome = new char[21];

		strcpy(nome, head->nominativo);

		delete head;
		delete tail;

		Paziente *temp = new Paziente;

		temp->next = NULL;
		temp->prev = NULL;

		head = temp;
		tail = temp;
	}
	// bisogna scorrere la lista
	else
	{
		char* max_code_name;
		
		// strcpy(max_code_name, nome);
		// tengo traccia del codice massimo incontrato
		Codice max_code = BIANCO;
		
		// primo scorrimento, cerco il codice massimo
		// o il primo codice rosso
		while(node != NULL && !found)
		{
			//cout << node->nominativo << " " << node->codice << " ";
			//cout << node->next << endl;
			
			// controllo priorità maggiore
			if(node->codice >= max_code)
			{
				max_code = node->codice;
			}
			// se c'è un codice rosso posso bloccarmi
			if(max_code == ROSSO)
			{
				found = true;
				nome = new char[21];
				
				strcpy(nome, node->nominativo);
				delete_node(node);
			}
			else
			{
				// se e solo se non ho trovato il paziente
				// devo continuare a scorrere
				node = node->next;
			}
		}
		
		// controllo se ho già trovato il paziente
		if (!found) 
		{
			node = head;
			found = false;

			// secondo scorrimento, cerco il primo col codice massimo dentro max_code
			while(node != NULL && !found)
			{
				// controllo se ho trovato il primo codice maggiore
				if(node->codice == max_code)
				{
					found = true;
					nome = new char[21];
					
					strcpy(nome, node->nominativo);
					delete_node(node);
				}
				else
				{
					// se e solo se non ho trovato il paziente 
					// devo continuare a scorrere
					node = node->next;
				}
			}
		}
	}
	
	return r;
}

ProntoSoccorso::~ProntoSoccorso()
{
	// controllo che ci sia qualcosa da distruggere
	if (head != NULL && tail != NULL)
	{
		Paziente *node = head;
		Paziente *temp;

		while (node != NULL)
		{
			temp = node;
			node = temp->next;

			delete temp;
		}
		head = NULL;
		tail = NULL;
	}
}

ProntoSoccorso ProntoSoccorso::operator=(ProntoSoccorso ps)
{
	ProntoSoccorso new_ps = ProntoSoccorso();
	Paziente *node = ps.head;

	while(node != NULL)
	{
		new_ps.ricovero(node->nominativo, node->codice);
		node = node->next;
	}

	return new_ps;
}

ProntoSoccorso::ProntoSoccorso(ProntoSoccorso &old_ps)
{
	Paziente *node = old_ps.head;

	while (node != NULL)
	{
		this->ricovero(node->nominativo, node->codice);
		node = node->next;
	}
}