#include<iostream>

using namespace std;

// Tipi di codice
enum Codice {BIANCO, VERDE, GIALLO, ROSSO};

// Gestione della lista
struct Paziente
{
	// dati
	char nominativo[21];
	Codice codice;

	// puntatori ai nodi precedente e successivo
	Paziente* prev;
	Paziente* next;
};

class ProntoSoccorso
{
	private:
		Paziente* head;
		Paziente* tail;
		
	public:
		// Costruttore
		ProntoSoccorso();
		// Costruttore di copia
		ProntoSoccorso(ProntoSoccorso &old_ps);
		// Distruttore
		~ProntoSoccorso();

		// output
		friend ostream &operator<<(ostream &os, const ProntoSoccorso &ps);

		// elimina elemento lista
		void delete_node(Paziente *node);
		// Aggiungi paziente
		void ricovero(char *nome, Codice priority);
		// servi il prossimo paziente
		int prossimo(char*& nome);

		// Operatore Assegnamento
		ProntoSoccorso operator=(ProntoSoccorso ps); 
};
