#include "compito.h"
#include <string.h>

Display::Display(int L, int C)
{
    if (L < 1 || C < 1)
    {
        L = 5;
        C = 8;
    }
    height = L;
    length = C;
    cursor_row = 0;

    array = new char *[height];
    for (int i = 0; i < height; i++)
    {
        array[i] = new char[length];
        array[i][0] = '\0';
    }

}

Display::~Display()
{
    for (int i = 0; i < height; i++)
    {
        delete[] array[i];
    }

    delete array;
}

void Display::writeT(char input[])
{
    if (input != "" && input != NULL)
    {
        int len = strlen(input);
        if (cursor_row < height)
        {
            strcpy(array[cursor_row], input);
            cursor_row++;
        }
        else
        {
            cursor_row = 0;
            strcpy(array[cursor_row], input);
        }
    }
}

ostream &operator<<(ostream &os, const Display &d)
{
    //os << "roba";
    for (int i = 0; i < d.height; i++)
    {
        os << "[" << i + 1;

        if (d.cursor_row == i)
            os << ">";
        else
            os << "]";

        for (int j = 0; j < d.length && d.array[i][j] != '\0'; j++)
        {
            os << d.array[i][j];
        }
        os << endl;
    }

    return os;
}