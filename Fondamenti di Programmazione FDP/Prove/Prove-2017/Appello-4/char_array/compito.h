#include <iostream>
using namespace std;

class Display
{
private:
    int length, height;
    //int last_filled;
    int cursor_row;
    char** array;
    
public:
    /**
     * 
     * 
    **/
    Display(int, int);
    ~Display();
    friend ostream &operator<<(ostream &os, const Display &d);

    void writeT(char[]);
};

