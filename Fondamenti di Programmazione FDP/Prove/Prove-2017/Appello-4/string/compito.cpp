#include "compito.h"

Display::Display(int L, int C)
{
    if (L < 1 || C < 1) 
    {
        L = 5;
        C = 8;
    }
    height = L;
    length = C;
    cursor_row = 0;

    string_array = new string[height];
    
    for(int i = 0; i < height; i++)
    {
        string_array[i] = "";
    }

    //delete [] temp;
    /*
    string_array = new char*[height];

    for(int i = 0; i < height; i++)
    {
        string_array[i] = new char[length];
        string_array[i][0] = '\0';
    }
    */
}

Display::Display(const Display &to_copy)
{
    height = to_copy.height;
    length = to_copy.length;
    cursor_row = to_copy.cursor_row;

    string_array = new string[height];
    
    for(int i = 0; i < height; i++)
    {
        string_array[i] = to_copy.string_array[i];
    }
}

Display::~Display()
{
    delete [] string_array;
}

string check_string(char input[])
{
    return input ? input : "";
}

void Display::writeT(char input[])
{
    string checked_input = check_string(input);
    checked_input = input;
    
    if (checked_input != "")
    {
        // int len = checked_input.length();
        checked_input = checked_input.substr(0, length);
        if (cursor_row < height) 
        {
            string_array[cursor_row] = checked_input;
            //strcpy(string_array[cursor_row], input);
            cursor_row++;
        }
        else
        {
            cursor_row = 0;
            string_array[cursor_row] = checked_input;
            //strcpy(string_array[cursor_row], input);
        }
    }
}
void Display::writeW(char input[])
{
    string checked_input = check_string(input);
    if (checked_input != "")
    {
        int len = checked_input.length();
        int count = len % length + 1;
        
        for(int i = 0; i < count; i++)
        {
            string sub_string = checked_input.substr((i * length), ((i + 1) * length));
            if (cursor_row < height)
            {
                string_array[cursor_row] = sub_string;
                cursor_row++;
            }
            else
            {
                cursor_row = 0;
                string_array[cursor_row] = sub_string;
            }
        }
    }
}

// operator overload
ostream &operator<<(ostream &os, const Display &d)
{
    //os << "roba";
    for(int i = 0; i < d.height; i++)
    {
        os << "[" << i+1;
        
        if (d.cursor_row == i)
            os << ">";
        else
            os << "]";
        
        os << d.string_array[i];
        
        /*
        for(int j = 0; j < d.length && j < d.string_array[i].length(); j++)
        {
            os << d.string_array[i][j];
        }
        */

        os << "\n";
    }
    
    return os;
}
Display &Display::operator=(const Display &to_copy)
{
    height = to_copy.height;
    length = to_copy.length;
    cursor_row = to_copy.cursor_row;

    string_array = new string[height];

    for (int i = 0; i < height; i++)
    {
        string_array[i] = to_copy.string_array[i];
    }
}