#include <iostream>
#include <string>
using namespace std;

class Display
{
private:
    int length, height;
    //int last_filled;
    int cursor_row;
    //char** array;
    string* string_array;
    
public:
    // costruttori e distruttori
    Display(int, int);
    Display(const Display &to_copy);
    ~Display();

    // operator overload
    friend ostream &operator<<(ostream &os, const Display &d);
    Display &operator=(const Display &to_copy);

    // metodi
    void writeT(char[]);
    void writeW(char[]);
};

