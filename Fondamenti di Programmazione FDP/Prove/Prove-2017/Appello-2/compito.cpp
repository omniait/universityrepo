#include "compito.h"

AlberoDiNatale::AlberoDiNatale(int n_piani)
{
	n = n_piani;
	branches = new char*[n_piani];
	
	for(int i = 0; i<n_piani; i++)
	{
		//char* branch = new char[n_piani-i+1];
		branches[i] = new char[n_piani-i+1];
		
		for(int j = 0; j<n_piani-i; j++)
		{
			branches[i][j] = '-';
		}
		
		branches[i][n_piani-i+1] = '\0';
	}
}

void AlberoDiNatale::aggiungiPallina(char colour, int height, int branch)
{
	// Controllo sul colour (R || V || B)
	if(colour == 'R' || colour == 'V' || colour == 'B')
	{
		// Out of index ed invalid numbers
		if(height < n && height >= 0 && branch >= 0)
		{
			// Controllo sulla height(il piano)
			if(branches[height][branch] == '-' && (branches[height][branch-1] != colour && branches[height][branch + 1] != colour ))
				branches[height][branch] = colour;
		}
	}
}

ostream &operator<<(ostream& os, const AlberoDiNatale& a)
{
	for(int i = a.n; i >= 0; i--)
	{
		//os << ' ';
		for(int j = 0; j < i + 1 ; j++)
		{
			os << ' ';
		}
		
		for(int j = 0; j < a.n - i; j++)
		{
			os<<a.branches[i][j]<<' ';
		}
		os << endl;
	}
	for(int i = 0; i < a.n; i++)
	{
		os<<' ';
	}
	os << '|';
	
	return os;
}

AlberoDiNatale::~AlberoDiNatale()
{
	for(int i = 0; i<n; i++)
	{
		delete[] branches[i];
	}
	delete[] branches;
}


