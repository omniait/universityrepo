#include <iostream>
using namespace std;

class AlberoDiNatale
{
	private:
		// n = numero piani
		int n;
		// l = lunghezza massima, del primo piano
		// int l;
		// branches = indici dei vari "piani"
		char** branches;
	
	public:
		// Costruttore
		AlberoDiNatale(int n_piani);
		// Distruttore
		~AlberoDiNatale();
		// Aggiungi pallina
		void aggiungiPallina(char colour, int height, int branch);
		// Override cout
		friend ostream &operator<<(ostream& os, const AlberoDiNatale& a);
};
