#include <string.h>
#include <iostream>

using namespace std;

struct type_voci
{
    char id[6];
    unsigned int voto;
    unsigned int crediti;
};

class Libretto
{
    private:
        bool is_empty;
        bool is_full;
        unsigned int n_voci_allocated;

    public:
        unsigned int n_voci;
        type_voci *voci;

        Libretto(unsigned int N);
        ~Libretto();

        friend ostream &operator<<(ostream &, const Libretto &);
        Libretto &operator=(const Libretto &);

        bool aggiungi(char* id, unsigned int voto, unsigned int cfu);
        float media();
        float laurea();
};
