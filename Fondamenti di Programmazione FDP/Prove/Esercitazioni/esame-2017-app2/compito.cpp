#include <iostream>
#include <regex>
// #include <cstdlib>
#include <string>
// #include <math.h>
#include "compito.h"

using namespace std;

/**
 * Constructor
 * Params:
 * @int in_n_voci - how much type_voci must be allocated
**/
Libretto::Libretto(unsigned int in_n_voci)
{
    if (in_n_voci > 0) 
    {
        n_voci = in_n_voci;
        n_voci_allocated = 0;
        voci = new struct type_voci[n_voci];

        // Initialise every element of voci[] to avoid unexpected behaviour
        for(unsigned int i = 0; i < n_voci; i++)
        {
            // \0 used as a flag for empty, shouldn't be necessary
            voci[i].id[0] = '\0';
            voci[i].voto = 0;
            voci[i].crediti = 0;
        }
    }
}

/** 
 * Destructor
 * No explanation needed, deallocate the only dynamic structure
**/
Libretto::~Libretto()
{
    delete[] voci;
}

//! PORCO DIO
ostream &operator<<(ostream &os, const Libretto &l)
{
    for (int i = 0; i < l.n_voci; i++)
        os << "ESAME" << (i + 1) << ": <" << l.voci[i].id << ","
           << l.voci[i].voto << "," << l.voci[i].crediti << ">" << endl;

    return os;
}

/** 
 * bool Libretto.aggiungi()
 * Add a new element to voci
 * Parameters:
 * @char id[6] - The new element id, must be unique
 * @unsigned int voto - The new element voto
 * @unsigned int cfu - The new element credito
 * Variables:
 * @bool added - If an element has been succesfully added
 * @bool error - If there has been any kind of error
 * ---
 * @return added
**/ 
bool Libretto::aggiungi(char id[6], unsigned int voto, unsigned int cfu)
{
    bool added = false;
    bool error = false;

    unsigned int i = 0;

    string ch_voto = to_string(voto);
    // cout << "voto " << voto << " to char: " << ch_voto << endl;
    string ch_cfu = to_string(cfu);
    // cout << "cfu " << cfu << " to char: " << ch_cfu << endl;

    regex pattern_cfu("6|9|(12)");
    cout << regex_match(ch_cfu, pattern_cfu) << endl;

    regex pattern_voto("(^[0-9]{1}$|^[1]{1}[0-7]{1}$)");
    cout << regex_match(ch_voto, pattern_voto) << endl;
    //basic_regex pattern_voto("");

    if ((voto < 18 || (voto > 30 && voto != 33)) || (cfu != 6 && cfu != 9 && cfu != 12))
    {
        error = true;
    }
    
    while(i < n_voci && !error && !added)
    {
        // compare id
        if (!strcmp(voci[i].id, id))
        {
            error = true;
            // cout << "String Compare: " << strcmp(voci[i].id, id) << endl;
        }

        if (!error) 
        {
            // add voce         
            if (voci[i].id[0] == '\0') 
            {
                /*
                for(unsigned int j = 0; j < 5; j++)
                    voci[i].id[j] = id[j];
                */
                strcpy(voci[i].id, id);
                voci[i].voto = voto;
                voci[i].crediti = cfu; 
                added = true;
                is_empty = false;
                n_voci_allocated += 1;
                if (n_voci_allocated == n_voci)
                    is_full = true;
            }
        }
        i++;
    }
    return added;
}

/** 
 * float Libretto.media()
 * Calculate average between voci.voto
 * Parameters:
 * None
 * Variables:
 * @float r - The final result
 * @unsigned int sum - Sum between voci.voto
 * ---
 * @return r
**/
float Libretto::media()
{
    float r = -1.0;
    unsigned int sum = 0;
    
    if (!is_empty) 
    {
        for(unsigned int i = 0; i < n_voci; i++)
        {
            sum += voci[i].voto;
        }

        r = (float) (sum / n_voci_allocated);
    }
    
    return r;
}

//! odio
Libretto& Libretto::operator = (const Libretto& l){
  if (this != &l){
     if (n_voci != l.n_voci){
	    delete [] voci;
	    n_voci = l.n_voci;
	    voci = new type_voci[n_voci];	
	 }
     n_voci = l.n_voci;
     for (int i = 0; i < n_voci; i++)
        voci[i] = l.voci[i];
  }
  return *this;
}

/**
 * 
 * 
 * 
**/
float Libretto::laurea()
{
    /**
     * sommatoria dei prodotti tra cfu e voto, diviso la sommatoria dei cfu
     * ^^^^^^^^^^^^
     * (MEDIAPESATA * 3) + 22
    **/
    float r = 0;
    // unsigned prod_cfu_voto;
    unsigned int summ_prod_cfu_voto = 0;
    unsigned int summ_cfu = 0;

    if (is_full) 
    {
        for(unsigned int i = 0; i < n_voci; i++)
        {
            int cfu, voto;
            cfu = voci[i].crediti;
            voto = voci[i].voto;
            // prod_cfu_voto = cfu * voto;
            summ_prod_cfu_voto += cfu * voto;
            summ_cfu += cfu;
        }
        // cout << summ_prod_cfu_voto << " / " << summ_cfu << endl;
        // cout << ((float)summ_prod_cfu_voto / (float)summ_cfu) << endl;
        r = ((float)summ_prod_cfu_voto / (float)summ_cfu) * 3 + 22;
    }
    
    return r;
}

