// compito.h
#include <cstring>
#include <iostream>

using namespace std;

// lunghezza massima delle stringhe
const unsigned StringLength = 10;

// nodo della lista DLL
struct Node
{
        // messaggio
        char messaggio[StringLength+1];
        // puntatori al nodo successivo e precedente
        struct Node * next;
        struct Node * prev;
};

class Monitor
{
        private:
        // attributi:
        // le righe massime del buffer messaggi
        unsigned n_messaggi;
        // cursore che indica l'ultimo inserimento
        unsigned cursore;
        // puntatori per la lista
        // primo elemento
        Node * head;
        // ultimo elemento, utile per la sovrascrizione
        Node * tail;
        // metodi di gestione lista
        // cancella un dato nodo
        void delete_node(struct Node*);
        // inserisci in testa
        void push_front(const char*);
        // inserisci in coda, usato per la copia e la somma
        void push_back(const char*);
        
        public:
        // costruttore
        Monitor(int);
        // distruttore (seconda parte)
        ~Monitor();
        // metodi
        // nuovo messaggio
        void inserisci(const char*);
        // override output
        friend ostream& operator<<(ostream&, const Monitor&);
        // seconda parte
        // costruttore copia
        // l'originale non va modificato
        Monitor(const Monitor&);
        // override "somma" di monitor
        Monitor operator+(const Monitor&);
};
