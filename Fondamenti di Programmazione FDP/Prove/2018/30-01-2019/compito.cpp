// compito.cpp
#include "compito.h"

// costruttore
Monitor::Monitor(int n_righe)
{
	// controllo di n_righe
	if (n_righe <= 0)
		n_righe = 3; 
	
	// assegnamento lunghezza del buffer
	n_messaggi = n_righe;
	// assegnamento cursore per indicare quando il buffer è pieno
	cursore = 0;
	// puntatori iniziali
	head = tail = NULL;
}

// distruttore
Monitor::~Monitor()
{
	// nodi per ciclare e distruggere
	struct Node *current, *next;
	// inizio dalla testa
	current = head;
	// continua fino al puntatore NULL
	while (current != NULL)
	{
		// salva l'indirizzo del prossimo nodo
		next = current->next;
		// cancella nodo attuale
		delete current;
		// avanza al prossimo nodo
		current = next;
	}
	// resetta head e tail
	head = tail = NULL;
}

// cancella nodo
void Monitor::delete_node(struct Node *to_delete)
{
	// controlla che il nodo esista
	if (to_delete != NULL)
	{
		// se il nodo è la testa, aggiorno la testa
		if (head == to_delete)
			head = to_delete->next;
		// se il nodo è la coda, aggiorno la coda
		if (tail == to_delete)
			tail = to_delete->prev;
		// se il nodo non è l'ultimo, devo aggiornare il nodo successivo
		if (to_delete->next != NULL)
			to_delete->next->prev = to_delete->prev;
		// se il nodo non è il primo, devo aggiornare il nodo precedente
		if (to_delete->prev != NULL)
			to_delete->prev->next = to_delete->next;
		// infine elimino il nodo
		delete to_delete;
	}
}

// inserimento in coda
void Monitor::push_back(const char *messaggio)
{
	// creazione del nuovo nodo
	struct Node *new_node = new Node;
	// alloco il messaggio
	strncpy(new_node->messaggio, messaggio, StringLength);
	// per sicurezza l'ultimo carattere viene assegnato a '\000'
	new_node->messaggio[StringLength] = '\000';
	// il nuovo nodo sarà l'ultimo quindi
	// il successivo non punta a niente
	new_node->next = NULL;
	// il precedente è la coda corrente
	new_node->prev = tail;
	// se esiste la coda
	if (tail != NULL)
	{
		// aggiorna il nodo in coda
		tail->next = new_node;
	}
	// altrimenti la lista è vuota
	else
	{
		// quindi l'elemento è anche la testa
		head = new_node;
	}
	// aggiorno in ogni caso la coda
	tail = new_node;
}

// inserimento in testa
void Monitor::push_front(const char *messaggio)
{
	// creazione del nuovo nodo
	struct Node *new_node = new Node;
	// alloco il messaggio
	strncpy(new_node->messaggio, messaggio, StringLength);
	// per sicurezza l'ultimo carattere viene assegnato a '\000'
	new_node->messaggio[StringLength] = '\000';
	// il nuovo nodo sarà il primo quindi
	// il successivo è la testa corrente
	new_node->next = head;
	// il precedente non punta a niente
	new_node->prev = NULL;
	// se esiste la testa
	if (head != NULL)
	{
		// aggiorna la testa
		head->prev = new_node;
	}
	// altrimenti la lista è vuota
	else
	{
		// quindi il nuovo elemento è anche la coda
		tail = new_node;
	}
	// aggiorno in ogni caso la testa
	head = new_node;
}

// metodo wrapper per alcuni controlli di base
void Monitor::inserisci(const char *messaggio)
{
	// controlla che il messaggio non sia NULL
	// il controllo che non sia vuoto non funziona direttamente 
	// (!= "", != '\0' e !='\000' danno sempre true nella chiamata del main a riga 65)
	// uso un controllo diverso per verificarlo, ovvero controllo che ci sia almeno 1 carattere
	if (messaggio != NULL && strlen(messaggio) > 1)
	{
		// se il cursore non è arrivato a n_messaggi
		if (cursore < n_messaggi)
		{
			// allora il buffer non è pieno quindi
			// aggiungi normalmente in testa
			push_front(messaggio);
			// sposta il cursore più in basso
			cursore++;
		}
		// altrimenti devo sovrascrivere
		else
		{
			// cancella l'ultimo messaggio, il meno recente (la coda)
			delete_node(tail);
			// aggiungi in testa il nuovo messaggio
			push_front(messaggio);
		}
	}
}

// overload dell'output
ostream &operator<<(ostream &os, const Monitor &MonitorObject)
{
	// scrivi la capienza del buffer messaggi
	os << '[' << MonitorObject.n_messaggi << ']' << endl;
	// parti dalla testa per scorrere
	struct Node *node = MonitorObject.head;
	// continua finché non incontri NULL
	while (node != NULL)
	{
		// scrivi il messaggio
		os << node->messaggio << endl;
		// prosegui
		node = node->next;
	}
	return os;
}

// costruttore di copia
Monitor::Monitor(const Monitor &to_copy)
{
	// copia la capienza del buffer messaggi
	n_messaggi = to_copy.n_messaggi;
	// imposta il cursore
	cursore = to_copy.cursore;
	// inizializza a NULL testa e coda
	head = tail = NULL;

	// scorrimento
	struct Node *node = to_copy.head;
	while (node != NULL)
	{
		// aggiungi in coda i nuovi nodi in quanto già ordinati e controllati
		push_back(node->messaggio);
		node = node->next;
	}
}

// overload della somma
Monitor Monitor::operator+(const Monitor &m1)
{
	// n_messaggi è uguale alla somma delle capienze
	int new_n_messaggi = n_messaggi;
	new_n_messaggi += m1.n_messaggi;
	// costruisci un nuovo monitor
	Monitor NewMonitor(new_n_messaggi);
	// nodo per scorrere le liste
	struct Node *node;
	// scorri e copia il primo monitor
	node = m1.head;
	while (node != NULL)
	{
		NewMonitor.push_back(node->messaggio);
		// fai proseguire il cursore
		NewMonitor.cursore++;
		node = node->next;
	}
	// scorri e copia il secondo monitor
	node = head;
	while (node != NULL)
	{
		NewMonitor.push_back(node->messaggio);
		// fai proseguire il cursore
		NewMonitor.cursore++;
		node = node->next;
	}
	// restituisci il risultato
	return NewMonitor;
}
