#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    //char s1[] = "asdf";
    //char* s2 = "xzcb";

    //cout << s1 << endl;
    //cout << s2 << endl;

    char s1[] = "Pippo";
    char s2[] = {'p', 'o', 'i', '\0'};
    char * s3 = "ieri";
    char * s4 = "domani";

    cout << s1 << endl;
    cout << s2 << endl;
    cout << s3 << endl;
    cout << s4 << endl;

    //s2 = s3; // errore!1!1!1
    s4 = s3;

    cout << s3 << endl;
    cout << s4 << endl;

    s4 = "Porco dio";
    
    cout << s3 << endl;
    cout << s4 << endl;

    cout << (void*)s3 << endl;

    return 0;
}
