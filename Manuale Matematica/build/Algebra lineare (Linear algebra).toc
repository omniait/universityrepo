\contentsline {section}{Algebra lineare (Linear algebra)}{1}{section*.1}% 
\contentsline {subsection}{Algebra delle matrici (Matrix algebra)}{2}{section*.3}% 
\contentsline {subsubsection}{Sistemi lineari con matrici}{3}{section*.4}% 
\contentsline {paragraph}{Dal sistema alla matrice}{3}{section*.5}% 
\contentsline {paragraph}{Operazioni sulle righe}{3}{section*.6}% 
\contentsline {paragraph}{Eliminazione tramite Gauss, forma a scalini o row-echelon form}{4}{section*.7}% 
\contentsline {subparagraph}{Risoluzione algoritmica dei sistemi}{4}{section*.8}% 
\contentsline {subsubsection}{Somma tra matrici}{6}{section*.9}% 
\contentsline {paragraph}{Propriet\IeC {\`a}}{7}{section*.10}% 
\contentsline {subsubsection}{Prodotto scalare-matrice}{7}{section*.11}% 
\contentsline {paragraph}{Propriet\IeC {\`a}}{7}{section*.12}% 
\contentsline {subsubsection}{Matrice zero}{7}{section*.13}% 
\contentsline {paragraph}{Opposta di una matrice}{8}{section*.14}% 
\contentsline {subsubsection}{Prodotto matrice-matrice}{8}{section*.15}% 
\contentsline {paragraph}{Prodotto scalare (dot product)}{8}{section*.16}% 
\contentsline {paragraph}{Propriet\IeC {\`a}}{9}{section*.17}% 
\contentsline {subsubsection}{Matrice identit\IeC {\`a}}{9}{section*.18}% 
\contentsline {paragraph}{Propriet\IeC {\`a}}{9}{section*.19}% 
\contentsline {subsubsection}{Matrice di trasformazione, trasformazioni lineari}{9}{section*.20}% 
\contentsline {subsubsection}{Matrici inverse e invertibilit\IeC {\`a}}{11}{section*.21}% 
\contentsline {paragraph}{Determinante}{11}{section*.22}% 
\contentsline {subparagraph}{Determinante matrice 2x2}{11}{section*.23}% 
\contentsline {subparagraph}{Determinante matrice 3x3}{11}{section*.24}% 
\contentsline {subparagraph}{Determinante matrice 4x4}{12}{section*.25}% 
\contentsline {paragraph}{Concetto di reciproco con le matrici}{12}{section*.26}% 
\contentsline {paragraph}{Inversa di una matrice ed invertibilit\IeC {\`a}}{12}{section*.27}% 
\contentsline {subsubsection}{Regola o teorema di Cramer}{13}{section*.28}% 
\contentsline {subsection}{Algebra lineare, teoria e pratica (Linear algebra theory and practice)}{13}{section*.29}% 
\contentsline {subsubsection}{Tabella convenzioni}{13}{section*.30}% 
\contentsline {subsubsection}{Vettori}{13}{section*.31}% 
\contentsline {paragraph}{Interpretazione algebrica e geometrica dei vettori}{13}{section*.32}% 
\contentsline {paragraph}{Somma e sottrazione tra vettori}{16}{section*.33}% 
\contentsline {paragraph}{Prodotto scalare-vettore}{17}{section*.34}% 
\contentsline {subparagraph}{Propriet\IeC {\`a}}{18}{section*.35}% 
\contentsline {paragraph}{Prodotto scalare tra vettori}{18}{section*.36}% 
\contentsline {subparagraph}{Propriet\IeC {\`a}}{19}{section*.37}% 
\contentsline {paragraph}{Lunghezza del vettore}{19}{section*.38}% 
\contentsline {subparagraph}{Definizione}{19}{section*.39}% 
\contentsline {subparagraph}{Interpretazione geometrica}{19}{section*.40}% 
\contentsline {subparagraph}{Dimostrazione dell'uguaglianza tra le definizioni algebrica e geometrica della norma di un vettore}{20}{section*.41}% 
\contentsline {paragraph}{Prodotto cartesiano/outer product}{20}{section*.42}% 
\contentsline {paragraph}{Vettori e numeri complessi}{20}{section*.43}% 
\contentsline {subparagraph}{Vettori complessi}{21}{section*.44}% 
\contentsline {subparagraph}{Coniugato di un numero complesso}{21}{section*.45}% 
\contentsline {subparagraph}{Trasposta di Hermite o la trasposta di un coniugato}{21}{section*.46}% 
\contentsline {paragraph}{Vettore unitario}{22}{section*.47}% 
\contentsline {subsubsection}{Dimensioni e spazio vettoriale}{22}{section*.48}% 
\contentsline {paragraph}{Breve definizione}{22}{section*.49}% 
\contentsline {paragraph}{Sottospazi}{22}{section*.50}% 
\contentsline {paragraph}{Sottoinsiemi}{23}{section*.51}% 
\contentsline {paragraph}{Indipendenza lineare}{24}{section*.53}% 
\contentsline {paragraph}{Base di un sottospazio}{26}{section*.54}% 
\contentsline {subsubsection}{Matrici}{27}{section*.55}% 
\contentsline {paragraph}{Introduzione alle matrici}{27}{section*.56}% 
\contentsline {paragraph}{Tipi di matrici}{28}{section*.57}% 
\contentsline {paragraph}{Somma di matrici e prodotto scalare-matrice}{28}{section*.58}% 
\contentsline {paragraph}{Trasposta di una matrice}{29}{section*.59}% 
\contentsline {paragraph}{Matrici complesse}{30}{section*.60}% 
\contentsline {paragraph}{Diagonale e traccia di matrice}{30}{section*.61}% 
\contentsline {paragraph}{Moltiplicazione matrice-matrice}{30}{section*.62}% 
\contentsline {paragraph}{Moltiplicazione matrice-vettore}{32}{section*.63}% 
\contentsline {paragraph}{Matrice identit\IeC {\`a} o identica e zero}{34}{section*.64}% 
\contentsline {paragraph}{Matrice simmetrica}{34}{section*.65}% 
\contentsline {paragraph}{Prodotto di Hadamard/Schur (approfondimento)}{35}{section*.66}% 
\contentsline {paragraph}{Prodotto di matrici simmetriche}{35}{section*.67}% 
\contentsline {paragraph}{Divisione tra matrici}{36}{section*.68}% 
\contentsline {paragraph}{Rango di una matrice}{36}{section*.69}% 
\contentsline {paragraph}{Spazi della matrice}{37}{section*.70}% 
\contentsline {subsubsection}{Risolvere sistemi lineari (sintesi dei concetti importanti dell'algebra delle matrici)}{38}{section*.71}% 
\contentsline {paragraph}{Dal sistema lineare alla matrice}{38}{section*.72}% 
\contentsline {paragraph}{Eliminazione di Gauss-Jordan}{38}{section*.73}% 
\contentsline {subsubsection}{Determinante}{39}{section*.74}% 
\contentsline {subsubsection}{Matrice inversa}{40}{section*.75}% 
\contentsline {subsubsection}{Proiezioni ed ortogonalizzazione}{42}{section*.76}% 
\contentsline {paragraph}{Proiezioni in \(^2\) ed \(^n\)}{42}{section*.77}% 
\contentsline {paragraph}{Vettori ortogonali e paralleli e scomposizione}{43}{section*.78}% 
\contentsline {paragraph}{Ortogonalit\IeC {\`a} e ortonormalit\IeC {\`a}}{44}{section*.79}% 
\contentsline {paragraph}{Ortogonalizzazione di Gram-Schmidt e scomposizione QR}{45}{section*.80}% 
\contentsline {paragraph}{Matrice inversa tramite scomposizione QR (approfondimento)}{46}{section*.81}% 
\contentsline {subsubsection}{Eigendecomposition}{46}{section*.82}% 
\contentsline {paragraph}{Spiegazione intuitiva}{46}{section*.83}% 
\contentsline {paragraph}{Trovare gli eigenvalues}{47}{section*.84}% 
\contentsline {paragraph}{Trovare gli eigenvectors}{48}{section*.85}% 
\contentsline {subsubsection}{Forma quadratica}{49}{section*.86}% 
\contentsline {subsection}{Ricette veloci}{49}{section*.87}% 
\contentsline {subsubsection}{Prodotto scalare (dot product)}{49}{section*.88}% 
\contentsline {subsubsection}{Norma/lunghezza di un vettore}{49}{section*.89}% 
\contentsline {subsubsection}{Riduzione in forma a scalini di una matrice}{49}{section*.90}% 
\contentsline {subsubsection}{Proiezione vettore su vettore}{49}{section*.91}% 
\contentsline {subsubsection}{Proiezione vettore su span}{50}{section*.92}% 
\contentsline {subsubsection}{Rette in forma implicita/cartesiana}{50}{section*.93}% 
\contentsline {subsubsection}{Rette in forma esplicita/parametrica}{50}{section*.94}% 
\contentsline {subsubsection}{Terminologia rette}{51}{section*.95}% 
\contentsline {subsubsection}{Retta ortogonale a due rette nello spazio}{51}{section*.96}% 
\contentsline {subsubsection}{Distanza minima tra due rette}{51}{section*.97}% 
\contentsline {subsubsection}{Matrice di rotazione}{51}{section*.98}% 
\contentsline {subsubsection}{Determinante}{51}{section*.99}% 
\contentsline {subsubsection}{Rango di una matrice}{51}{section*.100}% 
\contentsline {subsubsection}{Nucleo}{51}{section*.101}% 
\contentsline {subsubsection}{Area del parallelogramma/triangolo}{52}{section*.102}% 
\contentsline {subsubsection}{Iniettivit\IeC {\`a} e surgettivit\IeC {\`a} di un'applicazione lineare}{52}{section*.103}% 
\contentsline {subsubsection}{Cambio di base}{52}{section*.104}% 
\contentsline {subsubsection}{Autovalori, autovettori e autospazio}{53}{section*.105}% 
\contentsline {subsubsection}{Base Spettrale}{53}{section*.106}% 
\contentsline {subsubsection}{Matrice diagonalizzabile}{53}{section*.107}% 
\contentsline {subsubsection}{Complemento ortogonale}{53}{section*.108}% 
\contentsline {subsubsection}{Forma quadratica}{54}{section*.109}% 
\contentsline {subsection}{Collezione di definizioni}{54}{section*.110}% 
\contentsline {subsubsection}{Sistema lineare}{54}{section*.111}% 
\contentsline {subsubsection}{Spazio euclideo in \(^n\)}{54}{section*.112}% 
\contentsline {subsubsection}{Somma di vettori}{55}{section*.113}% 
\contentsline {subsubsection}{Prodotto scalare-vettore}{55}{section*.114}% 
\contentsline {subsubsection}{Propriet\IeC {\`a} spazio vettoriale di somma tra vettori e prodotto scalare-vettore}{55}{section*.115}% 
\contentsline {subsubsection}{Altri assiomi}{55}{section*.116}% 
\contentsline {subsubsection}{Norma/magnitudo/lunghezza}{55}{section*.117}% 
\contentsline {subsubsection}{Versore}{56}{section*.118}% 
\contentsline {subsubsection}{Sfera aperta e chiusa}{56}{section*.119}% 
\contentsline {subsubsection}{Prodotto scalare \(^n\)}{56}{section*.120}% 
\contentsline {subsubsection}{Coseno dell'angolo fra vettori non nulli}{56}{section*.121}% 
\contentsline {subsubsection}{Coniugato complesso}{56}{section*.122}% 
\contentsline {subsubsection}{Prodotto scalare in \(\C ^n\)}{56}{section*.123}% 
