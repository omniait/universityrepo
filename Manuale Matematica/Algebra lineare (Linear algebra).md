# Algebra lineare (Linear algebra)

[TOC]

## Algebra delle matrici (Matrix algebra)

Questo capitolo si concentra sugli aspetti algebrici delle matrici, quindi operazioni, sistemi, determinante, tralasciando la teoria per dopo.

Una matrice è un insieme di vettori, un vettore è una lista ordinata di valori, questi valori hanno un indice.

Per ora consideriamo solo elementi in $\R$ e matrici in $\R^n$, l’espansione a $\C^n​$ avverrà nei capitoli dopo l’introduzione.

Per trovare un elemento di una matrice ci riferiamo prima alla riga e poi alla colonna.

Una serie di esempi:
$$
\begin{align}
A_{2,2} = 
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix} & \quad
B_{2,3} = 
\begin{bmatrix}
a & b & c \\
d & e & f
\end{bmatrix} & \quad
C_{3,2} = 
\begin{bmatrix}
a & b \\
c & d \\
e & f
\end{bmatrix} \\
D_{1,5} = 
\begin{bmatrix}
a & b & c & d & e
\end{bmatrix} & \quad
E_{3,1} =
\begin{bmatrix}
a \\
b \\
c
\end{bmatrix}
\end{align}
$$
In particolare la matrice $D$ è un vettore riga e la matrice $E$ è un vettore colonna.

In generale una matrice è definita da:
$$
A_{m,n} = 
 \begin{bmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
  a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{m,1} & a_{m,2} & \cdots & a_{m,n} 
 \end{bmatrix} \in \R^{m \times n}
$$



### Sistemi lineari con matrici

#### Dal sistema alla matrice

L’algebra lineare si occupa principalmente di risolvere sistemi lineari, infatti i sistemi lineari dell’algebra sono rappresentabili con le matrici.

Per esempio:
$$
\begin{cases}
3x + 2y = 7 \\
x - 6y = 0
\end{cases} = 
\begin{bmatrix}
3 & 2 & 7 \\
1 & -6 & 0
\end{bmatrix} \\
\begin{cases}
-2x + y - t = 7 \\
x - y +z +4t = 0
\end{cases} =
\begin{bmatrix}
-2 & 1 & 0 & -1 & 7 \\
1 & -1 & 1 & 4 & 0
\end{bmatrix}
$$
Ogni riga del sistema lineare si traduce in una riga della matrice, ogni variabile del sistema lineare si traduce in una colonna, l’ultima colonna a destra della matrice di norma contiene le costanti.

È possibile riordinare le variabili nella matrice, questo significa spostare le colonne.

Esempio con un sistema non ordinato:
$$
\begin{cases}
2x + 3y - z = 11 \\
7y = 6 - x - 4z \\
-8z + 3 = y
\end{cases} =
\begin{cases}
2x + 3y - z = 11 \\
x + 7y + 4z = 6 \\
0x -y -8z = -3
\end{cases} =
\begin{bmatrix}
2 & 3 & -1 & 11 \\
1 & 7 & 4 & 6 \\
0 & -1 & -8 & -3
\end{bmatrix}
$$

#### Operazioni sulle righe

Sulle righe delle matrici è possibile effettuare diverse operazioni.

* Scambiare le righe

$$
\begin{bmatrix}
3 & 2 & 7 \\
1 & -6 & 0
\end{bmatrix} 
\to^{R_1 \leftrightarrow R_2} \to 
\begin{bmatrix}
1 & -6 & 0 \\
3 & 2 & 7 
\end{bmatrix}
$$

* Moltiplicare (e dividere) per una costante (diversa da $0$ ovviamente)

$$
\begin{bmatrix}
3 & 2 & 7 \\
1 & -6 & 0
\end{bmatrix} 
\to^{R_1 \to 2R_1} \to 
\begin{bmatrix}
6 & 4 & 14 \\
1 & -6 & 0
\end{bmatrix}
$$

* Somma tra righe

$$
\begin{bmatrix}
3 & 2 & 7 \\
1 & -6 & 0
\end{bmatrix} 
\to^{R_1 + R_2 \to R_1} \to 
\begin{bmatrix}
4 & -4 & 7 \\
1 & -6 & 0
\end{bmatrix}
$$

#### Eliminazione tramite Gauss, forma a scalini o row-echelon form

Una matrice è a scalini, triangolare o row-echelon form quando:

* Tutte le righe che consistono di soli 0 sono in fondo (le righe con 0 possono essere eliminate)
* Ogni colonna non-zero in ogni riga è a destra della prima colonna non-zero delle righe precedenti

In altre parole la diagonale principale, formata dagli elementi $a_{m,n}$ deve avere a sinistra solo $0$.

Per esempio:
$$
\begin{bmatrix}
\bold{4} & 1 & 0 & = & 17 \\
0 & \bold{2} & 5 & = & 10 \\
0 & 0 & \bold{-3} & = & 2
\end{bmatrix}
$$
In generale:
$$
A_{m,n} = 
 \begin{bmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,n}& C_1 \\
  0 & a_{2,2} & \cdots & a_{2,n}& C_2 \\
  \vdots  & \vdots  & \ddots & \vdots & \vdots \\
  0 & 0 & \cdots & a_{m,n}& C_m 
 \end{bmatrix}
$$
Questa è la forma di Gauss, è possibile risolvere il sistema con la back-substitution a partire da $a_{m,n}$ .

Gli elementi indicati col grassetto sono chiamati pivot, le colonne che contengono i pivot sono chiamate colonne pivot (accento o inglese o francese), hanno di particolare che hanno zeri sotto e a sinistra, non è necessario che siano sulla diagonale principale.

La forma di Gauss-Jordan che costituisce una matrice identica, è una forma ridotta dove la diagonale principale $a_{m,n}$ ha solo 1.

Di solito se ne parla con matrici quadrate ($m = n​$), ma il concetto è del tutto identico anche per matrici rettangolari ($m \ne n​$), la diagonale con elementi di valore $1​$ arriva “fin dove può”, ovvero finché non finiscono le righe o le colonne.
$$
A_{m,n} = 
 \begin{bmatrix}
  1 & 0 & \cdots & 0 & C_1 \\
  0 & 1 & \cdots & 0 & C_2 \\
  \vdots  & \vdots  & \ddots & \vdots & \vdots \\
  0 & 0 & \cdots & 1 & C_m 
 \end{bmatrix}
$$

##### Risoluzione algoritmica dei sistemi

L’eliminazione di Gauss-Jordan, che porta ad avere una matrice quadrata, è a tutti gli effetti un algoritmo, infatti possiamo identificare 4 o 5 passaggi:

1. (opzionale): ridurre al minimo le righe, ovvero portarle ad essere irriducibili, con elementi fra loro indivisibili
2. Se l’n.esimo elemento della n.esima riga è 0, scambialo con un’altra riga che ha come n.esimo elemento un numero diverso da 0; altrimenti procedi
3. Moltiplica la n.esima riga per uno scalare in modo che il primo elemento sia 1
4. Somma dei multipli della n.esima riga a tutte le altre righe per ottenere zero sulla n.esima colonna
5. Ripeti i passaggi dal 2 finché non finiscono le righe, incrementa n

Esempio:
$$
\begin{bmatrix}
-1 & -5 & 1 & = & 17 \\
-5 & -5 & 5 & = & 5 \\
2 & 5 & -3 & = & -10
\end{bmatrix}
$$

* Prima riga, primo passo

$$
\begin{bmatrix}
-1 & -5 & 1 & = & 17 \\
-5 & -5 & 5 & = & 5 \\
2 & 5 & -3 & = & -10
\end{bmatrix} 
\to^{R_2 \to R_2/5}\to
\begin{bmatrix}
-1 & -5 & 1 & = & 17 \\
-1 & -1 & 1 & = & 1 \\
2 & 5 & -3 & = & -10
\end{bmatrix}
$$

* Prima riga, secondo passo (non necessario)
* Prima riga, terzo passo

$$
\begin{bmatrix}
-1 & -5 & 1 & = & 17 \\
-1 & -1 & 1 & = & 1 \\
2 & 5 & -3 & = & -10
\end{bmatrix} 
\to^{R_1 \to -R_1}\to
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
-1 & -1 & 1 & = & 1 \\
2 & 5 & -3 & = & -10
\end{bmatrix}
$$

* Prima riga, quarto passo

$$
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
-1 & -1 & 1 & = & 1 \\
2 & 5 & -3 & = & -10
\end{bmatrix} \to^{R_2 \to R_2 + R_1}\to
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
0 & 4 & 0 & = & -16 \\
2 & 5 & -3 & = & -10
\end{bmatrix} \\
\to^{R_3 \to R_3 - 2 R_1}\to
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
0 & 4 & 0 & = & -16 \\
0 & -5 & -1 & = & 24
\end{bmatrix}
$$

* Seconda riga, secondo passo (non necessario)
* Seconda riga, terzo passo

$$
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
0 & 4 & 0 & = & -16 \\
0 & -5 & -1 & = & 24
\end{bmatrix} \to^{R_2 \to R_2/4}\to
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
0 & 1 & 0 & = & -4 \\
0 & -5 & -1 & = & 24
\end{bmatrix}
$$

* Seconda riga, quarto passo

$$
\begin{bmatrix}
1 & 5 & -1 & = & -17 \\
0 & 1 & 0 & = & -4 \\
0 & -5 & -1 & = & 24
\end{bmatrix} 
\to^{R_1 \to R_1 - 5 R_2}\to
\begin{bmatrix}
1 & 0 & -1 & = & 3 \\
0 & 1 & 0 & = & -4 \\
0 & -5 & -1 & = & 24
\end{bmatrix} \\
\to^{R_3 \to R_3 + 5 R_2}\to
\begin{bmatrix}
1 & 0 & -1 & = & 3 \\
0 & 1 & 0 & = & -4 \\
0 & 0 & -1 & = & 4
\end{bmatrix}
$$

A questo punto il sistema sarebbe già risolto, è in forma gaussiana, ed è possibile usare la back-substitution, infatti:
$$
\begin{cases}
x + 0y -z = 3 \\
0x + y +0z = -4 \\
0x + 0y -z = 4
\end{cases} 
\iff
\begin{cases}
x -z = 3 \\
y = -4 \\
z = -4
\end{cases} 
\iff
\begin{cases}
x -(-4) = 3 \\
y = -4 \\
z = -4
\end{cases} 
\iff
\begin{cases}
x = 3 - 4 = -1 \\
y = -4 \\
z = -4
\end{cases}
$$
Continuando invece con Gauss-Jordan:

* Terza riga, secondo passo (non necessario)
* Terza riga, terzo passo

$$
\begin{bmatrix}
1 & 0 & -1 & = & 3 \\
0 & 1 & 0 & = & -4 \\
0 & 0 & -1 & = & 4
\end{bmatrix}
\to^{R_3 \to -R_3}\to
\begin{bmatrix}
1 & 0 & -1 & = & 3 \\
0 & 1 & 0 & = & -4 \\
0 & 0 & 1 & = & -4
\end{bmatrix}
$$

* Terza riga, quarto passo

$$
\begin{bmatrix}
1 & 0 & -1 & = & 3 \\
0 & 1 & 0 & = & -4 \\
0 & 0 & 1 & = & -4
\end{bmatrix}
\to^{R_1 \to R_1 + R_3}
\begin{bmatrix}
1 & 0 & 0 & = & -1 \\
0 & 1 & 0 & = & -4 \\
0 & 0 & 1 & = & -4
\end{bmatrix}
$$

A questo punto abbiamo finito e la soluzione coincide con quella verificata dopo la seconda riga.

### Somma tra matrici

Per sommare (e sottrarre) le matrici è necessario che abbiano le stesse dimensioni, ogni elemento della prima matrice viene sommato con l’elemento della seconda matrice con indici uguali, quindi:
$$
A_{m,n} = 
 \begin{bmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
  a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{m,1} & a_{m,2} & \cdots & a_{m,n} 
 \end{bmatrix} \quad
 B_{m,n} = 
 \begin{bmatrix}
  b_{1,1} & b_{1,2} & \cdots & b_{1,n} \\
  b_{2,1} & b_{2,2} & \cdots & b_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  b_{m,1} & b_{m,2} & \cdots & b_{m,n} 
 \end{bmatrix} \\
A + B =
 \begin{bmatrix}
  a_{1,1} + b_{1,1} & a_{1,2} + b_{1,2} & \cdots & a_{1,n} + b_{1,n} \\
  a_{2,1} + b_{2,1} & a_{2,2} + b_{2,2} & \cdots & a_{2,n} + b_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{m,1} + b_{m,1} & a_{m,2} + b_{m,2} & \cdots & a_{m,n} + b_{m,n} 
 \end{bmatrix}
$$

#### Proprietà

La somma (addizione) tra matrici è commutativa ed associativa.

* $A + B = B + A​$
* $(A + B) + C = A + (B + C)$

La sottrazione tra matrici non è né commutativa né associativa.

* $A - B \ne B - A$
* $(A - B) - C \ne A - (B - C)​$

### Prodotto scalare-matrice

Qualunque matrice è moltiplicabile per uno scalare, ovvero un numero costante, il numero scalare moltiplica per sé stesso ogni elemento della matrice, restituendo alla matrice quindi l’elemento per lo scalare.
$$
\begin{align}
& k \in \R \quad A \in \R^{m \times n} \\
& A_{m,n} = 
 \begin{bmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
  a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{m,1} & a_{m,2} & \cdots & a_{m,n} 
 \end{bmatrix} \\
& k A = 
 \begin{bmatrix}
  k \times a_{1,1} & k \times a_{1,2} & \cdots & k \times a_{1,n} \\
  k \times a_{2,1} & k \times a_{2,2} & \cdots & k \times a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  k \times a_{m,1} & k \times a_{m,2} & \cdots & k \times a_{m,n} 
 \end{bmatrix}
\end{align}
$$

#### Proprietà

Il prodotto scalare per matrice è commutativo, associativo e lineare (praticamente è la distributiva).

* $kA = Ak$
* $(k_0 k_1) A = k_0(k_1A)$
* $k (A + B) = kA + kB​$

### Matrice zero

La matrice zero, indicata con $O_{m,n}​$ o semplicemente $O ​$, è una matrice riempita solo di $0​$, cioè ogni suo elemento $a_{m,n} = 0​$.

#### Opposta di una matrice

Una matrice opposta ad $A$ è il risultato di $A \times (-1) = -A $, quindi:
$$
A_{m,n} = 
\begin{bmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
  a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{m,1} & a_{m,2} & \cdots & a_{m,n} 
\end{bmatrix} \iff 
-A_{m,n} = 
\begin{bmatrix}
  -a_{1,1} & -a_{1,2} & \cdots & -a_{1,n} \\
  -a_{2,1} & -a_{2,2} & \cdots & -a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  -a_{m,1} & -a_{m,2} & \cdots & -a_{m,n} 
\end{bmatrix}
$$
Questo significa che $A + (-A) = O$

Un altro modo per ottenere la matrice zero è moltiplicare una qualunque matrice con $0$, ovvero: $0 A_{m,n} = O_{m,n}$.

### Prodotto matrice-matrice

Il prodotto matrice-matrice è più complicato, non vale la proprietà commutativa perché infatti sono importanti le dimensioni delle matrici.

Perché si possa fare il prodotto tra due matrici la prima matrice deve avere lo stesso numero di colonne delle righe della seconda matrice, quindi: $A_{m,n} B_{n,p}$.

Per effettuare la moltiplicazione tra matrici si prendono le righe della prima matrice per le colonne della seconda matrice facendone la somma.

Ovvero:
$$
A_{m,n} = 
\begin{bmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
  a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{m,1} & a_{m,2} & \cdots & a_{m,n} 
\end{bmatrix}
\quad
B_{n,p} =
\begin{bmatrix}
  b_{1,1} & b_{1,2} & \cdots & b_{1,p} \\
  b_{2,1} & b_{2,2} & \cdots & b_{2,p} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  b_{n,1} & b_{n,2} & \cdots & b_{n,p} 
\end{bmatrix} \\
C_{m,p} = AB \implies c_{ij} = \sum^{n}_{k=1} a_{ik}b_{kj} \\
c_{1,1} = a_{1,1}b_{1,1} + a_{1,2}b_{2,1} + a_{1,3}b_{3,1} + ... + a_{1n}b_{n1} \\ c_{1,2} = a_{1,1}b_{1,2} + a_{1,2}b_{2,2} + a_{1,3}b_{3,2} + ... + a_{1n}b_{n2} \\
etc.
$$

#### Prodotto scalare (dot product)

Il dot product (indicato con $\cdot​$), o prodotto scalare, è un’operazione che moltiplica una riga per una colonna o due vettori di dimensione identica, le dimensioni della riga e della colonna devono coincidere.
$$
\R^n \times \R^n \to \R \\
x \cdot y = x_1 y_1 + x_2 y_2 + \dots + x_n y_n \in \R
$$
Il prodotto scalare è usato per il prodotto matrice per matrice.

#### Proprietà

* Il prodotto matrice-matrice non è commutativo:
	* $A \in \R^{n,m}, B \in \R^{m,p} \quad AB \ne BA$
* Il prodotto matrice-matrice è associativo:
	* $(AB)C = A(BC)$
* Il prodotto matrice-matrice è distributivo sulla somma e sulla differenza:
	* $A(B+C) = AB + AC​$
	* $(B+C)A = BA + CA$

### Matrice identità

La matrice identità è l’unica matrice che moltiplicata ad una seconda matrice non cambia la seconda matrice.

La matrice identità è una matrice quadrata, indicata con $I_n ​$.
$$
I_n = 
\begin{bmatrix}
1 & 0 & \dots & 0 \\
0 & 1 & \dots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \dots & 1
\end{bmatrix}
$$

#### Proprietà

La moltiplicazione tra una matrice e la matrice identità è commutativa, ma la matrice identità potrà avere dimensioni diverse in base all'ordine, infatti per $IA$, $I$ avrà come dimensione il numero di righe di $A$, invece per $AI$, $I$ avrà come dimensione il numero di colonne di $A$.

### Matrice di trasformazione, trasformazioni lineari

Le matrici sono utili a rappresentare le trasformazioni lineari di punti, vettori o anche altre matrici nello spazio, la matrice di trasformazione rappresenta esattamente come ogni punto del piano deve muoversi.

Esempio:

![1550327855008](assets/1550327855008.png)

Definiamo il vettore $\overrightarrow{a} = \begin{bmatrix} 3 \\ 2 \end{bmatrix}$ ed una matrice di trasformazione $M = \begin{bmatrix} -2 & 4 \\ 3 & 0 \end{bmatrix}$.

Per applicare la trasformazione basta eseguire la moltiplicazione:
$$
M \overrightarrow{a} = 
\begin{bmatrix}
-2 & 4 \\
3 & 0
\end{bmatrix}
\begin{bmatrix}
3 \\
2
\end{bmatrix} =
\begin{bmatrix}
-2 \times 3 + 4 \times 2 \\
3 \times 3 + 0 \times 2
\end{bmatrix} =
\begin{bmatrix}
2 \\
9
\end{bmatrix}
$$
![1550328268118](assets/1550328268118.png)

### Matrici inverse e invertibilità

#### Determinante

Mentre una matrice è indicata con le parentesi quadre o tonde $\begin{bmatrix} -2 & 4 \\ 3 & 0 \end{bmatrix}$, il determinante è indicato con delle linee dritte, come il valore assoluto: $\begin{vmatrix} -2 & 4 \\ 3 & 0\end{vmatrix}$, oppure $|A|$ e $det(A)$.

Il determinante necessita di una matrice quadrata ed è sempre un numero reale.

##### Determinante matrice 2x2

Per calcolare il determinante moltiplichiamo il valore in alto a sinistra per quello in basso a destra, poi si sottrae col prodotto dei valori in basso a sinistra ed in alto a destra.

$\begin{vmatrix} -2 & 4 \\ 3 & 0\end{vmatrix} = (-2)(0) - (3)(4) = 0 - 12 = -12$.

In generale: $\begin{vmatrix} a & b \\ c & d \end{vmatrix} = ad - bc​$

Graficamente il determinante rappresenta il valore assoluto dell’area di un parallelogramma definito dai punti: $(0,0), (a,b), (c,d), (a+c, b+d)​$.

![](assets/Area_parallellogram_as_determinant.svg)

##### Determinante matrice 3x3

Il determinante di una matrice 3x3 è definito da:

![{\displaystyle {\begin{vmatrix}a&b&c\\d&e&f\\g&h&i\end{vmatrix}}=a{\begin{vmatrix}e&f\\h&i\end{vmatrix}}-b{\begin{vmatrix}d&f\\g&i\end{vmatrix}}+c{\begin{vmatrix}d&e\\g&h\end{vmatrix}}}](assets/1e2b66aee07eda072fc4996c89915b0554220226-1550331812875.svg)

Espansa abbiamo:

![{\displaystyle {\begin{aligned}{\begin{vmatrix}a&b&c\\d&e&f\\g&h&i\end{vmatrix}}&=a(assets/5025a3bca9e476699d45e5b91842a472a4cf903d.svg)-b(di-fg)+c(dh-eg)\\&=aei+bfg+cdh-ceg-bdi-afh.\end{aligned}}}](https://wikimedia.org/api/rest_v1/media/math/render/svg/5025a3bca9e476699d45e5b91842a472a4cf903d)

Una regola mnemonica esiste, chiamata regola di Sarrus:

![](assets/Sarrus_rule.svg)

I valori con la diagonale da sinistra a destra continua vanno moltiplicati, ogni diagonale continua va sommata, i valori con la diagonale da destra a sinistra a tratti vanno moltiplicati, ogni diagonale a tratti va poi sottratta alle altre.

In questo caso il determinante è il valore assoluto del volume del parallelepipedo costruito dai vettori $r_1 = [a_{11}, a_{21}, a_{31}]; r_2 = [a_{12}, a_{22}, a_{32}]; r_3 = [a_{13}, a_{23}, a_{33}]$.

![](assets/Determinant_parallelepiped.svg)

##### Determinante matrice 4x4

Per calcolare il determinante di una matrice $n \times n$ esistono delle formule lunghe e complicate, inizia a bestemmiare; riporto quella che mi è sembrata più semplice da memorizzare, partendo da una $4\times4$:
$$
A = 
\begin{bmatrix}
a & b & c & d \\
e & f & g & h \\
i & j & k & l \\
m & n & o & p
\end{bmatrix}
\\
det(A) = |A| = 
+
\begin{bmatrix}
a & & \\
 & \cdot & \\
 & &
 \begin{vmatrix}
  f & g & h \\
  j & k & l \\
  n & o & p
 \end{vmatrix}
\end{bmatrix}
-
\begin{bmatrix}
b \quad \quad \\
\cdot \quad \quad \\
\begin{vmatrix}
e & & g & h \\
i & & k & l \\
m & & o & p
\end{vmatrix}
\end{bmatrix}
+
\begin{bmatrix}
\quad \quad c \\
\quad \quad \cdot \\
\begin{vmatrix}
e & f &  & h \\
i & j &  & l \\
m & n &  & p
\end{vmatrix}
\end{bmatrix}
-
\begin{bmatrix}
\quad \quad \quad \quad d \\
\quad \quad \quad \quad \cdot \\
\begin{vmatrix}
e & f & g & \\
i & j & k & \\
m & n & o &
\end{vmatrix}
\end{bmatrix}
$$
In formula:
$$
|A| = a \cdot
\begin{vmatrix}
f & g & h \\
j & k & l \\
n & o & p
\end{vmatrix}
- b \cdot
\begin{vmatrix}
e & g & h \\
i & k & l \\
m & o & p
\end{vmatrix}
+ c \cdot
\begin{vmatrix}
e & f & h \\
i & j & l \\
m & n & p
\end{vmatrix}
- d \cdot
\begin{vmatrix}
e & f & g \\
i & j & k \\
m & n & o
\end{vmatrix} = \\
a ( f (k p - l o) - g (j p - l n) + h (j o - k n) ) - 
b ( e (k p - l o) - g (i p - l m) + h (i o - k m) ) + \\
c ( e (j p - l n) - f (i p - l m) + h (i n - j m) ) - 
d ( e (j o - k n) - f (i o - k m) + g (i n - j m) )
$$
La formula poi si espande ricorsivamente per le matrici $n \times n$.

#### Concetto di reciproco con le matrici

In generale possiamo dire che dividere un primo numero per un secondo numero è come moltiplicare il primo numero per il reciproco del secondo; per esempio $k \div 4 = k \frac{1}{4} = k 4^{-1}$.

Il concetto di reciproco viene quindi da questo, il reciproco di un numero $k$ è: $\displaystyle \frac{1}{k} : k \frac{1}{k} = k k^{-1} = 1$.

Espandendoci alle matrici, il reciproco di $A$ è: $AA^{-1} = I$.

#### Inversa di una matrice ed invertibilità

L’inversa di una matrice la si può calcolare tramite il determinante:
$$
A = 
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix}
\\
A^{-1} = \frac{1}{|A|} 
\begin{bmatrix}
d & -b \\
-c & a
\end{bmatrix} =
\frac{1}
{
    \begin{vmatrix}
    a & b \\
    c & d
    \end{vmatrix}
}
\begin{bmatrix}
d & -b \\
-c & a
\end{bmatrix} =
\frac{1}{ad - bc} 
\begin{bmatrix}
d & -b \\
-c & a
\end{bmatrix}
$$
Dalla formula diventa chiaro il problema, se il determinante è $0$ la matrice non è invertibile, in matematichese una matrice $2\times2$ non è invertibile se: $\displaystyle ad - bc = 0 \or ad = bc \or \frac{a}{b} = \frac{c}{d}$.

Un’altra necessità è che la matrice sia quadrata, dato che bisogna calcolarne il determinante.

### Regola o teorema di Cramer

Attraverso l’inversa di una matrice è possibile risolvere i sistemi lineari, infatti:
$$
\begin{cases}
ax + by = f \\
cx + dy = g 
\end{cases}
\iff
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix}
\begin{bmatrix}
x \\
y
\end{bmatrix}
=
\begin{bmatrix}
f \\
g
\end{bmatrix}
\iff
\begin{cases}
M = 
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix} \\
\overrightarrow{a} =
\begin{bmatrix}
x \\
y
\end{bmatrix} \\
\overrightarrow{b} =
\begin{bmatrix}
f \\
g
\end{bmatrix} \\
\end{cases}
\iff \\
M \overrightarrow{a} = \overrightarrow{b} \iff \overrightarrow{a} = M^{-1} \overrightarrow{b}
$$
Quindi, in generale, se annotiamo con $x_i​$ le incognite, per calcolare la $x_i​$esima incognita possiamo usare la formula di Cramer: $\displaystyle x_i = \frac{|A_i|}{|A|}​$, dove $A_i​$ denota la matrice formata sostituendo la $i​$esima colonna di $A​$ col vettore $b​$.

Questo sistema di risoluzione è estremamente inefficiente.

## Algebra lineare, teoria e pratica (Linear algebra theory and practice)

### Tabella convenzioni

|                      | Matrice                                       | Vettore              | Scalare                              |
| -------------------- | --------------------------------------------- | -------------------- | ------------------------------------ |
| **Rappresentazione** | $\begin{bmatrix}1 & 3 \\ 4 & 5 \end{bmatrix}$ | $[4, 2, -2]$         | $6$                                  |
| **Simboli**          | $A, B, C, ...$                                | $a, b, c, ...$       | $\alpha, \beta, \gamma, \delta, ...$ |
| **Elemento**         | $a_{01}, b_{12}, ...$                         | $a_1, b_1, c_2, ...$ | Non necessario                       |

### Vettori

#### Interpretazione algebrica e geometrica dei vettori

A livello algebrico un vettore è una lista ordinata di numeri, la dimensione di un vettore è data dal numero di numeri, da quanti numeri ha.

Possiamo anche espandere il concetto usando la parola elementi, implicando l’uso possibile anche con funzioni.

L’ordine in cui compaiono gli elementi è importante, se si tratta solo di vettori questi possono essere rappresentati sia in colonna, quindi li chiamiamo vettore colonna, che in riga, chiamati anche vettori riga.

Esempi ($len()$ denota la lunghezza, quindi anche le dimensioni, del vettore):
$$
\overrightarrow{a} = 
\begin{bmatrix}
3 \\
4
\end{bmatrix}
= [3, 4] \\
len(a) = 2 \\
\overrightarrow{b} = 
\begin{bmatrix}
1 \\
0 \\
\pi \\
4i \\
-2
\end{bmatrix}
=
[1, 0, \pi, 4i, -2] \\
len(b) = 5
$$
Mettendo le due interpretazioni a confronto:

| Valore algebrico | Interpretazione geometrica                 |
| ---------------- | ------------------------------------------ |
| $ [1, 2] $       | ![1550845369762](assets/1550845369762.png) |

Geometricamente i due componenti di un vettore sono la lunghezza e la direzione dal punto d’origine al punto finale, di norma il punto d’origine è l’origine dello spazio e quindi il vettore si dice in “posizione standard”.

Un vettore 3D:

| Valore algebrico                          | Interpretazione geometrica         |
| ----------------------------------------- | ---------------------------------- |
| $[1, 2, 3] \quad \quad \quad \quad \quad$ | ![Vettore3d](assets/Vettore3d.jpg) |

Come già accennato, un vettore può contenere delle funzioni, questo significa che è possibile costruire delle curve parametriche (argomento di Calcolo 2, utile in fisica) grazie ai vettori:

| Valore algebrico                                  | Interpretazione geometrica                  |
| ------------------------------------------------- | ------------------------------------------- |
| $\overrightarrow{r(t)} = [2 cos(t), 4 sin(t), t]$ | ![img](assets/Vector-valued_function-2.png) |

#### Somma e sottrazione tra vettori

La somma o sottrazione tra vettori è possibile solo con vettori di dimensione uguale, il procedimento di somma è elementare, da un aspetto algebrico: $[1, 0, 4, 3] + [2, -3, -2, 1] = [3, -3, 2, 4]$. Basta fare semplicemente una somma elemento per elemento.

| Valore algebrico            | Interpretazione geometrica      |
| --------------------------- | ------------------------------- |
| $[4, 2] + [-2, 6] = [2, 8]$ | ![](assets/2_somma_vettori.png) |

Geometricamente la somma tra vettori costruisce un triangolo, possiamo scegliere quale dei due vettori traslare; traslando entrambi otteniamo un parallelogramma da cui viene la “regola del parallelogramma”.

#### Prodotto scalare-vettore

Il prodotto tra uno scalare ed un vettore è semplice, si prende ogni elemento del vettore e lo si moltiplica per il valore dello scalare.

| Valore algebrico | Interpretazione gometrica         |
| ---------------- | --------------------------------- |
| $2 [4, 2]$       | ![](assets/3_scalare_vettore.png) |

##### Proprietà

Il prodotto scalare per vettore è commutativo, associativo e lineare (praticamente è la distributiva).

* $\alpha a = a \alpha$
* $(\alpha \beta) a = \alpha(\beta a)​$
* $\alpha (a + b) = \alpha a + \alpha b$

#### Prodotto scalare tra vettori

Il prodotto scalare ha diverse notazioni, a volte è chiamato dot product dal fatto che ha come simbolo il $\cdot$, riporto le varie notazioni con la formula: $\displaystyle a \cdot b = \left< a, b \right> = a^Tb = \sum_{i=1}^n a_i b_i$.

Se i vettori sono complessi, $\alpha$ sarà un numero complesso, $a^T$ indica la trasposta, può anche essere indicata con $a^*$, è un argomento affrontato nelle matrici.

Nei vettori è condizione necessaria che gli elementi/dimensione/lunghezza dei vettori sia la stessa tra i due vettori.

Un esempio:
$$
a = [1, 0, 2, 5, -2] \quad b = [2, 8, -6, 1, 0] \\
a \cdot b = 1 \times 2 + 0 \times 8 + 2 \times (-6) + 5 \times 1 + (-2) \times 0 =
2 - 12 + 5 = -5
$$

##### Proprietà

Il prodotto scalare è commutativo, omogeneo (rispetto al prodotto scalare-vettore), distributivo rispetto la somma.

* $a \cdot b = b \cdot a$
* $(\alpha a) \cdot b = \alpha (a \cdot b)$
* $a\cdot (b + c) = a \cdot b + b \cdot c$
* $a \cdot b = 0 \iff a \perp b$ (dimostrazione nel capitolo dopo)

#### Lunghezza del vettore

##### Definizione

La lunghezza del vettore, o magnitudo, o norma è definita semplicemente da: $|a| = ||a|| = \sqrt{a \cdot a}​$, questa formula viene dal teorema di Pitagora, infatti andiamo a calcolare l’ipotenusa di un triangolo costruito con i punti che compongono il vettore.

Quindi semplicemente andiamo a calcolare la somma dei quadrati degli elementi che compongono il vettore.

##### Interpretazione geometrica

A livello geometrico la norma è $cos(\theta_{ab}) ||a|| ||b||$.

Dall’equazione precedente possiamo risolverla in merito all’angolo $\theta$ tra i due vettori:
$$
\alpha = ||a||||b||cos(\theta_{ab}) \iff 
\frac{\alpha}{||a||||b||} = cos(\theta_{ab}) 
\iff \theta_{ab} = arcos\left(\frac{\alpha}{||a||||b||}\right)
$$

###### Proprietà derivate dall’equazione

| $\displaystyle \theta < 90^o \iff \theta < \frac{1}{2}\pi$ | $\displaystyle \theta > 90^o \iff \theta > \frac{1}{2}\pi$ | $\displaystyle \theta = 90^o = \frac{1}{2}\pi$ | $\theta = 0^o = 0 \pi$                           |
| ---------------------------------------------------------- | ---------------------------------------------------------- | ---------------------------------------------- | ------------------------------------------------ |
| $cos(\theta) > 0\\ \implies \alpha > 0$                    | $cos(\theta) < 0\\ \implies a < 0$                         | $cos(\theta) = 0\\ \implies \alpha = 0$        | $cos(\theta)=1 \\ \implies \alpha = ||a|| ||b||$ |

Se l’angolo $\alpha = 0$ significa che i due vettori sono ortogonali.

Se l’angolo $\alpha = ||a|| ||b||$, quindi $cos(\theta) = 1$ allora i vettori si dicono co-lineari, quindi sono sulla stessa retta.

##### Dimostrazione dell’uguaglianza tra le definizioni algebrica e geometrica della norma di un vettore

$$
\alpha = a \cdot b = \sum_{i = 1}^n a_i b_i = cos(\theta_{ab}) ||a|| ||b|| \\
||a-b||^2 = ||a||^2 + ||b||^2 - 2||a|| ||b|| cos(\theta_{ab}) \\
||a-b||^2 = (a-b)\cdot(a-b) \iff 
||a-b||^2 = a \cdot a + b\cdot b - a\cdot b - a \cdot b \iff \\
\iff ||a-b||^2 = a \cdot a + b\cdot b - 2 a\cdot b \\
a \cdot a + b \cdot b - 2 a \cdot b = ||a||^2 + ||b||^2 - 2 ||a|| ||b|| cos(\theta_{ab}) \iff \\
\iff
||a||^2 + ||b||^2 - 2 a \cdot b = ||a||^2 + ||b||^2 - 2 ||a|| ||b|| cos(\theta_{ab}) \iff \\
\iff cos(\theta_{ab}) ||a|| ||b|| = a \cdot b
$$

Ma quindi due vettori sono ortogonali anche quando il loro prodotto scalare è uguale a $0$.

Se i due vettori hanno il prodotto scalare minore di $0$ vuol dire che l’angolo è ottuso, se i due vettori hanno il prodotto scalare maggiore di $0$ l’angolo è acuto.

#### Prodotto cartesiano/outer product

In caso possa servire, l’outer product si tratta di un prodotto tra vettori dove la trasposta è del secondo vettore quindi: $A = a b^T$.

Dall’outer product abbiamo la formazione di una matrice quadrata con le dimensioni di uno dei due vettori (indipendente dato che sono entrambi di dimensioni uguali).

Per effettuarlo si moltiplica ogni colonna di $b$ (che da vettore colonna diventa riga, supposto che siano entrambi vettori colonna) con ogni riga di $a$.
$$
\begin{bmatrix}
a \\
b \\
c \\
d
\end{bmatrix}
[e, f, g, h]
=
\begin{bmatrix}
e a & f a & g a & h a \\
e b & f b & g b & h b \\
e c & f c & g c & h c \\
e d & f d & g d & h d \\
\end{bmatrix}
$$


#### Vettori e numeri complessi

In generale algebra lineare tratta solo di spazi in $\R^n ​$, ma capita di avere a che fare con dei numeri complessi.

Un numero complesso può essere rappresentato graficamente, per esempio il punto del piano immaginario: $[2, 3i]$, questo implica anche che i numeri immaginari sono vettori.

Esempio col prodotto tra numeri complessi:
$$
z = a + bi \quad w = c + di \\
z, w \in \C \\
z \times w = (a + bi) (c + di) = ac + adi + cbi - bd
$$

##### Vettori complessi

È possibile costruire dei vettori di numeri complessi, per esempio:
$$
\begin{bmatrix}
1 + 3i \\
0 -2i \\
4 + 0i \\
-5 + 0i
\end{bmatrix}
\in \C^4
$$
Tutte le operazioni viste per i vettori nei numeri reali rimangono, per esempio il prodotto scalare:
$$
\begin{align}
\begin{bmatrix}
1 + 3i \\
0 -2i \\
4 + 0i \\
-5 + 0i
\end{bmatrix}
\cdot
\begin{bmatrix}
6 + 2i \\
8 + 0i \\
0 + 3i \\
-5 + 0i
\end{bmatrix}
=
(1 + 3i)(6 + 2i) + (-2i)(8) + (4)(3i) + (-5)(-5) = \\
(6 + 2i + 18i + 6i^2) + (-16i) + (12i) + (25) = 
20i - 16i + 12i + 25 = 16i + 25 = 25 + 16i
\end{align}
$$

##### Coniugato di un numero complesso

| Numero Complesso | Coniugato del numero |
| ---------------- | -------------------- |
| $a + bi$         | $a - bi$             |
| $a - bi $        | $a + bi $            |

Dalla tabella è chiaro che basta cambiare il segno della parte immaginaria del numero, graficamente si tratta di una simmetria rispetto la retta dei reali

##### Trasposta di Hermite o la trasposta di un coniugato

$$
\begin{bmatrix}
1 + 3i \\
0 -2i \\
4 + 0i \\
-5 + 0i
\end{bmatrix}^H
=
\begin{bmatrix}
1 + 3i \\
0 -2i \\
4 + 0i \\
-5 + 0i
\end{bmatrix}^*
=
\begin{bmatrix}
1 + 3i \\
0 -2i \\
4 + 0i \\
-5 + 0i
\end{bmatrix}^T
=
[ 1-3i, +2i, 4, -5]
$$

Questa trasposta, diversa dalla normale, è necessaria per calcolare la norma di un numero complesso, infatti, prendiamo per esempio il numero complesso $(3,4i)​$, sappiamo che il risultato dovrebbe essere $5​$ perché l’ipotenusa del triangolo $\left< (0,0), (3,0), (0,4) \right>​$ è $5​$:
$$
||z||^2 \ne z^Tz = [3 + 4i][3 + 4i] = 9 + 12i + 12i + 16i^2 = 9 - 16 + 24i = -7 + 24i \\
||z||^2 = z^Hz = [3 - 4i][3 + 4i] = 9 + 12i - 12i - 16i^2 = 9 + 16 = 25 \\
||z|| = \sqrt{25} = 5
$$

#### Vettore unitario

Il vettore unitario è quel vettore che ha come lunghezza $1$, quindi $||v|| = 1$.

Per trovare il vettore unitario in qualunque direzione, dato un vettore, la formula è semplice:
$$
\exists \alpha \in \R : ||\alpha v || = 1 \implies \alpha = \frac{1}{||v||}
$$
Ovviamente è impossibile trovare il vettore unitario del vettore zero in quanto non possiede alcuna direzione essendo un punto (geometricamente) ed è uguale a 0 (rendendo impossibile la divisione algebricamente).

La normalizzazione di un vettore è la trasformazione di questo vettore nel proprio vettore unitario affinché quindi sia soddisfatta la condizione del vettore unitario.

### Dimensioni e spazio vettoriale

#### Breve definizione

La dimensione di un vettore è definita dal numero di elementi che ha, per esempio $v = [v_1, v_2]​$ ha due dimensioni.

Un campo è un insieme in cui somma, sottrazione, moltiplicazione e divisione sono operazioni valide, per esempio $\N​$ non è un campo perché le operazioni di sottrazione e divisione possono andare oltre l’insieme stesso, quindi in $\Z​$ o $\Q​$.

Le dimensioni vengono rappresentate in algebra lineare con un apice rispetto l’insieme su cui lavoriamo, quindi, per esempio, $\R^n$ ha $n$ dimensioni.

Lo spazio vettoriale è quindi chiuso rispetto la moltiplicazione e la somma (o in altre parole combinazioni lineari) e deve contenere l’origine.

#### Sottospazi

Dato un vettore,  il sottospazio è l’insieme di tutti i vettori che possono essere creati prendendo tutte le combinazioni lineari possibili.
$$
v \in \R^n \quad
\alpha \in \R \\
\alpha v
$$
Per esempio: $v = [2,3] \quad u=2[2,3] = [4,6] \quad w=-3 [4,6] = [-12, -18]$, fanno tutti parte dello stesso sottospazio di base $[2,3]$.

| È sotto-spazio di $v = [2,3] $? | Vettore |
| ------------------------------- | ------- |
| No                              | $[2,4]$ |
| No                              | $[0,3]$ |
| No                              | $[0,4]$ |

Gli ultimi due vettori ($[0,3]$ e $[0,4] $) fanno però parte dello stesso sottospazio.

È possibile anche costruire sottospazi da più vettori:
$$
\alpha v + \beta w \\
v = [2,3] \quad
w = [0,4] \quad 
6v - 4w = [12,2]
$$
Uno sottospazio vettoriale deve:

* Avere come operazioni interne la somma ed il prodotto scalare
* Contenere il vettore zero $0$

Definizione formale
$$
\forall v, w \in V, \quad \forall \alpha, \beta \in \R \\
\alpha v + \beta w \in V
$$
Dove $V$ è il sottospazio.

Geometricamente un vettore descrive un sottospazio di una dimensione, cioè una retta; due vettori (linearmente indipendenti, quindi il secondo vettore non è sulla retta del primo vettore) descrivono un sottospazio di due dimensioni, e così via.

Ogni spazio $\R^n$ ha come sottospazi tutte le combinazioni possibili da $0$ fino a $\R^n$ incluso. Visualizzando se abbiamo 3 dimensioni abbiamo il sottospazio definito dall’origine che ha $0$ dimensioni, tutte le rette passanti per l’origine, tutti i piani passanti per l’origine, e lo spazio tridimensionale stesso.

#### Sottoinsiemi

Un sottoinsieme è un insieme di punti che:

* Può o non può contenere l’origine
* Può non essere chiuso
* Può essere limitato

Esempio: $A = \{ x, y \in \R : x > 0 \and y > 0 \}$ (descrive un piano)

Col fatto che $A$ non include l’origine non può essere uno sottospazio di $\R^2$.

Altro esempio: $C = \{x,y \in \R: x^2 + y^2 = 1 \}$ (descrive una circonferenza unitaria)

Questo non è un sottospazio perché non include l’origine.

Altro esempio: $B = \{ x,y \in \R : y = 4x \}$ (descrive la retta $y = 4x$)

Questo è un sottospazio di dimensione 1 perché contiene l’origine ed è possibile effettuare somme e prodotti scalari.

Per comprendere se un sottoinsieme è un sottospazio abbiamo due passi:

1. Scoprire se l’origine è inclusa
2. Provare a descrivere dei vettori a partire dalle condizioni dell’inseieme nella forma $\alpha v + \beta w$

#### Span

Definizione formale
$$
span(\{v_1, ..., v_n\}) = \{ \alpha_1 v_1 + ... + \alpha_n v_n; \alpha \in \R \}
$$
Lo span di una certa quantità di vettori è l’insieme di tutti i punti che possono essere definiti come combinazioni lineari di questi vettori.

Esempio:
$$
v = [1, 2, 0] \quad w = [3, 2, 1] \quad S = \{[1,1,0],[1,7,0]\} \\
v \in span(S)? \quad w \in span(S)? \\
[1,2,0] = \alpha [1,1,0] + \beta[1,7,0] \quad \alpha = 5/6, \beta = 1/6 \\
v \in span(S) \\
[3,2,1] = \alpha[1,1,0] + \beta[1,7,0] \\
w \not \in span(S)
$$

#### Indipendenza lineare

Un insieme è dipendente linearmente se almeno uno dei suoi componenti è risultato di una combinazione tra gli altri componenti.

Esempi di dipendenza lineare:
$$
\begin{align}
& \{w_1, w_2\} = \left\{ 
	\begin{bmatrix}
	1 \\
	2 \\
	3
	\end{bmatrix},
	\begin{bmatrix}
	2 \\
	4 \\
	5
	\end{bmatrix}
\right\} \quad 
w_2 = 2 w_1 \\
& \{v_1, v_2, v_3\} =
\left\{ 
	\begin{bmatrix}
	0 \\
	2 \\
	5
	\end{bmatrix},
	\begin{bmatrix}
	-27 \\
	5 \\
	-37
	\end{bmatrix},
	\begin{bmatrix}
	3 \\
	1 \\
	8
	\end{bmatrix}
\right\} \quad
v_2 = 7v_1 - 9v_3
\end{align}
$$
Esempi di indipendenza lineare:
$$
\begin{align}
& \{w_1, w_2\} = \left\{ 
	\begin{bmatrix}
	1 \\
	2 \\
	3
	\end{bmatrix},
	\begin{bmatrix}
	2 \\
	4 \\
	7
	\end{bmatrix}
\right\} \\
& \{v_1, v_2, v_3\} =
\left\{ 
	\begin{bmatrix}
	0 \\
	2 \\
	5
	\end{bmatrix},
	\begin{bmatrix}
	-27 \\
	0 \\
	-37
	\end{bmatrix},
	\begin{bmatrix}
	3 \\
	1 \\
	9
	\end{bmatrix}
\right\} 
\end{align}
$$
Definizione formale:
$$
\alpha_1 v_1 + \alpha_2 v_2 + ... + \alpha_n v_n = 0, \alpha \in \R
$$
Per mostrare meglio la dipendenza lineare:
$$
\alpha_1 v_1 = \alpha_2 v_2 + ... + \alpha_n v_n \\
v_1 = \frac{\alpha_2}{\alpha_1} v_2 + ... + \frac{\alpha_n}{\alpha_1}v_n \implies \alpha \in \R, \alpha_1 \ne 0
$$
Ovviamente possiamo prende un qualunque vettore dell’insieme.

Esempi grafici:

| Insieme dipendente                                           | Insieme indipendente                     | Insieme dipendente                       |
| ------------------------------------------------------------ | ---------------------------------------- | ---------------------------------------- |
| ![](assets/7_indipendenza_lineare_1.png) | ![](assets/7_indipendenza_lineare_2.png) | ![](assets/7_indipendenza_lineare_3.png) |

I vettori indipendenti formano una nuova dimensione, nel primo caso il vettore rosso continua ad essere nella dimensionalità $\R^1$ descritta dal vettore verde, nel secondo caso i due vettori sono indipendenti e formano quindi una dimensionalità $\R^2$, nell’ultimo caso il vettore blu è già incluso nel piano descritto dai vettori rosso e verde, questo è anche un caso particolare che ci porta ad una regola:

* Qualunque insieme di $M > N$ vettori in $\R^N$ è dipendente
* Qualunque insieme di $M \le N$ vettori in $\R^N$ può essere indipendente

Come determinare se un insieme è indipendente:

1. Conta i vettori comparando la loro dimensionalità $\R^N$, in altre parole il numero di elementi con la loro dimensione
2. Controlla gli $0$ negli elementi, il vettore zero porta sempre alla dipendenza lineare per via della sua proprietà
3. Fai delle prove
4. Usa il metodo del rango delle matrici, i vettori diventano colonne di una matrice

#### Base di un sottospazio

La base canonica, base standard o naturale, nello spazio euclideo, indicata con $e^n$, è formata dagli assi cartesiani, quindi ha forma:
$$
e_x, e_y = \left\{
\begin{bmatrix}
1 \\
0 \\
\end{bmatrix},
\begin{bmatrix}
0 \\
1 \\
\end{bmatrix}
\right\}
$$
In particolare la base canonica è l’insieme dei vettori unitari che puntano nelle direzioni degli assi cartesiani, e sono basi per tutti gli insiemi $ \R^n $.

Una base qualunque è quindi formata dallo span di vettori linearmente indipendenti.

Per esempio:
$$
\text{Insieme } \R^2 \\
e = \left\{
	\begin{bmatrix}
	1 \\ 0
	\end{bmatrix}
	\begin{bmatrix}
	0 \\ 1
	\end{bmatrix}
\right\} \quad 
T = \left\{
	\begin{bmatrix}
	1 \\ 1
	\end{bmatrix}
	\begin{bmatrix}
	0 \\ 2
	\end{bmatrix}
\right\}
$$
Ogni punto nel piano $ \R^2 $ è esprimibile come combinazione lineare di qualunque base in $\R^2$.

Per esempio:
$$
\begin{align}
& P_{[e]} = [2,1] \\
& P_{[T]} = [2, -0.5] \\
& Q_{[e]} = [3,3] \\
& Q_{[T]} = [3,0]
\end{align}
$$
Esempio identificare basi in $\R^2$ ed $\R^3$:
$$
\begin{align}
& M_1 = \left\{ 
	\begin{bmatrix}
	-3 \\ 0 \\ 0
	\end{bmatrix},
	
	\begin{bmatrix}
	-1 \\ 0 \\ 0
	\end{bmatrix},

	\begin{bmatrix}
	13 \\ 0 \\ 0
	\end{bmatrix}
\right\} \\
& M_2 = \left\{ 
	\begin{bmatrix}
	-4 \\ 1 \\ 0
	\end{bmatrix},
	
	\begin{bmatrix}
	\pi \\ 3 \\ 0
	\end{bmatrix},

	\begin{bmatrix}
	3 \\ 2\pi \\ 3
	\end{bmatrix}
\right\} \\
& M_3 = \left\{ 
	\begin{bmatrix}
	-3 \\ 0 \\ 0
	\end{bmatrix},
	
	\begin{bmatrix}
	-1 \\ 0 \\ 0
	\end{bmatrix},

	\begin{bmatrix}
	13 \\ 0 \\ 1
	\end{bmatrix}
\right\}
\end{align}
$$


L’insieme $M_2$ è una base per $\R^3$, perché contiene 3 vettori linearmente indipendenti.

L’insieme $M_1$ e l’insieme $M_3$ non sono linearmente indipendenti.

Una base non può assolutamente essere linearmente dipendente, perché anche se il sottospazio ha dimensione corretta ogni punto in quella base avrebbe infinite rappresentazioni.

Ricapitolando: la base di uno spazio vettoriale è un insieme di vettori linearmente indipendenti che generano lo spazio e quindi ogni elemento dello spazio vettoriale può essere scritto in modo unico come combinazione lineare dei vettori appartenenti alla base.

### Matrici

#### Introduzione alle matrici

Nelle matrici, come già visto, ci riferiamo agli elementi prendendo prima la riga e poi la colonna, è possibile anche creare sotto-matrici che formano matrici più grandi.

La diagonale principale è quella che parte da in alto a sinistra fino in basso a destra, cioè data una matrice $A_{m,n}$ gli elementi della diagonale principale sono tutti gli elementi $a_{ii}$ per $i$ $1 \to m$ (o $n$ se ha meno colonne che righe); come già detto ha senso di solito parlare di diagonale solo nelle matrici quadrate, ma può capitare che ci si riferisca alla diagonale anche in matrici non quadrate.

La grandezza della matrice è determinata dal numero di righe e colonne, quindi una matrice $m \times n$ ha $m$ righe e $n$ colonne.

La dimensione di una matrice la possiamo rappresentare con $\R^{m \times n}$, nota che una matrice $m \times n$ è diversa da una matrice $n \times m$.

Esistono anche matrici chiamate tensori, che sono $m \times n \times k$.

L’algebra lineare si occupa di risolvere, in fin dei conti, due equazioni:
$$
AX = B \quad AX = 0
$$
Dove $X$ è un vettore colonna di incognite, mentre $A, B$ sono noti.

#### Tipi di matrici

Una matrice può essere:

* Quadrata, se il numero di righe corrisponde al numero di colonne
* Rettangolare, se il numero di righe non corrisponde al numero di colonne
* Simmetrica, se la matrice è quadrata e gli elementi sono simmetrici rispetto la diagonale principale, ovvero $a_{ij} = a_{ji}$
* Anti-simmetrica, se la matrice è quadrata e gli elementi sono simmetrici e di segno opposto rispetto la diagonale principale, ovvero $a_{ij} = - a_{ji}$
* Identità, se la matrice è quadrata e gli elementi sulla diagonale principale sono tutti uguali a $1$ mentre gli altri sono uguali a $0$, si indica con $I_{m}$ se la dimensione è specificata o semplicemente $I​$.
* Zero, se ogni elemento della matrice è uguale a $0$, si indica con $0_{m,n}$ oppure con $O_{m,n}$
* Diagonale, se tutti gli elementi, tranne quelli della diagonale principale, sono uguali a $0​$, gli elementi sulla matrice diagonale possono essere qualunque numero scalare, la matrice può essere anche rettangolare, questo significa che esistono righe o colonne di $0​$
* Triangolare, se la matrice è quadrata e tutti gli elementi sopra o sotto della diagonale principale sono $0​$, gli altri possono essere qualunque numero scalare

#### Somma di matrici e prodotto scalare-matrice

Per sommare (o sottrarre) due matrici, queste devono avere le stesse dimensioni $m \times n$.

Il prodotto scalare-matrice non ha condizioni necessarie particolari.

La somma tra matrici è tra matrici è commutativa ed associativa, la sottrazione non è né commutativa né associativa.

Il prodotto scalare-matrice è commutativo e lineare.

Breve riepilogo delle proprietà:

* $A + B = B + A$
* $(A + B) + C = A + (B + C)$
* $A - B \ne B - A$
* $(A - B) - C \ne A - (B - C)$
* $\alpha A = A \alpha$
* $(\alpha \beta) A = \alpha (\beta A)$
* $\alpha (A + B) = \alpha A + \alpha B$


#### Trasposta di una matrice

La trasposta di un vettore converte il vettore da vettore riga a vettore colonna e viceversa, questo vuol dire anche che la trasposta della trasposta di un vettore è il vettore stesso.

La trasposta di una matrice ha lo stesso concetto, le colonne diventano righe, le righe diventano le colonne.

Per esempio:
$$
A = 
\begin{bmatrix}
1 & 5 \\
0 & 6 \\
2 & 8 \\
5i & 3 \\
-2 & 0
\end{bmatrix} \quad
A^T = A^* = 
\begin{bmatrix}
1 & 0 & 2 & 5i & -2 \\
5 & 6 & 8 & 3 & 0
\end{bmatrix} \quad A^{TT} = A = 
\begin{bmatrix}
1 & 5 \\
0 & 6 \\
2 & 8 \\
5i & 3 \\
-2 & 0
\end{bmatrix}
$$
Una matrice simmetrica è tale che: $A = A^T$.

Un matrice anti-simmetrica è tale che: $A = -A^T$.

Definizione formale:
$$
(A^T)_{ij} = A_{ji} \quad \forall A \in K^{m \times n} \quad 1 \le i \le m, \quad 1 \le j \le n
$$
Proprietà:

* La trasposta della trasposta è la matrice stessa: ${\displaystyle \left(A^{T}\right)^{T}=A}$
* La trasposta della somma di due matrici è uguale alla somma delle due matrici trasposte:
	${\displaystyle (A+B)^{T}=A^{T}+B^{T}}​$
* L'ordine delle matrici si inverte per la moltiplicazione:
	${\displaystyle \left(AB\right)^{T}=B^{T}A^{T}}​$
* Se ${\displaystyle c}$ è uno scalare, la trasposta di uno scalare è lo scalare invariato:
	${\displaystyle (cA)^{T}=cA^{T}}$
* Nel caso di matrici quadrate, il determinante della trasposta è uguale al determinante della matrice iniziale: ${\displaystyle \det(A^{T})=\det(A)}$
* Il prodotto scalare tra due vettori colonna ${\displaystyle \mathbf {a} }$ e ${\displaystyle \mathbf {b} }$ può essere calcolato come: ${\displaystyle \mathbf {a} \cdot \mathbf {b} =\mathbf {a} ^{T}\mathbf {b}}$ 
* Se ${\displaystyle A}$ ha solamente elementi reali, allora ${\displaystyle A^{T}A}$ è una matrice simmetrica semidefinita positiva.
* La trasposta di una matrice invertibile è ancora invertibile e la sua inversa è la trasposta dell'inversa della matrice iniziale: ${\displaystyle (A^{T})^{-1}=(A^{-1})^{T}}$
* Se ${\displaystyle A^{T}=A^{-1}}$allora ${\displaystyle A}$ è una matrice ortogonale
* Se ${\displaystyle A}$ è una matrice quadrata, allora i suoi autovalori sono uguali agli autovalori della sua trasposta.

#### Matrici complesse

Una matrice complessa è una matrice in $\C^{m \times n}$, ad esempio:
$$
A = 
\begin{bmatrix}
1 & -1+5i & 0 \\
-1 & -2 & -4 \\
6i & -4 & 5-2i
\end{bmatrix}
\quad
A^H = A^* = 
\begin{bmatrix}
1 & -1 & -6i \\
-1-5i & -2 & -4 \\
0 & -4 & 5+2i
\end{bmatrix}
$$

La matrice trasposta coniugata è anche chiamata matrice aggiunta.

#### Diagonale e traccia di matrice

La diagonale principale di una matrice quadrata è la diagonale che va dall'angolo in alto a sinistra a quello in basso a destra, è possibile comunque considerare anche la diagonale di matrici non quadrate.
$$
diag(A) = (A)_{ii} \quad 1 \le i \le n
$$
La traccia di una matrice quadrata è la somma di tutti gli elementi della diagonale principale, non si può fare la traccia di una matrice rettangolare.
$$
tr(A) = \sum_{i = 1}^n a_{ii}
$$

#### Moltiplicazione matrice-matrice

Affinché si possa fare il prodotto tra due matrici il numero delle righe della seconda matrice dev’essere uguale al numero delle colonne della prima matrice, in generale: $ A_{m \times n} B_{n \times k} = C_{m \times k} ​$.

Esempi di validità e non validità

| Operazione               | $A B$              | $BA$               | $C^TA$             |
| ------------------------ | ------------------ | ------------------ | ------------------ |
| **Dimensioni**           | $A_{5,2}; B_{2,7}$ | $A_{5,2}; B_{2,7}$ | $A_{5,2}; C_{5,7}$ |
| **Dimensioni risultato** | $R_{5,7}$          | Impossibile        | $R_{7,2}$          |

Perché è possibile scrivere il prodotto scalare con la trasposta: 
$$
v,w \in \R^{n} \implies v_{n,1}, w_{n,1} \implies v^T_{1, n} \\
v^T w = u_{1, 1}
$$

$$
A \in \R^{m \times n}, \quad B \in \R^{n \times k} \\
AB = C \\
c_{ij} = \sum_{p = 1}^n a_{ip} b_{pj}
$$

Possiamo anche visualizzare dei vari metodi di moltiplicazione tra matrici:

![1550492248289](assets/1550492248289.png)



Proprietà:

* La moltiplicazione fra matrici è generalmente non commutativa, ovvero $AB \ne BA$
* La moltiplicazione fra matrici è distributiva rispetto alla somma:
	${\displaystyle A(B+C)=AB+AC}$
	${\displaystyle (A+B)C=AC+BC}$
* Per ogni scalare $k$ vale: ${\displaystyle k(AB)=(kA)B=A(kB)}$
* La moltiplicazione fra matrici è associativa: ${\displaystyle A(BC)=(AB)C}$
* L'elemento neutro per l'operazione di moltiplicazione fra matrici è la matrice identica: $AI = IA = A$
* La matrice nulla $0$ con $n$ righe annulla qualsiasi altra matrice: $0A = A0 = 0$
* Una matrice quadrata $A$ è invertibile se esiste un’altra matrice, indicata con $A^{-1}$, tale che: $AA^{-1} = A^{-1}A = I$, in generale una matrice è invertibile se il determinante è non nullo
* Già visto nelle proprietà della trasposta: $(AB)^T = B^T A^T$
* $(AB)^{-1} = B^{-1}A^{-1}$

#### Moltiplicazione matrice-vettore

Quando si fa il prodotto tra una matrice ed un vettore, il risultato è sempre un vettore, che sia colonna o riga.

Per esempio: 
$$
A_{m \times n} w_{n \times 1} = v_{m \times 1} \\
w_{n \times 1}^T A_{m \times n} = v_{1 \times n}
$$
Nel caso particolare delle matrici simmetriche, indichiamo con $S$ una matrice simmetrica:
$$
Sw = v \iff (Sw)^T = v^T \iff w^TS^T = v^T \iff w^TS = v^T
$$
Questo significa che non ci importa l’ordine del prodotto quando abbiamo una matrice simmetrica, infatti:
$$
\begin{bmatrix}
a & b \\
b & c
\end{bmatrix}
\begin{bmatrix}
2 \\ 3
\end{bmatrix}
=
\begin{bmatrix}
2a + 3b \\
2b + 3c
\end{bmatrix}
=
\left(
\begin{bmatrix}
2 & 3
\end{bmatrix}
\begin{bmatrix}
a & b \\
b & c
\end{bmatrix}
\right)^T
=
\begin{bmatrix}
2a + 3b & 2b + 3c
\end{bmatrix}^T
$$
Esercizio, trova il valore giusto:
$$
\begin{bmatrix}
2 & 4 \\
2 & 1
\end{bmatrix}
\begin{bmatrix}
x \\ 4 
\end{bmatrix}
=
\begin{bmatrix}
22 \\ 10
\end{bmatrix}
\iff
\begin{bmatrix}
2(x) + 4(4) \\
2(x) + 1(4)
\end{bmatrix}
=
\begin{bmatrix}
22 \\ 10
\end{bmatrix}
$$

$$
2x + 16 = 22 \iff 2x = 22 - 16 \iff x = 3 \\
2x + 4 = 10 \iff 2x = 10 - 4 \iff x = 3
$$

Esempio di trasformazione:

| Aspetto algebrico        | Risultato geometrico                                         |
| ------------------------ | ------------------------------------------------------------ |
| ![asgd](assets/asgd.svg) | ![1_applicazione_lineare](assets/1_applicazione_lineare-1550497725323.png) |

I vettori verde e blu sono i vettori risultato dopo le trasformazioni di $v​$, prima con $Av​$ e poi con $Bv​$.

La matrice di rotazione è una matrice particolare così costituita:
$$
\begin{bmatrix}
cos(\theta) & -sin(\theta) \\
sin(\theta) & cos(\theta)
\end{bmatrix}
$$
Dove $\theta​$ è un angolo in radianti.

Affinché si compia una rotazione la lunghezza del vettore non deve cambiare.

La dimostrazione è semplice perché $cos^2(\theta) + sin^2(\theta) = 1$ (il segno meno della prima riga si cancella grazie al quadrato), quindi la trasformazione ha sempre lunghezza $1$ che non cambia quindi la lunghezza del vettore.

La matrice di rotazione è una matrice ortogonale.

Un caso particolare di trasformazione:
$$
\begin{bmatrix}
2 & 1 \\
2 & 3
\end{bmatrix}
\begin{bmatrix}
1 \\
2
\end{bmatrix}
=
\begin{bmatrix}
4 \\
8
\end{bmatrix}
= 4
\begin{bmatrix}
1 \\
2
\end{bmatrix}
$$
Questo avviene solo per questa particolare combinazione ed ordine di vettore e matrice, il vettore $[1,2]$ è un Eigen-vector o autovettore, mentre lo scalare $4$ è un Eigen-value o autovalore, la situazione particolare è generalizzabile con: $A v = \alpha v ​$ che è la equazione fondamentale dell’autovalore, vedi la sezione dedicata.

#### Matrice identità o identica e zero

La matrice identità è una matrice particolare, la matrice identità è una matrice quadrata, indicata con $I_n $.
$$
I_n = 
\begin{bmatrix}
1 & 0 & \dots & 0 \\
0 & 1 & \dots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \dots & 1
\end{bmatrix}
$$
Ha delle proprietà:

* Fondamentale: $AI = IA = A$.
* È sempre invertibile e l’inversa della matrice identità è la matrice identità stessa $I^{-1} = I$.
* Le colonne della matrice identità in $\R^n$ sono i vettori $e_i$ della base canonica.
* La matrice identità è diagonale ed ha autovalore $1$.

La matrice zero o nulla $0$ è una matrice i cui valori sono tutti pari a zero, possiamo specificare anche il numero di righe e colonne.
$$
0_{m \times n} = 
\begin{bmatrix}
0 & ... & 0 \\
\vdots & \ddots & \vdots \\
0 & \dots & 0
\end{bmatrix}
$$
Ha la seguente proprietà:

* Elemento neutro della somma tra matrici: $A + 0 = 0 + A = A$ 

#### Matrice simmetrica

La matrice simmetrica è una matrice quadrata che è la trasposta di sé stessa, in termini matematici: $A^T = A$.

Un altro modo per indicarla è usare i singoli elementi, cioè: $a_{ij} = a_{ji} \quad \forall i, j$.

Per rendere simmetrica una matrice possiamo sommare la matrice con la sua trasposta, infatti $A + A^T​$ è simmetrica, per esempio:
$$
\begin{bmatrix}
a & b & c \\
d & e & f \\
g & h & i
\end{bmatrix}
+
\begin{bmatrix}
a & d & g \\
b & e & h \\
c & f & i
\end{bmatrix} =
\begin{bmatrix}
2a & b + d & c + g \\
d + b & 2e & f + h \\
g + c & h + f & 2i
\end{bmatrix}
$$
Questo metodo di simmetrizzazione funziona solo per le matrici quadrate, dato che non è possibile sommare matrici di dimensioni diverse, però è immediato.

Esiste un altro metodo che usa la moltiplicazione e funziona per qualunque matrice $A^TA$ è simmetrica:
$$
A_{n \times m}^TA_{m \times n} = S_{n \times n} \\
A_{m \times n}A_{n \times m}^T = S_{m \times m}
$$
per esempio:
$$
\begin{bmatrix}
a & b & c \\
d & e & f
\end{bmatrix}
\begin{bmatrix}
a & d \\
b & e \\
c & f
\end{bmatrix} =
\begin{bmatrix}
a^2 + b^2 + c^2 & ad + be + cf \\
ad + be + cf & d^2 + e^2 + f^2
\end{bmatrix}
$$
Queste regole diventano utili nel calcolo statistico perché la diagonale principale diventa la varianza delle righe della matrice, mentre la diagonale secondaria è la covarianza delle righe della matrice.

Alcune proprietà:

* La somma di due matrici simmetriche è una matrice simmetrica
* Il prodotto di due matrici simmetriche non è una matrice simmetrica

#### Prodotto di Hadamard/Schur (approfondimento)

Date due matrici ${\displaystyle A,B}$ di dimensioni uguali ${\displaystyle m\times n}$, il prodotto di Hadamard, indicato come ${\displaystyle A\circ B}$ è una matrice con le stesse dimensioni degli operandi i quali elementi sono dati da: ${\displaystyle (A\circ B)_{i,j}=(A)_{i,j}(B)_{i,j}}$.

Cioè moltiplica ogni elementi di $A$ per il corrispondente elemento di $B​$, questo implica che il prodotto è commutativo, associativo e distributivo.

Nel caso di prodotti di matrici diagonali, il prodotto standard $AB$ è uguale ad $A \circ B$

#### Prodotto di matrici simmetriche

Partendo da un esempio:
$$
\begin{bmatrix}
a & c \\
c & d
\end{bmatrix}
\begin{bmatrix}
e & f \\
f & g
\end{bmatrix}
=
\begin{bmatrix}
ae + cf & af + cg \\
ce + df & cf + dg
\end{bmatrix}
$$
Se vogliamo tenere la simmetria possiamo cambiare i valori di $a$ ed $e$, infatti:
$$
a = d, e = g \\
\begin{bmatrix}
d & c \\
c & d
\end{bmatrix}
\begin{bmatrix}
g & f \\
f & g
\end{bmatrix}
=
\begin{bmatrix}
dg + cf & df + cg \\
cg + df & cf + dg
\end{bmatrix}
$$
Questa proprietà non si applica a matrici più grosse di $2 \times 2$.

#### Divisione tra matrici

Se intendiamo la divisione come prodotto tra un elemento ed il reciproco del secondo elemento allora la divisione tra matrici diventa: $\displaystyle \frac{A}{B} = A B ^{-1}$.

Nasce un problema, cioè $B^{-1}$ dev’essere invertibile affinché si possa fare la divisione, una volta verificato questo valgono tutte le proprietà già mostrate del prodotto.

#### Rango di una matrice

Il rango di una matrice, indicato con $rk(A)$, $r(A)$ o $rank(A)$, è il massimo numero di vettori riga linearmente indipendenti tra loro o il massimo numero di vettori colonna linearmente indipendenti, questo perché il rango per righe e per colonne coincidono.

Il rango è un numero intero non negativo, quindi un numero $\N$, indica la dimensionalità delle informazioni interne alla matrice.

Alcuni esempi intuitivi:
$$
\begin{align}
A = \begin{bmatrix}
1 & 1 & 0 & 0 & 2 & 1 & 2 \\
2 & 1 & 0 & 5 & 6 & 2 & 1
\end{bmatrix} \quad
r(A) = 2 \\
B = \begin{bmatrix}
1 & 1 & -4 \\
2 & -1 & 2
\end{bmatrix} \quad
r(B) = 2
\end{align}
$$
Il massimo rango possibile è dato da $r(A_{m \times n}) \le min(m,n)$, se $r(A_{m \times n}) = min(m,n)$ allora la matrice ha “Rango massimo”.

Una proprietà importante del rango di una matrice è che la matrice è **invertibile** se e solo se ha **rango massimo**.

Esistono diversi metodi per calcolare il rango:

* Conta il numero di colonne o righe linearmente indipendenti
* Riduci la matrice in forma triangolare e conta il numero dei pivot
* Calcola la SVD, ovvero la decomposizione ai valori singolari, contando il numero dei valori singolari diversi da 0
* Calcola la decomposizione spettrale (eigen decomposition) contando gli autovalori diversi da 0

Come agisce il rango di matrici sommate o moltiplicate?

Seppur non sia possibile analizzare a priori il valore del rango, conoscendo le due matrici iniziali, dopo un’operazione, possiamo comunque dire qualcosa su quanto sarà al massimo.

* $r(A+B) \le r(A) + r(B)$.
* $r(AB) \le min \{ r(A), r(B) \}$
* $r(A) = r(A^TA) = r(A^T) = r(AA^T)$
* Il prodotto scalare-matrice non varia il valore di $r(A)$.

Calcolare il rango di una matrice e il rango di una matrice concatenata con un vettore è possibile verificare se il vettore è nello span della matrice, se il rango è uguale significa che il vettore è interno allo span della matrice, altrimenti no ed il rango aumenta di 1.

#### Spazi della matrice

La spazio delle colonne di una matrice è mostrato con $C(A)​$ ed è il sottospazio generato dai vettori colonna della matrice, la definizione formale è: $C(A) = \{ \beta_1 a_1 + ... + \beta_n a_n, \beta \in \R \} = span(a_1 + ... + a_n )​$

Analogamente lo spazio delle righe, $R(A) = C(A^T)$ è il sottospazio generato dai vettori riga della matrice.

Il nullspace, kernel o nucleo, è un sottoinsieme del dominio della funzione, e viene spesso indicato come  $\ker (f)$. Eredita le stesse proprietà algebriche dello spazio in cui vive, ed è strettamente collegato all'immagine della funzione.

Per quanto riguarda una matrice:
$$
A \in \R^{m \times n}, v \in \R^n, v \ne 0 \\
\ker(A) := \{ v \in \R^n, v \ne 0 : Av=0 \}
$$
Esempio:
$$
\begin{bmatrix}
1 & 1 \\
2 & 2
\end{bmatrix}
\begin{bmatrix}
v_1 \\ v_2
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0
\end{bmatrix} \\
\ker(A) = \alpha 
\begin{bmatrix}
1 \\ -1
\end{bmatrix}
$$

* Se una matrice ha nucleo vuoto, allora ha colonne linearmente indipendenti
* Se una matrice ha nucleo non vuoto, allora ha delle colonne linearmente dipendenti

Grazie a quanto appreso finora possiamo dice che c’è una soluzione alla formula $AX = B$ se $B \in C(A)$.

Se non c’è una soluzione possiamo cercare la soluzione più vicina con: $A \hat{X} = \hat{B}$, dove $\hat{B} \in C(A)$, a questo punto la soluzione migliore è la minore data da $||\hat{B} - B||$, questa è la “least square solution”, o metodo dei minimi quadrati.

Se $B = 0$ allora la soluzione parla di $A X = 0$ e parliamo di eigenvalue ed eigenvector, autovalore ed autovettore, l’equazione diventa quindi: $(A \pm \alpha I)X = 0$ dove $\alpha$ è l’autovalore e $X$ l’autovettore.

Proprietà matrice $M \times N$:

* $dim(C(A)) + dim (ker(A^T)) = M$
* $dim(C(A^T)) + dim (ker(A)) = N​$

### Risolvere sistemi lineari (sintesi dei concetti importanti dell’algebra delle matrici)

#### Dal sistema lineare alla matrice

Sinteticamente, essendo stato già affrontato, la conversione di un sistema in matrice prevede di ordinare le incognite, ci interessano i coefficienti, se non esiste l’incognita in una riga il coefficiente è $0​$, e intabellarle, per esempio:
$$
\begin{cases}
2x + 3y - 4z = 5 \\
-x + 7y + 9z = 7
\end{cases}
=
\begin{bmatrix}
2 & 3 & -4 & 5 \\
-1 & 7 & 9 & 7
\end{bmatrix}
$$
È importante tenere i valori noti a destra, possiamo anche specificare l’ordine delle variabili usando un altro tipo di matrice, più flessibile:
$$
\begin{array}{c,c,c|c}
x & y & z & b \\
\hline
2 & 3 & -4 & 5\\
-1 & 7 & 9 & 7
\end{array}
$$
In particolare la conversione a matrice passa attraverso il seguente metodo:
$$
\begin{cases}
2x + 3y - 4z = 5 \\
-x + 7y + 9z = 7
\end{cases}
\iff
\begin{bmatrix}
2 & 3 & -4\\
-1 & 7 & 9
\end{bmatrix}
\begin{bmatrix}
x \\ y \\ z
\end{bmatrix}
=
\begin{bmatrix}
5 \\ 7
\end{bmatrix}
$$

#### Eliminazione di Gauss-Jordan

Ricapitolando l’algoritmo per la riduzione al minimo, in forma di Gauss-Jordan:

1. (opzionale): ridurre al minimo le righe, ovvero portarle ad essere irriducibili, con elementi fra loro indivisibili
2. Se l’n.esimo elemento della n.esima riga è 0, scambialo con un’altra riga che ha come n.esimo elemento un numero diverso da 0; altrimenti procedi
3. Moltiplica la n.esima riga per uno scalare in modo che il primo elemento sia 1
4. Somma dei multipli della n.esima riga a tutte le altre righe per ottenere zero sulla n.esima colonna
5. Ripeti i passaggi dal 2 finché non finiscono le righe, incrementa n

Gli elementi diversi da $0$ che hanno a sinistra e sotto solo zeri, possono anche non essere sulla diagonale principale, sono detti pivot e sono importanti per altre applicazioni.

Dopo l’eliminazione il rango della matrice è identico, così come lo spazio delle righe ma lo spazio delle colonne può esser ridotto.

### Determinante

Il determinante si può calcolare solo se la matrice è quadrata, se il determinante è $0$ allora la matrice ha colonne linearmente dipendenti.

Per esempio in una matrice $2 \times 2$: 
$$
det(A) = \begin{vmatrix}
a & \gamma a \\
b & \gamma b
\end{vmatrix}
=
a\gamma c - \gamma ac = \gamma(ac - ac) = \gamma 0 = 0
$$
Il determinante di una matrice simmetrica $2 \times 2$ è particolarmente più semplice: 
$$
det(A) = \begin{vmatrix}
a & b \\
b & c
\end{vmatrix}
=
ac - b b = ac - b^2
$$
Il determinante della matrice identità $I_2$:
$$
det(I_2) = 
\begin{vmatrix}
1 & 0 \\
0 & 1
\end{vmatrix} =
1 - 0 = 1
$$
Se invertiamo le righe possiamo notare una proprietà del determinante:
$$
det(I_{2}^{re}) = 
\begin{vmatrix}
0 & 1 \\
1 & 0 
\end{vmatrix} = 0 -1 = -1
$$
Quando si spostano le righe il determinante cambia di segno.

Il determinante è importante per trovare l’inversa di una matrice.

Proprietà del determinante per $A \in \R^{n \times n }$:

* Se tutti gli elementi di una riga o colonna sono nulli allora $|A| = 0$
* Se $A​$ ha due righe o colonne uguali o proporzionali allora $|A| = 0​$
* Se una riga o colonna è combinazione lineare di due o più altre righe o colonne ad essa parallele allora $|A| = 0$ (ne consegue che il determinante è $0$ quando una matrice è linearmente dipendente)
* Se su $A$ vengono scambiate due righe o colonne allora $|A|$ cambia di segno
* $|AB| = |A| |B|$
* $|A^{-1}| = (|A|)^{-1}$
* $|\alpha A| = \alpha^n |A| \quad \alpha \in \R$
* $|A| = |A^T|$

Il calcolo del determinante è particolarmente complicato e lungo, essendo già stato affrontato riporto le formule finali ed il loro significato nello spazio:

* Determinante matrice $2 \times 2$, rappresenta l’area del pentagramma di vertici $(0,0), (a,b), (c,d), (a+c, b+d)$: 
	* $$\displaystyle \begin{vmatrix} a & b \\ c & d \end{vmatrix} = ad - bc$$
* Determinante matrice $3 \times 3​$: 
  * $${\displaystyle {\begin{vmatrix}a & b & c\\d & e & f\\g & h & i\end{vmatrix}}=a(ei - fh)-b(di-fg)+c(dh-eg)\\=aei+bfg+cdh-ceg-bdi-afh}​$$
* Determinante matrice $4 \times 4​$: 
  * $$a \cdot
    \begin{vmatrix}
    f & g & h \\
    j & k & l \\
    n & o & p
    \end{vmatrix} b \cdot
    \begin{vmatrix}
    e & g & h \\
    i & k & l \\
    m & o & p
    \end{vmatrix} c \cdot
    \begin{vmatrix}
    e & f & h \\
    i & j & l \\
    m & n & p
    \end{vmatrix} d \cdot
    \begin{vmatrix}
    e & f & g \\
    i & j & k \\
    m & n & o
    \end{vmatrix} = \\
    a ( f (k p - l o) - g (j p - l n) + h (j o - k n) ) - 
    b ( e (k p - l o) - g (i p - l m) + h (i o - k m) ) + \\
    c ( e (j p - l n) - f (i p - l m) + h (i n - j m) ) - 
    d ( e (j o - k n) - f (i o - k m) + g (i n - j m) )​$$

Per ricavare una matrice dal suo determinante partiamo da degli esempi:
$$
\begin{vmatrix}
a & b \\
c & \delta
\end{vmatrix}
= d \iff
a \delta - bc = d \iff
\delta = \frac{d + bc}{a}
$$
In questo caso è importante sapere almeno 4 valori, l’incognita può essere qualunque.

Esempio 2:
$$
\begin{vmatrix}
\delta & 4 \\
1 & \delta
\end{vmatrix} = 0 \iff 
\delta^2 - (1)(4) = 0 \iff
\delta^2 = 4 \iff \delta \pm 2
$$
Esempio 3 (shift di una matrice):
$$
\begin{vmatrix}
5 - \delta & -\frac{1}{3} \\
-3 & 5 - \delta
\end{vmatrix} = 0 \iff
(5 - \delta)(5 - \delta) - 1 = 0 \iff
\delta^2 + 25 - 10 \delta - 1 = 0 \iff \\
\delta^2 - 10 \delta + 24 = 0 \iff
(\delta - 6)(\delta - 4) = 0 \iff \delta = 6 \or \delta =4
$$

### Matrice inversa

Una matrice quadrata è invertibile se esiste un’altra matrice tale che il loro prodotto matrice per matrice restituisce la matrice identità, cioè: $A_{n \times n} B_{n \times n} = I_{n}$.

Una matrice invertibile è detta regolare, una matrice non invertibile è detta singolare.

Se prendiamo l’equazione fondamentale dell’algebra lineare: $Ax = b \iff A^{-1}Ax = A^{-1}b \iff Ix = A^{-1}b \iff x = A^{-1}b$.

A questo punto si capisce l’importanza della matrice inversa.

Essendo il prodotto tra matrici non commutativo è importante non fare il seguente errore:
$$
AB = C \iff ABB^{-1} = CB \iff AI = CB \iff A = CB \quad\quad B^{-1}AB \ne CB^{-1}(!)
$$
Il calcolo dell’inversa è possibile farlo in vari metodi:

* Eliminazione di Gauss
* Matrice dei cofattori

Il primo metodo si scompone nei seguenti passaggi:

1. $B = (A | I)$
2. Applicare l’algoritmo di Gauss-Jordan
3. $B = (I|C)$, $C = A^{-1}$

Questo perché si va a costruire il sistema $C[A|I] = [I|A^{-1}] \iff C = A^{-1}$, dove $C$ è l’insieme di tutte le operazioni sulle righe e sulle colonne di $A$.

Il secondo metodo usa l’equazione: $\displaystyle A^{-1} = \frac{1}{det(A)} \cdot \left( cof(A) \right)^T$

$cof_{ij}(A) := (-1)^{i+j} \cdot det(A_{ij})$

Per esempio il cofattore 2,3 lo ricaviamo così:

![](assets/4380db912fd85e8b84b898f5522ccb15a3fa1172.svg)

L’inversa, quando esiste, è unica, dimostriamolo per assurdo:
$$
B \ne C \\
\begin{cases}
AB = I \\
AC = I
\end{cases} \iff
\begin{cases}
A^{-1}AB = A^{-1}I \\
A^{-1}AC = A^{-1}I
\end{cases} \iff
\begin{cases}
B = A^{-1} \\
C = A^{-1}
\end{cases} \implies
B = C
$$

Proprietà ($A, B, C$ invertibili):

* $(ABC)^{-1} = C^{-1}B^{-1}A^{-1}$ (in questo caso $ABC$ può essere invertibile, ma non presi singolarmente)
* $det(A) \ne 0$ (ha colonne linearmente indipendenti)
* $r(A_{n \times n}) = n$ (rango massimo)
* $A^T$ è invertibile
* $Ax = 0 \iff x =0$ ($ker(A) = \emptyset$ (ha colonne linearmente indipendenti)
* $(A^{-1})^{-1} = A$
* $(A^T)^{-1} = (A^{-1})^T​$

### Proiezioni ed ortogonalizzazione

#### Proiezioni in $\R^2$ ed $\R^n$

Partendo da $\R^2​$ in generale la proiezione di un punto su un vettore la otteniamo tracciando la retta passante per $b​$, il punto che vogliamo proiettare, e che incontra con un angolo retto il vettore $a​$, di conseguenza la retta è ortogonale ed il punto di incontro è la proiezione stessa.
$$
(v - D u)\cdot u = 0 \iff
u \cdot v - u \cdot u D = 0 \iff
||u||^2 D = u \cdot v \iff
D = \frac{u^T v}{u^T u} \\
proj_u(v) = \frac{u \cdot v}{||u||^2}u
$$

| Interpretazione geometrica                 | Formula algebrica                                          |
| ------------------------------------------ | ---------------------------------------------------------- |
| ![1550591725848](assets/1550591725848.png) | $\displaystyle D = proj_u(v) = \frac{u \cdot v}{||u||^2}u$ |

Generalizzando per $\R^n$ dobbiamo riscrivere la formula.

NB: $A$ dev’essere ortogonale, cioè una matrice quadrata con rango massimo.
$$
A^T(b-Ax) = 0_n \iff 
A^T b - A^TAx = 0_n \iff 
A^TAx = A^Tb \iff \\
(A^T A)^{-1}(A^T A)x = (A^T A)^{-1}A^Tb \iff I_n x = (A^T A)^{-1} A^Tb \iff x = (A^T A)^{-1} A^T b \iff \\
x = (A^TA)^{-1} A^Tb \iff x = A^{-1}A^{-T}A^Tb \iff x = A^{-1}b
$$

#### Vettori ortogonali e paralleli e scomposizione

Dal punto di vista geometrico quello che si va a fare è questo:

![1550593848151](assets/1550593848151.png)

Il vettore blu tratteggiato è il nostro vettore di partenza, chiamiamolo $w$.

$v$ è invece il vettore rosso di riferimento.

Dal punto di vista algebrico la scomposizione o decomposizione, passa quindi attraverso i seguenti passaggi:
$$
w = w_{||v} + w_{\perp v} \\
\begin{align}
& w_{||v} = proj_{v}(w) = \frac{w^Tv}{v^Tv}v = \frac{v \cdot w}{||v||^2}v \\
& w_{\perp v} = w - w_{||v}
\end{align}
$$
Dimostriamo l’ortogonalità:
$$
w_{\perp v} \cdot w_{||v} = 0 \\
\begin{align}
& \left( w - \frac{v \cdot w}{||v||^2}v \right) \cdot \left( \frac{v \cdot w}{||v||^2}v \right) = 0 \iff
\frac{w^T v}{v^T v}v^T w - \frac{w^T v}{v^T v}\frac{w^T v}{v^t v} v^Tv = 0 \iff \\
& \iff \frac{(w^T v)^2}{v^Tv} - \frac{(w^T v)^2}{v^T v} = 0
\end{align}
$$

#### Ortogonalità e ortonormalità

Una matrice ortogonali, di solito indicata con $Q$, ha tutte le colonne ortogonali, cioè il prodotto scalare tra una colonna e tutte le altre è $0$ ed ogni colonna ha norma $1$.

Definizione formale:
$$
\left< Q_i, Q_j \right> = \begin{cases}
1 & i = j \\
0 & i \ne j
\end{cases}
$$
Cioè il prodotto scalare è uguale ad $1$ quando la colonna in esame è la stessa, $0$ altrimenti.

Inoltre una matrice (che tra l’altro è una base) è ortonormale se ogni vettore colonna ha norma $1$.

La matrice identità è ortogonale.

Dalla definizione possiamo passare ad un’altra forma della matrice ortogonale se è quadrata:
$$
Q^TQ = I \iff Q^{-1} Q = I
$$
Quindi la matrice ortogonale ha trasposta e inversa identiche.

Una matrice ortogonale può anche non essere quadrata.

Nota che:

* In $\R^2$ l’unico modo per avere una matrice ortogonale è prendere la matrice identità e ruotarla.
* Una matrice ortogonale in $\R^3$ rispetta la regola della mano destra (o sinistra).

Esempio in $\R^3$:
$$
Q = \frac{1}{3} 
\begin{bmatrix}
2 & 1 \\
-2 & 2 \\
1 & 2
\end{bmatrix} \\
Q^T Q = I \iff 
\frac{1}{3}
\begin{bmatrix}
2 & -2 & 1 \\
1 & 2 & 2
\end{bmatrix}
\begin{bmatrix}
2 & 1 \\
-2 & 2 \\
1 & 2
\end{bmatrix} =
\begin{bmatrix}
1 & 0 \\
0 & 1
\end{bmatrix}
$$
Nota che la trasposta deve comparire prima perché l’uguaglianza sia corretta.

#### Ortogonalizzazione di Gram-Schmidt e scomposizione QR

Intuitivamente, pensiamo una matrice come una lista di colonne, modifichiamo la seconda colonna rendendola ortogonale alla prima, si prende la terza colonna rendendola ortogonale alla prima e alla seconda colonna, etc.

In generale:
$$
A = \{ v_1, v_2, ..., v_n\} \\
\begin{align}
& w_1 = v_1 \\
& w_2 = v_2 - \frac{v_2 \cdot w_1}{w_1 \cdot w_1} w_1 \\
& w_3 = v_3 - \frac{v_3 \cdot w_1}{w_1 \cdot w_1}w_1 - \frac{v_3 \cdot w_2}{w_2 \cdot w_2} w_2 \\
& \vdots \\
& w_i = v_i - \frac{v_i \cdot w_1}{w_1 \cdot w_1} w_1 - \frac{v_i \cdot w_2}{w_2 \cdot w_2} w_2 - ... - \frac{v_i \cdot w_{i-1}}{w_{i-1} \cdot w_{i-1}} w_{i-1} \\
& w_i = v_i - \sum_{j = 1}^{i -1} \frac{v_i \cdot w_j}{w_j \cdot w_j} w_j, \quad 1 \le i \le n 
\end{align} \\
B = \{w_1, ..., w_n\}
$$
Per rendere infine la base $B$ ortonormale dividiamo i vettori per la loro norma, cioè $e_i = \frac{w_i}{||w_i||}$.

$B$ però ha perso informazioni riguardo $A$, se chiamiamo $B$ come $Q$ allora possiamo avere la forma $QR$ dove non abbiamo perdita di informazioni: $A = QR$.

Per ottenere $R$ partiamo semplicemente dall’equazione:
$$
A = QR \iff Q^T A = Q^T Q R \iff Q^T A = R
$$

#### Matrice inversa tramite scomposizione QR (approfondimento)

Partiamo dall’equazione $A = QR$ e lavoriamo da lì.
$$
A = QR \iff A^{-1} = (QR)^{-1} \iff A^{-1} = R^{-1} Q^{-1} \iff A^{-1} = R^{-1} Q^T
$$
Questo metodo diventa molto più efficiente per calcolare l’inversa di una matrice.

### Eigendecomposition

#### Spiegazione intuitiva

La eigendecomposition è possibile solo in matrici quadrate, questo processo serve per estrarre $m$ eigenvalues e $m$ eigenvectors, in italiano sono chiamati autovalori ed autovettori.

Intuitivamente partiamo da un’applicazione lineare generica: $A v = w$.

Quando questa trasformazione si comporta come: $A v = \alpha v$ abbiamo una proprietà particolare, data dalla combinazione di $A$ e $v$ allora abbiamo $v$ come autovettore e $\alpha$ come autovalore, in italiano si mette il prefisso “auto” perché la trasformazione si sta comportando come uno scalare e quindi una trasformazione da sé a sé.

Questo processo ha un’importante applicazione nella statistica, infatti in un dataset è possibile trovare un eigenvector principale, affiancato da altri eigenvector secondari ortogonali quante sono le dimensioni del dataset, riuscendo a creare una nuova base su cui è semplice lavorare nei dati (PCA).

Un’altra spiegazione intuitiva passa da un esempio col cubo di rubik, supponiamo di avere un cubo di rubik disorganizzato, questo è la nostra matrice iniziale, le rotazioni sul cubo sono gli eigenvector che ci consentono infine di avere una matrice diagonalizzata, i colori corrispondono agli eigenvalues.

#### Trovare gli eigenvalues

Per trovare gli eigenvalues partiamo dall’equazione generica:
$$
Av = \delta v \iff Av - \delta v = 0 \iff (A - \delta I)v = 0
$$
Dove $(A - \delta I)$ è una matrice con $ker$ non nullo.
$$
(A - \delta I) v = 0 \iff |A - \delta I| = 0
$$
Questa forma viene anche chiamata “forma polinomiale caratteristica”.

Esempio 1:
$$
A = \begin{bmatrix}
1 & 5 \\
2 & 4
\end{bmatrix} \\
\begin{align}
& 
\left|	
\begin{bmatrix}
	1 & 5 \\
  	2 & 4
\end{bmatrix}
- \delta
\begin{bmatrix}
1 & 0 \\
0 & 1
\end{bmatrix}
\right|
= 0
\iff
\begin{vmatrix}
1 - \delta & 5 \\
2 & 4 - \delta
\end{vmatrix}
= 0
\iff
(1-\delta)(4-\delta)-10 = 0
\iff \\
&
\delta^2 + 4 - 5 \delta - 10 = 0 \iff 
\delta^2 - 5\delta - 6 = 0 \iff
(\delta - 6)(\delta + 1) = 0
\implies
\delta = 6; \delta = 1
\end{align}
$$
Quindi $\delta = 6$ e $\delta = 1$ sono gli autovalori della matrice.

Esempio 2:
$$
A = 
\begin{bmatrix}
-2 & 2 & -3 \\
-4 & 1 & -6 \\
-1 & -2 & 0
\end{bmatrix} \\
\begin{align}
&
\begin{vmatrix}
-2 -\delta & 2 & -3 \\
-4 & 1-\delta & -6 \\
-1 & -2 & 0-\delta
\end{vmatrix} = 0
\iff \\
& (-2 -\delta)(1-\delta)(-\delta) + 12 - 24 - 3(1 - \delta) - 8\delta + 12(2+\delta) = 0 \iff \\
& (-1)(-2-\delta+2\delta-\delta^2)(\delta) -12 -3 + 3\delta - 8\delta + 24 + 12\delta = 0 \iff \\
& 2\delta -\delta^2 + \delta^3 + 24 - 15 + 7\delta = 0 \iff 9\delta - \delta^2 + \delta^3 + 9 = 0 \iff \delta(9 - \delta + \delta^2) + 9 = 0 \iff \\
& \delta(9 - \delta + \delta^2) = - 9
\implies 
\begin{cases}
\delta = -3 \\
\delta = -1 \\
\delta = 3
\end{cases}
\end{align}
$$
Possiamo dedurre che una matrice $N \times N$ ha $N $ autovalori/eigenvalues. 

Per una matrice $2 \times 2$ esiste un metodo veloce per determinare gli autovalori.
$$
A = 
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix}\\
\begin{align}
&
\begin{vmatrix}
a-\delta & b \\
c & d-\delta
\end{vmatrix} = 0 \iff
(a-\delta)(d-\delta)-bc = 0 \iff
\delta^2 + ad - a \delta - d \delta - bc = 0 \iff \\

& \delta^2 - \delta(a + d) + (ad - bc) = 0
\end{align}
$$
$\delta(a+d)$ è la traccia della matrice, $(ad - bc)$ è invece il determinante quindi:
$$
\delta^2 - tr(\delta) + det(A) = 0
$$
Per alcune matrici particolari non c’è bisogno di calcolare gli autovalori, infatti:

Una matrice diagonale o una matrice triangolare ha come autovalori gli elementi sulla diagonale principale.

#### Trovare gli eigenvectors

Come trovare gli autovettori:

1. Trova gli autovalori $\delta$
2. Per ogni autovalore, trova $v \in ker(A - \delta I)$, $v$ è il nostro autovalore correlato a $\delta $

Esempio:
$$
A = 
\begin{bmatrix}
1 & 2 \\
2 & 1
\end{bmatrix}
\\
\begin{align}
\begin{vmatrix}
1-\delta & 2 \\
2 & 1-\delta
\end{vmatrix} = 0 \iff
(1 - \delta)(1-\delta)-4 = 0 \iff
\delta^2 - 2\delta - 3 = 0 \iff
(\delta - 3)(\delta+1) = 0
\end{align} \\

\delta = 3, -1
$$
Troviamo il primo autovettore:
$$
\delta = 3 \\
\begin{align}
\begin{bmatrix}
1 - 3 & 2 \\
2 & 1 - 3 
\end{bmatrix}
\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0
\end{bmatrix}
\iff
\begin{bmatrix}
- 2 & 2 \\
2 & - 2 
\end{bmatrix}
\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0
\end{bmatrix}
\end{align}
\iff
\begin{cases}
x_1 = 1 \\
x_2 = 1
\end{cases}
$$
Troviamo il secondo autovettore:
$$
\delta = -1 \\
\begin{align}
\begin{bmatrix}
1 - (-1) & 2 \\
2 & 1 - (-1) 
\end{bmatrix}
\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0
\end{bmatrix}
\iff
\begin{bmatrix}
2 & 2 \\
2 & 2 
\end{bmatrix}
\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix}
=
\begin{bmatrix}
0 \\ 0
\end{bmatrix}
\end{align}
\iff
\begin{cases}
x_1 = 1 \\
x_2 = -1
\end{cases}
$$


### Forma quadratica

## Ricette veloci

Una ricetta è una lista di passaggi, quindi questo ricettario mostra solo i passaggi necessari a risolvere problemi standard, non spiega perché e per come.

N.B. Quando si calcola la trasposta di un qualcosa in $\C$ bisogna usare la formula di Hermite sennò il risultato viene sbagliato.

### Prodotto scalare (dot product)

Il prodotto scalare tra due vettori qualunque è:

$\displaystyle a \cdot b = \left< a, b \right> = a^Tb = \sum_{i=1}^n a_i b_i $

### Norma/lunghezza di un vettore

La norma di un vettore qualunque è data da:

$\displaystyle ||a|| = \sqrt{a \cdot a} = \sqrt{\sum_{i = 1}^n a_i^2} $

### Riduzione in forma a scalini di una matrice

1. (opzionale): ridurre al minimo le righe, ovvero portarle ad essere irriducibili, con elementi fra loro indivisibili
2. Se l’n.esimo elemento della n.esima riga è 0, scambialo con un’altra riga che ha come n.esimo elemento un numero diverso da 0; altrimenti procedi
3. Moltiplica la n.esima riga per uno scalare in modo che il primo elemento sia 1
4. Somma dei multipli della n.esima riga a tutte le altre righe per ottenere zero sulla n.esima colonna
5. Ripeti i passaggi dal 2 finché non finiscono le righe, incrementa n

### Proiezione vettore su vettore

La proiezione di un generico vettore $u​$ su $v​$ è data da:
$\displaystyle proj_v(u) = \frac{v \cdot u}{||v||^2}v​$

### Proiezione vettore su span

Come prima cosa bisogna controllare l’ortogonalità dello span $A$.

Per farlo si fa il prodotto scalare tra tutti i componenti di $A$, se è uguale a $0$ possiamo procedere.

Se $v$ è il vettore da proiettare ed $a, b$ i componenti di $A$, dobbiamo calcolare la proiezione $proj_a(v)$ e $proj_b(v)$, sommando i risultati.

### Rette in forma implicita/cartesiana

Forma implicita: $ax + by + c = 0$

Per convertire una retta dalla forma implicita/cartesiana bisogna risolvere il sistema scegliendo il parametro più conveniente, che può essere una qualunque variabile ($x,y,z$ se siamo in $\R^3 $).

C’è da esprimere $x, y$ in funzione del parametro scelto.

Coefficiente angolare: $\displaystyle m = -\frac{a}{b}​$

Termine noto: $\displaystyle q = -\frac{c}{b}$

Date due rette $a_1 x + b_1 y + c_1 = 0$ e $a_2 x + b_2 y + c_2 = 0$:

Sono parallele se: $a_1b_2 - a_2b_1 = 0$

Sono perpendicolari se: $a_1a_2 + b_1b_2 = 0​$

### Rette in forma esplicita/parametrica

Forma esplicita parametrica: $f(x) = mx + q$ o più generico: $P(t) = A + t(B - A)$.

Direzione della retta $v_r = B - A$

Coefficiente angolare: $\displaystyle m = \frac{y_2 - y_1}{x_2 - x_1}​$

Termine noto: $\displaystyle q = \frac{x_2y_1 - x_1y_2}{x_2 - x_1}$

Retta passante per due punti $P_1(x_1; y_1), P_2(x_2;y_2)$: $\displaystyle \frac{y - y_1}{y_2 - y_1} = \frac{x - x_1}{x_2 - x_1} \iff y = \frac{x - x_1}{x_2 - x_1} (y_2 - y_1) + y_1$

Date due rette $f(x) = m_1x+q_1$ e $g(x) = m_2x+q_2$:

Sono parallele se: $m_1 = m_2​$

Sono perpendicolari se: $\displaystyle m_1 = - \frac{1}{m_2} \iff m_1m_2 = -1$

### Terminologia rette

* Parallele: le rette non si incrociano mai
* Perpendicolari: le rette si incrociano e formano un angolo retto
* Sghembe: le rette sono su due piani diversi
* Incidenti: le rette si incrociano
* Coincidenti: le rette sono identiche

### Retta ortogonale a due rette nello spazio

### Distanza minima tra due rette

1. Scrivi le equazioni parametriche delle due rette $r, s$
2. Calcola $r - s$
3. Imponi l’ortogonalità con i due vettori direzione (prodotto scalare tra $r-s$ ed i loro vettori direzione), si costruisce quindi un sistema lineare
4. I punti di minima distanza sono dati dalle equazioni iniziali di $r, s$, sostituisci i due parametri del sistema nelle equazioni di $r, s $
5. La minima distanza è data dalla distanza $P, Q$, quindi dalla norma della differenza dei risultati della sostituzione del passo precedente
6. L’equazione della retta di minima distanza è data dall’equazione della retta passante per i due punti $P, Q$ 

### Matrice di rotazione

$$
\begin{bmatrix}
cos(\theta) & -sin(\theta) \\
sin(\theta) & cos(\theta)
\end{bmatrix}
$$
### Determinante

### Rango di una matrice

Riduci in forma di Gauss (a scalini), conta i Pivot.

### Nucleo

$$
A \in \R^{m \times n}, v \in \R^n, v \ne 0 \\
\ker(A) := \{ v \in \R^n, v \ne 0 : Av=0 \}
$$

* Se una matrice ha nucleo vuoto, allora ha colonne linearmente indipendenti
* Se una matrice ha nucleo non vuoto, allora ha delle colonne linearmente dipendenti

Per il teorema del rango possiamo calcolare il nucleo nel seguente modo, con $n$ numero di colonne:
$$
n = dim(\ker(A)) + r(A)
$$

### Area del parallelogramma/triangolo

Dati 3 vertici o 2 di cui uno è sottinteso esser l’origine ($a​$, $b​$, $O​$), per calcolare l’area del parallelogramma il procedimento è il seguente:
$$
\begin{align}
& a' = a - O \\
& b' = b - O \\
& A_{pentagramma} = 2A_{triangolo} = \sqrt{||a'||^2 \times ||b'||^2 - (a' \cdot b')^2}
\end{align}
$$

### Iniettività e surgettività di un’applicazione lineare

Supposto di avere un’applicazione lineare $f: V \to W $ per analizzare l’iniettività dobbiamo studiare il nucleo, se il nucleo è $0$ allora l’applicazione lineare è iniettiva.

Per analizzare la surgettività di un’applicazione lineare dobbiamo studiare l’immagine, in particolare:

$dim(V) = dim(ker(f)) + dim (Im(f)) \iff dim(Im(f)) = dim(V) - dim(ker(f))​$.

Se $dim(Im(f)) = dim(W)$ allora l’applicazione lineare è suriettiva.

### Cambio di base

Supposto di aver due basi $B' = \{ v'_1, ... v'_n \}; B'' = \{ v''_1,..., v''_n \}$.

Abbiamo un vettore espresso rispetto alla base $B'$, $w = [w_1, ..., w_n]$.

Scrivi i vettori della base $B'$ come combinazioni lineari dei vettori di $B''$.
$$
v'_1 = a_{11} v''_1 + ... + a_{1n} v''_n \\
\vdots \\
v'_n = a_{n1} v''_1 + ... + a_{nn} v''_n
$$
La matrice di cambio di base è data da:
$$
M_{B', B''} =
\begin{bmatrix}
	a_{11} & a_{12} & ... & a_{1n} \\
	a_{21} & a_{22} & ... & a_{2n} \\
	\vdots & \vdots & \ddots & \vdots \\
	a_{n1} & a_{n2} & \dots & a_{nn}
\end{bmatrix}
$$


### Autovalori, autovettori e autospazio

Gli autovalori sono definiti da $|A - \delta I| = 0$.

Per ogni autovalore il suo autovettore è dato da: $v \in ker(A - \delta I)​$, il sistema quindi lo costruisci mettendo un determinato autovalore sulla diagonale principale di $A​$ (sottrazione termine a termine), moltiplichi $A​$ per un vettore incognita e risolvi l’equazione per $0​$.

La molteplicità algebrica ($m.a.$) di un autovalore è data dalla sua molteplicità nel polinomio semplicemente, quindi se un valore annulla $2$ volte il polinomio, l’autovalore ha molteplicità $2$.

L’autospazio è dato da: $V = \{ x \in \R^n : (\delta I - A) X = 0 \}$, la molteplicità geometrica ($m.g.$) è data dalla dimensione dell’autospazio.

Nota: $m.a. \ge m.g.$

### Base Spettrale

Una base spettrale è data dai vettori che costituiscono le basi degli autospazi.

### Matrice diagonalizzabile

Sia $A​$ una matrice quadrata $n \times n​$, $A​$ è diagonalizzabile se esiste $P​$, una matrice invertibile, tale che: $D = P^{-1}AP \iff PD = AP​$. $P​$ è la matrice diagonalizzante di $A​$.

Una matrice è diagonalizzabile se:

1. Il numero degli autovalori, contati con la loro molteplicità, è pari all’ordine della matrice (ovvero $n$)
2. La molteplicità geometrica di ogni autovalore coincide con la relativa molteplicità algebrica

Le matrici simmetriche sono sempre diagonalizzabili.

Se la matrice è quadrata $n \times n$ che ammette $n$ autovalori distinti allora è diagonalizzabile.

Gli autovalori sono ricavati dalla formula: $|A - \lambda I| = 0$

### Complemento ortogonale

Data una base di riferimento $S = [v_1, ..., v_n]$, i quali componenti vettoriali li indichiamo con $v_i = s_1, s_2, ..., s_n$ con $1 \le i \le m​$.

Allora 
$$
S^{\perp} = 
\begin{bmatrix}
s_{11} & s_{12} & ... & s_{1n} \\
s_{21} & s_{22} & ... & s_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
s_{m1} & s_{m2} & \dots & s_{mn} 
\end{bmatrix}
\begin{bmatrix}
x_1 \\
x_2 \\
\vdots \\
x_n
\end{bmatrix}
=
0_m
$$

### Forma quadratica

Data una forma quadratica:
$$
\begin{align}
& H(x) = ax^2 \\
& H(x,y) = ax^2 + by^2 + cxy\\
& H(x,y,z) = ax^2 + by^2 + cz^2 + dxy + exz + fyz
\end{align}
$$
La forma quadratica $H​$ in uno spazio vettoriale reale $V​$ si dice: 

* definita positiva se per ogni vettore $v \ne 0, v \in V$ si ha $Q(v) > 0​$
* definita negativa se per ogni vettore $v \ne 0, v \in V$ si ha $Q(v) < 0​$
* semi-definita positiva se per ogni vettore $v \ne 0, v \in V$ si ha $Q(v) \ge 0$
* semi-definita negativa se per ogni vettore $v \ne 0, v \in V$ si ha $Q(v) \le 0$

## Collezione di definizioni

### Sistema lineare

Un sistema lineare è un insieme di $m$ equazioni lineari in $n$ incognite che devono essere verificate
contemporaneamente, cioè sono equazioni di primo grado in più incognite.

### Somma di vettori

$\forall x,y \in \R^n, x + y = [x_1 + y_1, x_2 + y_2, ..., x_n + y_n]$

### Prodotto scalare-vettore

$\forall x \in \R^n, \forall \alpha \in \R : \alpha x = [\alpha x_1, \alpha x_2, ..., \alpha x_n]$


### Proprietà di somma tra vettori e prodotto scalare-vettore

1. Proprietà associativa della somma tra vettori: $\forall x,y,z \in \R^n : x + (y + z) = (x + y) + z$
2. Proprietà commutativa della somma tra vettori: $\forall x, y \in \R^n : x + y = y + x$
3. Proprietà distributiva del prodotto scalare-vettore verso la somma: $\forall x,y \in \R^n, \forall \alpha \in \R : \alpha(x + y) = \alpha x + \alpha y$
4. Proprietà distributiva del prodotto vettore-scalare verso la somma: $\forall x \in \R^n, \forall \alpha, \beta \in \R : (\alpha + \beta)x = \alpha x + \beta x$
5. Proprietà associativa del prodotto scalare-vettore: $\forall x \in \R^n, \forall \alpha, \beta \in \R : \alpha(\beta x) = (\alpha \beta)x$

### Spazio vettoriale

Spazio in cui valgono tutte le proprietà della somma tra vettori e del prodotto con gli scalari.

La dimensione di uno spazio vettoriale è il numero di elementi presenti in ciascuna base di $X$, cioè il numero massimo di vettori indipendenti che si possono trovare in $X$.

Proprietà:

* $0 x = 0 \quad \forall x$
* $ax = 0 \implies a = 0 \or x = 0$

### Spazio euclideo in $\R^n$

Uno spazio euclideo è uno spazio vettoriale in cui valgono gli assiomi e i postulati della geometria euclidea. Si tratta dello spazio di tutte le $n​$-uple di numeri reali, che viene munito di un prodotto interno reale (prodotto scalare) per definire i concetti di distanza, lunghezza e angolo.

Ovvero:

* Fissato un generico intero $n​$, si denoterà con $\R^n​$ l’insieme delle $n​$-uple ordinate di numeri reali. L’intero $n​$ si dirà dimensione di $\R^n​$.
* Dato $x \in \R^n$, con $x = [x_1, x_2, ..., x_n]$, il numero reale $x_i$ è detto “$i$-esima componente” del vettore $x$.
* $\forall x, y \in \R^n : x = y$ se $1 \le i \le n, x_i = y_i$

### Prodotto scalare $\R^n$

$\displaystyle u \cdot v = u_1v_1 + u_2 v_2 + ... + u_n v_n = \sum_{i = 1}^n u_i v_i \quad \forall u, v \in \R^n$

Proprietà del prodotto scalare:

1. $\forall u \in \R^n, u \cdot u \ge 0$
2. $\forall u \in \R^n, u \cdot u = 0 \iff u = 0​$
3. $\forall u, v \in \R^n, u \cdot v = v \cdot u$
4. $\forall u, v, w \in \R^n, \forall \alpha, \beta \in \R, (\alpha u + \beta v) \cdot w = \alpha u \cdot w + \beta v \cdot w$
5. $\forall u,v \in \R^n, u \cdot v = 0 \iff u \perp v$


### Altri assiomi

1. $\forall x \in \R^n: 1x = x$
2. $\forall x \in \R^n \exists y \in \R^n : y + x = x \iff y = 0$
3. $\forall x \in \R^n \exists y \in \R^n: y + x = 0 \iff y = -x$
4. $\forall x \in \R^n : 0x = 0$
5. $\forall x \in \R^n : (-1)x = -x$
6. $\forall x, y \in \R^n : -(x+y) = -x - y$

### Norma/magnitudo/lunghezza

$\displaystyle \forall u \in \R^n, ||u|| = \sqrt{\sum_{i = 1}^n u^2_i} = \sqrt{u_1^2 + ... + u_n^2}$

Proprietà della norma:

1. $||x|| \ge 0 \quad \forall x \in \R^n$
2. $||x|| = 0 \iff x = 0​$
3. $||\alpha x|| = |\alpha| ||x|| \quad \forall x \in \R^n, \forall \alpha \in \R​$
4. $ ||x + y || \le ||x|| + ||y|| \quad \forall x, y \in \R$

### Versore

Dato un vettore $u \in \R^n, u \ne 0​$, si definisce versore il vettore ottenuto considerandone il multiplo secondo il reciproco della sua norma: $\displaystyle vers(u) = \frac{1}{||u||} u​$

### Sfera aperta e chiusa

La sfera è data dall’insieme di tutti i punti $x​$ di uno spazio metrico $M​$ le cui distanze da un suo punto dato $x_0​$, chiamato centro della sfera.

Se questi punti sono minori di una grandezza data $r > 0$, cioè il raggio, abbiamo una sfera aperta definita da $p(x, x_0) < r$.

Se questi punti sono minori o uguali di una grandezza data $r > 0$, cioè il raggio, abbiamo una sfera chiusa definita da $p(x, x_0) \le r$.

La superficie sferica della sfera chiusa è data da $p(x, x_0) = r$.


### Coseno dell’angolo fra vettori non nulli

$\displaystyle \forall u, v \in \R^n, cos(\hat{uv}) = \frac{u \cdot v}{||u|| ||v||}$

### Coniugato complesso

Dato un qualunque numero complesso nella forma standard $z = a + bi$ il suo coniugato, indicato con $\overline{z}$ è dato da: $\overline{z} = a + (-1b) i= a - bi $.

### Prodotto scalare in $\C^n$

$\displaystyle \forall u,v \in \C^n, u \cdot v = \sum_{i = 1}^n u_i \overline{v_i}$ dove $\overline{v_i}$ è il coniugato complesso.

Proprietà del prodotto scalare in $\C^n$:

1. $\displaystyle \forall u \in \C^n, u \cdot u = \sum_{i = 1}^n u_i \overline{u_i} = \sum_{i=1}^n ||u_i||^2 \ge 0$
2. $u \cdot u = 0 \iff u = 0$
3. Emi-simmetria: $\forall u, v \in \C^n, u \cdot v = \overline{v} \cdot \overline{u}$
4. Sesquilinearità: $\forall u,v ,w \in \C^n, \forall \alpha, \beta \in \C, u \cdot (\alpha v + \beta w) = \overline{\alpha} u \cdot v + \overline{\beta} u \cdot w$ oppure $(\alpha u + \beta v) \cdot w = \alpha u \cdot w + \beta v \cdot w$

### Proiezione

Una proiezione è una funzione $proj_v(u)​$ che associa ad un vettore $u​$ un altro vettore $v​$ che rispetta le seguenti proprietà:

1. $proj_v(u + w) = proj_v(u) + proj_v(w) \quad proj_v(\alpha u) = \alpha proj_v(u)$
2. $proj_v(proj_v(u)) = proj_v(u)$

### Proiezione vettore su vettore

La proiezione di un vettore $u$ sul vettore $v$ è data da:

$v \ne 0, \displaystyle proj_v(u) = \frac{v \cdot u}{||v||^2}v$

Ha le seguenti proprietà:

1. Eredita le proprietà della proiezione in generale
2. $\alpha (proj_u(u)) = \alpha u$
3. $(u - proj_v(u)) \cdot v = 0$

### Proiezione vettore su uno span qualsiasi

Dato un sotto-spazio $V$ di $\R^n$, $B = \{b_1, b_2, ..., b_r \}$ una base ortonormale di $V$.

La proiezione di un vettore $u​$ su $V​$ è il vettore:

$proj_V(u) = (u \cdot b_1) b_1 + (u \cdot b_2) b_2 + ... + (u \cdot b_r) b_r$

Nota: $(V^{\perp})^{\perp} = V, \R^n = V \oplus V^{\perp} \implies u - proj_V(u) \in V^{\perp}​$, ovvero la proiezione di $u​$ su $V​$ è unica e non dipende dalla base ortonormale.

### Distanza tra vettori

Per ogni coppia di vettori $x,y$ in qualsiasi spazio, si definisce la distanza come $dist(x,y) = ||x - y||$.

Proprietà:

1. $dist(x,y) \ge 0$
2. $dist(x,y) = 0 \iff x = y$
3. $dist(x,y) = dist(y,x)$
4. $dist(x,y) \le dist(x,z) + dist(y,z)$

### Area del parallelogramma/triangolo

Dati 3 vertici o 2 di cui uno è sottinteso esser l’origine ($a$, $b$, $O$), per calcolare l’area del parallelogramma il procedimento è il seguente:
$$
\begin{align}
& a' = a - O \\
& b' = b - O \\
& A_{pentagramma} = 2A_{triangolo} = \sqrt{||a'||^2 \times ||b'||^2 - (a' \cdot b')^2}
\end{align}
$$

### Prodotto vettoriale in $\R^3$

Il prodotto vettoriale in $\R^3​$ è particolarmente importante per le sue applicazioni, è indicato con $\times​$, cross-product, oppure con $\and​$.

Il prodotto vettoriale è definito da $\R^3 \to \R^3​$.
$$
u \times v = 
\begin{vmatrix}
x & y & z \\
u_1 & u_2 & u_3 \\
v_1 & v_2 & v_3
\end{vmatrix}
=
\begin{bmatrix}
\begin{vmatrix}
u_2 & v_2 \\
u_3 & v_3
\end{vmatrix} \\
- \begin{vmatrix}
u_1 & v_1 \\
u_3 & v_3
\end{vmatrix} \\
\begin{vmatrix}
u_1 & v_1 \\
u_2 & v_2
\end{vmatrix} 
\end{bmatrix}
=
\begin{bmatrix}
u_2 v_3 - u_3v_2 \\
-(u_1 v_3 - u_3 v_1) \\
u_1 v_2 - u_2 u_1
\end{bmatrix}
$$
$x, y, z​$ sono i versori degli assi cartesiani.

Proprietà ($a,b,c \in \R^3; \alpha \in \R$) :

* Bilineare: 
	* $(\alpha a) \times b = \alpha(a \times b) = a \times (\alpha b)$
	* $(a + c) \times b = a \times b + c \times b$
	* $a \times (b + c) = a \times b + a \times c$
* $ a\times b = 0 \iff a,b $ linearmente dipendenti
* Anti-commutativo: $a \times b = -b \times a$
* Identità di Jacobi: $a \times (b \times c) + b \times (c \times a) + c \times (a \times b) = 0$ 

### Ortogonalità e ortonormalità

Due vettori si dicono ortogonali se il loro prodotto scalare è 0, cioè $u,v \in \R^n,u \cdot v = 0\implies u \perp v $, in altre parole i due vettori si incrociano formando un angolo di 90 gradi.

Una base è ortogonale se tutti i vettori che formano la base sono ortogonali, la base canonica è sempre ortogonale, cioè $A = [v_1, v_2, ..., v_n], \forall i \ne j, v_i \perp v_j \iff v_i \cdot v_j = 0$.

Una base è ortonormale ha tutti i vettori ortogonali ed ogni vettore ha norma 1, cioè $||v_i|| = 1 \forall v_i \in A$.

### Complemento ortogonale

Si definisce complemento ortogonale il sottospazio $S \subseteq V$ definito da $S^{\perp} := \{ v \in V : v \cdot s = 0 \ \forall s \in S \}$.

### Span

Insieme di tutte le combinazioni lineari dei vettori.

$\displaystyle span(u_1, u_2, ..., u_n) = <u_1, u_2, ..., u_n> := \left\{ \sum_{i = 1}^n \alpha_i u_i,\ \alpha_i \in \R \or \alpha_i \in \C  \right\}$

### Dipendenza lineare

Un sistema di vettori è detto linearmente dipendente se almeno uno di essi è combinazione lineare degli altri. $V = [u_1, u_2, ..., u_n]​$ sono dipendenti $\exists \alpha_1, ... , \alpha_n​$, non tutti nulli tali che $\displaystyle \sum_{i = 1}^n \alpha_i u_i = 0​$.

### Indipendenza lineare

Un sistema di vettori non dipendente si dice linearmente indipendente, quindi: $V = [u_1, ..., u_n]$ indipendenti se $\displaystyle \sum_{i=1}^n \alpha_i u_i = 0 \iff \alpha_i = 0$.

### Base

Una base di uno spazio vettoriale è un sistema di generatori indipendente: $X = <x_1, x_2, ..., x_n>​$, $x_1 ,..., x_n​$ sono tutti indipendenti.

### Sottospazio

Sia $X$ uno spazio vettoriale e $Y$ un sottoinsieme di $X$, cioè $Y \subseteq X$, allora $Y$ è detto sottospazio di $X$ ed ha le seguenti proprietà:

1. $y_1 + y_2 \in Y \quad \forall y_1, y_2 \in Y$
2. $\alpha y_1 \in Y \quad \forall y \in Y, \forall \alpha \in \R$

### Somma diretta

La somma diretta $Z​$ tra $U​$ e $V​$ è uno spazio vettoriale i cui elementi si scrivono in modo univoco come somma di due elementi, uno appartenente a $U​$ e l’altro a $W​$.

Cioè: $z \in Z = U \oplus V \implies \exists! u \in U, v \in V : z = u + v​$. Se i sottospazi sono in somma diretta allora $U \cap V = \{ 0 \}​$.

### Matrice

Una matrice $m \times n$, $m$ righe e $n$ colonne, a termini razionali, reali o complessi, è una funzione $A : \{  1, ..., m\} \times \{ 1, ..., n \} \to \Q \or \R \or \C$.

Indichiamo con un apice $m \times n$, ad esempio $\R^{m \times n}$, l’insieme delle matrici $m \times n$ a termini appartenenti all'insieme numerico indicato.

### Somma tra matrici, prodotto scalare-matrice

| Somma                            | Prodotto scalare-matrice          |
| -------------------------------- | --------------------------------- |
| $(A + B)_{ij} = A_{ij} + B_{ij}$ | $(\alpha A)_{ij} = \alpha A_{ij}$ |

### Matrice quadrata, triangolare, diagonale

Una matrice $A \in \R^{m \times n}​$ è quadrata se e solo se $m = n​$.

Una matrice quadrata viene detta diagonale se $a_{ij} \in A : a_{ij} = 0 \forall i \ne j$.

Una matrice quadrata viene detta triangolare (inferiore) se $a_{ij} \in A : a_{ij} = 0 \forall i>j$, per la triangolare superiore inverti il minore con un maggiore.

### Matrice identità o identica

Una matrice quadrata è detta identica, indicata con $I​$, se è diagonale e se tutti gli elementi sulla diagonale sono uguali a $1​$.
$$
(a_{ij}) = 
\begin{cases}
1 & i = j \\
0 & i \ne j
\end{cases}
$$

### Matrice minore

Una matrice $\R^{k \times k}$ ottenuta sopprimendo da una matrice $A \in \R^{n \times n} n-k$ righe ed $n - k$ colonne viene detta minore di $A$.

I minori principali sono quelli nei quali vengono soppresse righe e colonne dello stesso indice, chiamato “sviluppo di Laplace” o “cofattore”.

### Convenzione di Einstein

Se un prodotto di quantità dipendenti da indici contiene una coppia di indici uguali, è sottintesa una somma di tutti i prodotti ottenuti variando gli indici in tutti i valori possibili.

$\displaystyle u_i v_i = \sum_{i = 1}^n u_i v_i$

### Prodotto tra matrici

Date $A \in \R^{m \times n}$ e $B \in \R^{n \times p}$ si definisce $C =AB \in \R^{m \times p}$ ponendo:
$$
C \ni c_{ij} = A_{ih}B_{hj} = \sum_{h = 1}^n A_{ih}B_{hj}
$$

### Matrice trasposta

Data $A \in \R^{m \times n}$, si definisce la matrice trasposta $A^T = A^* \in \R^{n \times m} : (A^T)_{ij} = A_{ji}$.

La matrice quadrata trasposta uguale a sé stessa è detta simmetrica, la matrice quadrata trasposta uguale a sé coi segni cambiati è detta anti-simmetrica.

### Matrice aggiunta

La matrice trasposta coniugata, chiamata anche matrice aggiunta, è definita da: $A \in \C^{m \times n}, B = A^* = A^H : (B)_{ij} = \overline{A_{ji}}$, praticamente è la trasposta dove sono coniugati i numeri complessi.

### Matrice auto-aggiunta

Una matrice auto-aggiunta è tale che $A = A^T = A^*$, affinché la matrice sia auto-aggiunta deve avere elementi reali sulla diagonale principale e la matrice dev’essere quadrata.

### Matrice regolare e matrice singolare

Una matrice quadrata è regolare se le sue colonne sono indipendenti, altrimenti è singolare.

### Matrice inversa

Una matrice $A \in \R^{n \times n}$ è detta invertibile se $\exists X \in \R^{n \times n} : AX = I$, più semplicemente, annotando $X$ come $A^{-1}$: $A A^{-1} = I$.

Se una matrice non ha inversa è detta singolare, infatti una matrice senza inversa ha colonne linearmente dipendenti.

Una matrice è invertibile se ha colonne indipendenti e formano una base per $\R^n​$.

L’inversa della matrice identità è la matrice identità stessa.

### Determinante

Il determinante di una matrice quadrata $A$ è un numero che descrive alcune proprietà algebriche e geometriche della matrice, viene indicata con $det(A)$ oppure $|A|​$.

Le proprietà del determinante sono:

* $|e| = 1$, $e​$ è l’insieme dei vettori della base canonica
* $|A_1,..., A_i, ..., A_j, ..., A_n| = - |A_1, ..., A_j, ..., A_i, ..., A_n|$, se si scambiano le righe di una matrice, il determinante cambia segno
* $|A_1 + B_1, A_2, ..., A_n| = |A_1, A_2, ..., A_n| + |B_1, A_2, ..., A_n|$
* $|\alpha A_1, A_2, ..., A_n| = \alpha |A_1, ..., A_n|$
* $|A| = 0 \iff$ la matrice è linearmente dipendente

Nel caso di una matrice triangolare superiore abbiamo una facilitazione nel calcolo del determinante:
$$
A = 
\begin{bmatrix}
a & b \\
0 & c
\end{bmatrix}
\quad
|A| = ab \\
B = 
\begin{bmatrix}
a & b & c \\
0 & d & e \\
0 & 0 & f
\end{bmatrix}
\quad
|B| = a 
\begin{vmatrix} 
d & e \\ 
0 & f 
\end{vmatrix} - 0 
\begin{vmatrix} 
b & c \\
0 & f
\end{vmatrix} + 0
\begin{vmatrix} 
b & c \\
d & e
\end{vmatrix}
=
a(d f) = adf \\
|U| = a_{11}a_{22}...a_{nn} \quad U:= \text{Matrice triangolare}
$$


### Operatore/applicazione lineare

È un operatore $A: X \to Y​$ che verifica le seguenti proprietà:

| Additività                                     | Omogeneità                                                 |
| ---------------------------------------------- | ---------------------------------------------------------- |
| $A(x + x') = A(x) + A(x') \forall x, x' \in X$ | $A(\alpha x) = \alpha A(x) \forall x \in X, \alpha \in \R$ |

Inoltre: $A(0) = A(0 x) = 0 A(x) = 0$

Lo spazio $X$ è detto dominio, mentre $Y$ è detto co-dominio o immagine.

### Iniettività, suriettività, biettività

Dato una funzione $A:X \to Y​$ questa si dice:

| Iniettiva                                 | Suriettiva                                 |
| ----------------------------------------- | ------------------------------------------ |
| $\forall x, x' \in X, A(x) = A(x'), x=x'$ | $\forall y \in Y \exists x \in X:A(x) = y$ |

Una funzione biettiva è una funzione sia iniettiva sia suriettiva.

### Applicazione lineare inversa

Se $A: X \to Y$ è iniettiva e suriettiva, è biettiva quindi è invertibile; allora $\exists \ A^{-1} : Y \to X : A(A^{-1}(y)) = y,\ A^{-1}(A(x)) = x \quad x \in X, y \in Y$.

### Forma lineare diagonale

Data $A: X \to X$ lineare, con $dim(X)$ finita e non nulla, fissata una base $<u_1, ..., u_n>$ di $X$, $A$ si dice diagonale rispetto alla base se la sua matrice associata a quella base è diagonale sia nel dominio che nel codominio.

Cioè in notazione di Einstein:

$\displaystyle A(x) = a_{ji} x_i u_j \quad i \ne j \implies a_{ij} = 0 $

Una forma lineare si dice diagonalizzabile se esiste una base rispetto alla quale $A$ è diagonale.

### Nucleo

$ker(A) = \{ x \in X: A(x) = 0 \}$

### Immagine

$A(x) = \{y \in Y : \exists x \in X \text{ per cui } A(x) = y \}$

### Matrice associata ad $A$ ed a $2$ basi

$\displaystyle A(e_i) = \sum_{j = 1}^n A_{ij} e'_j​$

### Matrice di cambio di base

Supposto di aver due basi $B' = \{ v'_1, ... v'_n \}; B'' = \{ v''_1,..., v''_n \}$.

Scrivi i vettori della base $B'$ come combinazioni lineari dei vettori di $B''$.
$$
v'_1 = a_{11} v''_1 + ... + a_{1n} v''_n \\
\vdots \\
v'_n = a_{n1} v''_1 + ... + a_{nn} v''_n
$$
La matrice di cambio di base è data da:
$$
M_{B', B''} =
\begin{bmatrix}
	a_{11} & a_{12} & ... & a_{n1} \\
	a_{21} & a_{22} & ... & a_{n2} \\
	\vdots & \vdots & \ddots & \vdots \\
	a_{1n} & a_{2n} & \dots & a_{nn}
\end{bmatrix}
$$
### Forma bilineare

Siano $V$ e $W$ spazi vettoriali, $V \times W$ è il loro prodotto cartesiano e $K$ un campo qualunque.

Definiamo una forma bilineare $\phi: V \times W \to K$ che associa ad ogni coppia di elementi $v \in V$ e $w \in W$ lo scalare $\phi(v,w) \in K$, lineare su entrambe le componenti $v, w$, cioè fissato uno dei due argomenti la funzione (forma bilineare appunto) è lineare rispetto all'altro.

Proprietà:

* $\phi(v_1 + v_2, w) = \phi(v_1, w) + \phi(v_2,w) \quad \forall v_1, v_2 \in V, \forall w \in W$
* $\phi(v, w_1 + w_2) = \phi(v, w_1) + \phi (v, w_2) \quad \forall w_1, w_2 \in W, \forall v \in V$
* $\phi(\alpha v, w) = \phi(v, \alpha w) = \alpha \phi (v,w) \quad \forall v \in V, \forall w \in W, \forall \alpha \in K$

Se $V$ e $W$ coincidono, la forma si dice bilineare su $V$.

### Spazio duale

Uno spazio duale è lo spazio vettoriale formato da tutti i funzionali lineari $f: V \to K​$ tale che:

* $(f + g)(w) := f(w) + g(w)$
* $(\alpha f)(w) := \alpha f(w)$

### Base duale

Sia $B = \{v_1, ..., v_n \}$ una base di $V$, la base duale $B^* = \{ v_1^*, ..., v_n^* \}$ è definita dalle relazioni:
$$
v_i^*(v_j) = 
\begin{cases}
1 & i = j \\
0 & i \ne j
\end{cases}
$$

### Applicazione inversa

Un’applicazione lineare $A: X \to Y$ è detta invertibile se $\exists A^{-1} : Y \to X$, $A^{-1}$ è detta inversa tale che:

* $A^{-1}(A(x)) = x \quad \forall x \in X$
* $A(A^{-1}(y)) = y \quad \forall y \in Y$

### Rango

Sia $A: X \to Y$ lineare, si definisce il rango di $A$ come la dimensione della sua immagine $dim(Im(A))$

### Tipi di applicazione lineare

Siano $A: X \to Y, B:X \to X$ allora:

| Nome         | Significato                             |
| ------------ | --------------------------------------- |
| Omomorfismo  | Se e solo se $A$ è lineare              |
| Epimorfismo  | Se e solo se $A$ è lineare e suriettiva |
| Monomorfismo | Se e solo se $A$ è lineare ad iniettiva |
| Isomorfismo  | Se e solo se $A$ è lineare e biettiva   |
| Endomorfismo | Se e solo se $B$ è un omomorfismo       |
| Automorfismo | Se e solo se $B$ è un isomorfismo       |

### Isomorfismo canonico

Dato uno spazio vettoriale $X$ reale, di dimensione finita $n > 0$, data una sua base $e_1,..., e_n$, si definisce l’isomorfismo canonico $\phi: X \to \R^n$ relativo alla base $e_1, ..., e_n$ come l’applicazione che associa ad ogni $x \in X$ le proprie coordinate $(x_1, ..., x_n) \in \R^n$ rispetto alla base.

### Forma quadratica

Dato uno spazio vettoriale reale $X$, una funzione $H:X \to \R$ è detta forma quadratica su $X$ se esiste una forma bilineare $\alpha$ su $X$ tale che $H(u) = \alpha(u,u) \forall u \in X$

### Diagonalizzabilità

Data una forma bilineare ($\alpha(u,v)​$) o quadratica ($\alpha(u,u)​$) su uno spazio reale $X​$ di dimensione finita $n​$ è detta diagonale rispetto ad una base $B = [e_1, ..., e_n]​$ se la matrice $\alpha_{ij} = \alpha(e_i, e_j)​$ che la rappresenta è diagonale.

Verrà inoltre detta diagonalizzabile se esiste almeno una base rispetto alla quale essa è diagonale.

Oppure dato uno spazio vettoriale (reale o complesso) $X$ ed un operatore $A:X \to X$ diremo che tale operatore è diagonalizzabile se esistono basi di $X$ formate da autovettori di $A$.

### Autovalore, autovettore, autospazio, spettro e base spettrale

Sia $X$ uno spazio vettoriale finito.

Sia $A:X \to X$ lineare, se $u \in X, u \ne 0, \exists \alpha \in \R : A(u) = \alpha u$ allora $u$ è un autovettore di $A$ e $\alpha​$ il suo autovalore.

L’autospazio relativo ad un certo $\alpha_1$ è $<u_1, ..., u_k>$ con $u_1, ..., u_k : A(u) = \alpha_1 u$ o anche $ker(A-\alpha I)$

L’insieme di tutti gli autovalori di $A$ è detto spettro di $A$. Una base spettrale di $X$ è una base formata da autovettori di $A$.

La molteplicità algebrica di un autovalore è la molteplicità delle radici nel polinomio caratteristico di $A$.

La molteplicità geometrica di un autovalore indica la dimensione dell’autospazio relativo all’autovalore.

Un’applicazione lineare è diagonalizzabile se esistono basi di $X$ formate da autovettori di $A$.

L’equazione caratteristica di una matrice è l’equazione $|A - \alpha I| = 0​$, usata per trovare gli autovalori ed i relativi autovettori.

### Polinomio caratteristico

Data una matrice $A \in \R^{n \times n}$, si definisce equazione caratteristica di $A$ l’equazione $det(A - \alpha I) = 0$; il polinomio $p(\alpha) = det(A - \alpha I)$ è detto polinomio caratteristico di $A$.

### Esponenziale complesso

$$
z \in \C, \quad a, b \in \R \\
e^z = e^{a + bi} = e^{a}e^{bi} = e^a (cos(b) + sin(b)i)
$$

### Derivata

Data una funzione $f(t) = g(t) + h(t)i$, si definisce la sua derivata (complessa) ponendo $f'(t) = g'(t)+h'(t)i$ in ogni punto nel quale entrambe le componenti reali $g$ ed $h$ siano derivabili.

### Operatore autoaggiunto

Sia $A: X \to X$. $A$ è autoaggiunto $\iff (A(u)) \cdot v = u \cdot (A(v)) \quad \forall u,v \in X$.

## Teoremi

### Teorema fondamentale dell’algebra

Ogni polinomio di grado $n \ge 1$ su un campo algebricamente chiuso ammette esattamente $n$ radici (o zeri) nel suddetto campo.

Ne consegue che un polinomio complesso ammette esattamente $n$ radici complesse, contando anche la loro molteplicità, mentre un polinomio reale ammette al massimo $n$ radici reali perché il campo dei numeri reali non è chiuso.

### Teorema della proiezione

Siano $u, v​$ vettori di $\R^n​$, con $v \ne 0​$.

$(u - proj_v(u)) \cdot v = 0$ cioè è ortogonale.



Dimostrazione:
$$
\begin{align}
& (u - proj_v(u)) \cdot v = 0 \iff 
\left( u - \frac{v \cdot u}{||v||^2}v \right) \cdot v = 0 \iff 
\left(
u \cdot v - \frac{v \cdot u}{||v||^2}v \cdot v
\right) = 0 \iff \\
& \iff u \cdot v - u \cdot v = 0
\iff u \cdot v = u \cdot v \iff 0 = 0
\end{align}
$$

### Minima distanza

Sia $Y$ un sottospazio di $X$ di dimensione finita; allora $\forall y \in Y, \forall x \in X ||x - y|| \ge ||x - proj_y(x)||$.

Dimostrazione:
$$
||x - y||^2 = ||x - proj_y(x) + proj_y(x) - y||^2 = ||x - proj_y(x)||^2 + ||proj_y(x) - y||^2
$$
Dal teorema di Pitagora (l’ipotenusa al quadrato è uguale alla somma dei cateti al quadrato) e dalle proprietà della proiezione.

Si ha quindi, infine: $||x - y ||^2 =  ||x - proj_y(x)||^2 + ||proj_y(x) - y||^2$, la disequazione iniziale è verificata.

### Forma geometrica della disuguaglianza di Schwarz

Siano $x,y$ vettori di $\R^n$ o $\C^n$ con $y \ne 0$, si ha $||x|| \ge || proj_y(x)||$.

Dimostrazione:

$x, proj_y(x)$ sono entrambi non-negativi, quindi $||x||^2 \ge ||proj_y(x)||^2$.

$x - proj_y(x)$ è ortogonale rispetto a $y$.

$||x||^2 = ||proj_y(x)||^2 + ||x - proj_y(x)||^2$.

### Disuguaglianza di Cauchy-Schwarz

Siano $x,y​$ vettori di $\R^n​$, si ha $| x \cdot y| \le ||x|| ||y||​$.

Dimostrazione:
$$
\begin{align}
& z = x - proj_y(x) = x - \frac{x \cdot y}{y \cdot y} y \\
& z \cdot y = x \cdot y - \frac{x \cdot y}{y \cdot y} y \cdot y = 0 \\
& x = \frac{x \cdot y}{y \cdot y} y + z \\
& ||x||^2 = \left| \frac{x \cdot y}{y \cdot y} \right|^2 ||y||^2 + ||z||^2 \ge \frac{| x \cdot y |^2}{||y||^2} \\
& ||x||^2 ||y||^2 \ge |x \cdot y|^2 \iff | x \cdot y| \le ||x|| ||y||
\end{align}
$$

### Disuguaglianza triangolare

Siano $x,y$ vettori di $\R^n$ o $\C^n$, allora $||x+y|| \le ||x||+||y||$.

Dimostrazione:
$$
||x+y|| \le ||x|| + ||y|| \iff ||x+y||^2 \le (||x|| + ||y||)^2 \iff \\ 
\iff ||x||^2 + ||y||^2 + 2 x \cdot y \le ||x||^2 + ||y||^2 + 2||x||||y|| \iff x \cdot y \le |x \cdot y|
$$
La disuguaglianza è verificata perché:

1. Sappiamo che $|x \cdot y| \le ||x|| ||y||$ dalla disuguaglianza di Schwarz
2. Inoltre $x \cdot y \le |x \cdot y|$

### Dipendenza lineare

Dati $u \in U$, se un $u_i$ è dipendente vuol dire che esistono $a_1, ..., a_n$ non tutti nulli, tali che $\displaystyle \sum_{i = 1}^n a_i u_i = 0$.

Dimostrazione:
$$
u_j \text{ dipendente} \\
u_j = \sum_{i = 1}^n a_i u_i \iff u_j - \sum_{i = 1}^n a_i u_i = 0 \\
a_j \ne 0 \\
a_j u_j = \sum_{i = 1}^n a_i u_i \iff u_j = \frac{\sum_{i = 1}^n a_i u_i}{a_j}
$$

### Lemma fondamentale

$<a_1, a_2, ..., a_n> = <a_2, a_3, ..., a_n> \iff a_1 ​$ è combinazione lineare degli altri.

Dimostrazione:
$$
A = <a_1, a_2, ..., a_n> \\
A' =<a_2, a_3, ..., a_n> \\
\text{Sicuramente }A' \subseteq A \\
A = A' \implies a_1 \in A' \implies a_1 = \sum_{i = 2}^n \alpha_i a_i \\
B \in A \implies B = \beta_1 a_1 + \sum_{i = 2}^n \beta_i a_i = \beta_1 \sum_{i = 2}^n \alpha_i a_i + \sum_{i = 2}^n \beta_i a_i = \sum_{i = 2}^n \gamma_i a_i
$$

### Lemma dello scambio
$$
B \in A \implies <a_1, ..., a_j, ..., a_n> = <a_1, ..., a_{j-1}, B, a_{j+1}, ..., a_n > \implies \\
\implies B = \sum_{i = 2}^n b_i a_i = b_j a_j + \sum_{i=2}^n b_i a_i \implies a_j = \frac{B}{b_j} + \frac{\sum_{i = 2}^n b_i a_i}{b_j}
$$
Dal lemma fondamentale segue che $a_j$ possa esser eliminato senza cambiare lo span.

### Sistema di generatori dello spazio somma diretta

$$
X = <x_1, ..., x_n>, Y = <y_1, ..., y_m> \iff X + Y = <x_1, ..., x_n, y_1, ..., y_m> \\
$$

Dimostrazione:
$$
\forall z \in (X + Y) \implies \exists x \in X, \exists y \in Y : z = x + y
$$
Quindi $z$ è combinazione lineare di $(X+Y)$, che hanno elementi indipendenti dalla tesi, cioè (notazione di Einstein):
$$
\alpha_i x_i = 0, \beta_i y_i = 0 \implies \forall i\ \alpha_i = 0, \beta_i = 0
$$

### Lemma dei vettori indipendenti

Siano $a_1, ..., a_n$ vettori indipendenti, se $b \not \in <a_1, ..., a_n> \implies b, a_1, ..., a_n$ sono indipendenti.

Dimostrazione per assurdo ($b, a_1, ..., a_n$ dipendenti):
$$
\alpha_i a_i + \beta b = 0, b \ne 0 \\
b = - \frac{\alpha_i a_i}{\beta} \implies b \in <a_1, ..., a_n>
$$

### Lemma di Steinitz o teorema sul massimo numero di vettori indipendenti

Sia $V = span(u_1, u_2, . . . , u_r)$ uno spazio vettoriale generabile con $r$ vettori. Siano $w_1, w_2, . . . , w_s$ $s$ vettori linearmente indipendenti di $V$. Allora $s \le r$.

Dimostrazione 1: per assurdo supponiamo $s > r$.

Consideriamo $w_1$, poiché $V = span(u_1, u_2, ..., u_r)$ allora esistono opportuni “pesi” ($a_r$) tali che: $w_1 = a_1 u_1 + a_2 u_2 + ... + a_r u_r \iff w_1 - (a_1 u_1 + a_2 u_2 + ... + a_r u_r) = 0$

Deve esistere almeno un peso $a_i \ne 0$ altrimenti $w_1 = 0$, ciò non può accadere perché i vettori $w_s$ sono indipendenti.

Se $a_1 \ne 0$ allora: $span(w_1, u_1, u_2, ..., u_r) = span(w_1, u_2, ..., u_r)$.

Poiché: $V \supe span(w_1, u_1, u_2, ..., u_r) \supe span(u_1, u_2, ..., u_r) = V \implies V = span(w_1, u_2, ..., u_r) $.

A questo punto consideriamo $w_2$ dove ci deve essere almeno un peso $a_i \ne 0$ con $i > 1$ altrimenti il vettore è dipendente dagli altri.

Continuando per induzione si ottiene che $w_{r + 1} \in V = span(w_1, w_2, ..., w_r)$ per cui $w_{r+1}$ è legato, linearmente dipendente dagli altri.

Dimostrazione 2:
$$
X \in \R^n, B_x = <x_1, ..., x_n>, m > n \implies <y_1, ..., y_m> \text{Dipendenti} \\
y_1 \in <x_1, ..., x_n> \quad X = <y_1, x_2, ..., x_n> \\
y_2 \in <y_1, x_2, ..., x_n> \quad X = <y_1, y_2, x_3, ..., x_n>
$$
Dati $y$ indipendenti non posso comunque andare oltre $y_n$ perché a quel punto lo span $X$ non ha altri $x$ sostituibili a $y$, questo implica che sicuramente gli $y_{n+1}, ..., y_m$ sono dipendenti.

### Teorema delle dimensioni

Sia $X$ uno spazio di dimensione finita, allora tutte le sue basi hanno lo stesso numero di elementi, detto dimensione di $X$.

Dimostrazione:
$$
X \in \R^n \quad B_{Xx} = < x_1, ..., x_n > \quad B_{Xy} = <y_1, ..., y_m> \\
\begin{cases}
m > n \implies y_1, ..., y_m \text{ dipendenti, quindi la base non è valida} \\
m < n \implies x_1, ..., x_n \text{ dipendenti, quindi la base non è valida} \\
m = n \text{ unica possibilità}
\end{cases}
$$

### Teorema dei generatori

Sia $X$ uno spazio di dimensione finita $n$, allora qualunque sistema di $n$ vettori di $X$ indipendenti tra loro è una base di $X$.

Dimostrazione:
$$
y_1, ..., y_n \text{ Sistema indipendente in }X \\
<y_1, ..., y_n> \subset X \implies \exists b \in X : b \not \in <y_1, ..., y_n> \implies <y_1, ..., y_n, b> \text{indipendente}
$$
Però questo va contro il teorema sul massimo numero di vettori indipendenti perché ci sarebbero $n+1$ vettori indipendenti di $X$.

###

### Regola o teorema di Cramer

Attraverso l’inversa di una matrice è possibile risolvere i sistemi lineari, infatti:
$$
\begin{cases}
ax + by = f \\
cx + dy = g 
\end{cases}
\iff
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix}
\begin{bmatrix}
x \\
y
\end{bmatrix}
=
\begin{bmatrix}
f \\
g
\end{bmatrix}
\iff
\begin{cases}
M = 
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix} \\
\overrightarrow{a} =
\begin{bmatrix}
x \\
y
\end{bmatrix} \\
\overrightarrow{b} =
\begin{bmatrix}
f \\
g
\end{bmatrix} \\
\end{cases}
\iff \\
M \overrightarrow{a} = \overrightarrow{b} \iff \overrightarrow{a} = M^{-1} \overrightarrow{b}
$$
Quindi, in generale, se annotiamo con $x_i$ le incognite, per calcolare la $x_i$esima incognita possiamo usare la formula di Cramer: $\displaystyle x_i = \frac{|A_i|}{|A|}$, dove $A_i$ denota la matrice formata sostituendo la $i$esima colonna di $A$ col vettore $b$.

Dimostrazione:

Supposto che la matrice $A$ sia invertibile (e quindi $|A| \ne 0$), $C^T$ è la matrice dei cofattori $A$ trasposta.
$$
A^{-1} = \frac{C^T}{det(A)} \iff AC^T = det(A)I \\
\begin{align}
\begin{bmatrix}
a_{11} & ... & a_{1n} \\
\vdots & \ddots & \vdots \\
a_{n1} & \dots & a_{nn}
\end{bmatrix}
\begin{bmatrix}
C_{11} & ... & C_{1n} \\
\vdots & \ddots & \vdots \\
C_{n1} & \dots & C_{nn}
\end{bmatrix}
=
\begin{bmatrix}
|A| & ... & 0 \\
\vdots & \ddots & \vdots \\
0 & \dots & |A|
\end{bmatrix} \\
a_{11}C_{11} + a_{12}C_{12} + a_{13}C_{13} + ... = det(A) \text{ Regola dei cofattori}
\end{align}
$$
A questo punto per dimostrare gli zero presenti non sulla diagonale principali basta considerare che quando si va a fare il prodotto tra una riga $i$ ed una colonna $j \ne i$ la matrice risultante ha due righe uguali, linearmente dipendenti e quindi il determinante è $0$.

### Teorema di Syslvester (reale)

Il teorema di Sylvester afferma che se $\phi$ è un prodotto scalare sullo spazio vettoriale reale $V$ di dimensione $n​$ allora:

* Esiste una base ortogonale di $V​$ per $\phi​$
* Due basi ortogonali per $V​$ hanno la stessa segnatura che dipende solo da $\phi​$
* Due prodotti scalari con la stessa segnatura sono congruenti




### Somma diretta

Affinché una somma sia diretta tra $U$ e $V$ bisogna avere $U \cap V = \{ 0 \}$

