# Manuale di matematica

## Simone Romano

### Indice o tabella dei contenuti o sommario

[TOC]

# Introduzione

>  Repetita iuvant, repetitio est mater studiorum.

Questo dicevano i latini ed è il motivo principale per cui ho cercato di costruire (all'ultimo, da bravo procrastinatore professionista) questo manuale di matematica, in cui ho messo tutto quello che mi ricordo e che mi serve, perché sono già sclerato, e spero servirà, sclerando meno nel futuro prossimo, nel corso del mio studio universitario.

Si tratta davvero di un manuale sintetico e pratico, lasciando letteralmente fuori ogni dimostrazione (altrimenti non finirebbe più, anche se dovrei mettercele) che mi dovrebbe essere utile, spero, a passare gli esami di matematica, tra cui analisi 1 e 2, algebra lineare, calcolo numerico e non so che altri diti in culo ci saranno.

Quando parlo di “qualcosa polare” si può tradurre con “qualcosa trigonometrico”.

Non si sostituisce affatto ai libri di matematica, né è sua intenzione, ma immagina di avere un problema da risolvere in fretta oppure non ti ricordi una proprietà (ecco perché ci sono anche algebra, tricazzometria e altri capitoli apparentemente inutili), ora che lo trovi in uno dei libri di matematica (che hanno come minimo 1000 pagine) puoi diventare nonno e spero non sia tua intenzione laurearti a 90 anni, anche se sappiamo entrambi che diventerai vecchio prima di uscirne vivo.

Se qualcuno avrà mai voglia di fare una traduzione in inglese ogni capitolo è organizzato sul modello accademico anglosassone, ecco perché parlo di calcolo e non di analisi.

Un altro motivo per cui riporto questa divisione è perché è più semplice separare (almeno dal mio punto di vista) gli argomenti in tutte queste frazioni più semplici dando una migliore continuità.

## Capitoli completi e incompleti e in lavorazione

I capitoli completi sono:

* [Fondamenti di calcolo infinitesimale (Fundamentals of calculus)](#Fondamenti di calcolo infinitesimale (Fundamentals of calculus))
* [Calcolo infinitesimale 1 (Calculus 1)](#Calcolo infinitesimale 1 (Calculus 1))
	* Potenziamento: Applicazioni delle derivate

Il capitolo in lavorazione è:

* [Algebra lineare (Linear algebra)](#Algebra lineare (Linear algebra))

I capitoli incompleti sono:

* [Fondamenti di matematica (Fundamentals of math)](#Fondamenti di matematica (Fundamentals of math)) - Struttura presente, alcuni sotto-capitoli completi
* [Algebra I & II (Algebra I & II)](#Algebra I & II (Algebra I & II)) - Neanche iniziato, struttura presente (algebra lineare è trattata nel suo capitolo dedicato, gli aspetti puramente algebrici sono trattati nell'introduzione all’algebra delle matrici)
* [Pre-calcolo & trigonometria (Pre-calculus & trigonometry)](#Pre-calcolo & Trigonometria (Pre-calculus & trigonometry)) - Manca trigonometria, geometria analitica ed esempi di equazioni e frazioni complesse
* [Calcolo infinitesimale 2 (Calculus 2)](#Calcolo infinitesimale 2 (Calculus 2)) - Da finire i tipi di serie e le tabelle degli integrali
	* Potenziamento: Applicazioni degli integrali, curve polari e parametriche
* [Calcolo infinitesimale 3 (Calculus 3)](#Calcolo infinitesimale 3 (Calculus 3)) - Neanche iniziato, struttura ancora assente

## Nota sui capitoli

Analisi 1 include i capitoli:

* [Fondamenti di calcolo infinitesimale (Fundamentals of calculus)](#Fondamenti di calcolo infinitesimale (Fundamentals of calculus))
* [Calcolo infinitesimale 1 (Calculus 1)](#Calcolo infinitesimale 1 (Calculus 1))
* [Calcolo infinitesimale 2 (Calculus 2)](#Calcolo infinitesimale 2 (Calculus 2))
* [Calcolo infinitesimale 3 (Calculus 3)](#Calcolo infinitesimale 3 (Calculus 3)) - Parzialmente (Equazioni differenziali di primo ordine)

Algebra Lineare include i capitoli:

* [Numeri complessi](#Numeri complessi) - Parzialmente (Introduzione, Equazioni, Frazioni e Piano complesso, sono comunque ripresi nel sotto-capitolo dedicato)
* [Algebra lineare (Linear algebra)](#Algebra lineare (Linear algebra)) - Ovviamente

Analisi 2 include i capitoli:

* Boh

Calcolo numeri include i capitoli:

* Boh

# Fondamenti di matematica (Fundamentals of math)

## Numeri

### Insiemi numerici

Il primo insieme di numeri è $\N​$, ovvero i numeri “naturali”, in questo insieme ci sono i numeri che puoi “contare”, 0, 1, 2, 3, etc.

Il numero 0 è un punto di domanda ma secondo lo standard ISO è incluso in $\N​$ quindi accettiamolo.

Il secondo insieme è $\Z​$, ci stanno i numeri negativi e i naturali, formando così i numeri “interi” (gli integer dei linguaggi di programmazione).

Il terzo insieme è $\Q​$, ci stanno le frazioni e li chiamiamo “razionali” proprio perché possiamo esprimerli con una frazione; in questo insieme iniziamo a vedere dei numeri che hanno anche infinite cifre decimali (i numeri periodici ma che sono comunque esprimibili tramite frazioni), questi in programmazione sono i float, double o decimal.

Il quarto insieme, il bimbo speciale del gruppo, è $\mathbb{I}$, che include i numeri irrazionali, che non possiamo esprimere tramite frazioni, come $\pi$ ed $e$.

Poi c’è $\R​$, che è semplicemente l’unione di tutti questi insiemi.

Infine abbiamo $\C$, i numeri complessi che è l’insieme più grande e lo incontriamo nel caso delle radici non reali, come $\sqrt{-2}$. 

![img](assets/insieme-dei-numeri-irrazionali.png)

### Elemento neutro

L’elemento neutro è un numero che non modifica il valore di un altro numero, in algebra lineare si parla di identità.

| Insieme                                | Operazione                  | Elemento neutro/identità |
| -------------------------------------- | --------------------------- | ------------------------ |
| Numeri reali $\R$                      | Somma $+$                   | $0$                      |
| Numeri reali $\R$                      | Moltiplicazione $\times$    | $1$                      |
| Interi positivi $\N - \{ 0\}$          | Minor comune multiplo $mcm$ | $1$                      |
| Matrice $m$-per-$n$ $M_{m,n}$          | Somma $+$                   | Matrice zero $O_{m, n}$  |
| Matrice quadrata $n$-per-$n$ $M_{n,n}$ | Moltiplicazione $\times $   | Matrice identità $I_n$   |

### Numero opposto

Un opposto di un numero $a$ è semplicemente il risultato di $-a$, se $a = 0$ allora l’opposto diventa $-0$ che però è sempre $0$, questo è l’unico numero il quale opposto è uguale a sé stesso, questo lo possiamo estendere anche alle matrici zero $O$.

### Operazioni sui segni

Ovviamente i segni sono importanti, sulla somma possiamo dire a priori solo che la somma di due positivi ci dà un numero positivo, mentre la somma tra due numeri negativi ci dà un numero negativo.

Sulle moltiplicazioni invece è tutto già definito:

| Segni dei numeri | Risultato |
| ---------------- | --------- |
| $+, +$           | $+$       |
| $-, -$           | $+$       |
| $+, -$           | $-$       |
| $-, +$           | $-$       |

NB: per somma intendo anche la differenza e per moltiplicazione anche la divisione, questo perché differenza e divisione le possiamo esprimere in termini di somma e moltiplicazione, la stessa moltiplicazione non è altro che una serie di somme.

### Valore assoluto

Il valore assoluto di un numero lo possiamo immaginare come la distanza dall’origine, quest’idea la troviamo anche nel modulo dei numeri complessi e dei vettori in algebra lineare.

Il valore assoluto semplicemente restituisce il valore del numero senza il segno, questo però indica anche che il risultato è sempre un numero positivo.

Per esempio: $|-3| = 3 = |3| = +3​$.

## Fattori e multipli

## Frazioni

## Numeri misti

## Decimali

## Rapporto e proporzione

## Esponenti

## Radicali

## Notazione scientifica

# Algebra I & II (Algebra I & II)

## Operazioni

## Regole delle equazioni

## Equazioni semplici

## Funzioni

## Disuguaglianze

## Grafici

## Sistemi

## Polinomiali

## Fattorizzazione

## Esponenti e radicali

## Rapporto e proporzione

## Espressioni razionali

# Pre-calcolo & trigonometria (Pre-calculus & trigonometry)

## Angoli

## Funzioni


## Geometria analitica

## Numeri complessi

### Introduzione

 Il numero immaginario si indica con $i$ ed ha la seguente proprietà: $i^2 = -1 \iff i = \sqrt{-1}$.

Le espressioni con i numeri complessi vengono trattate come normali espressioni dove si tengono separate la parte “reale” e la parte “immaginaria”.

Di norma si scrive prima il numero reale e poi il numero immaginario.

La forma standard quindi è la seguente: $z = a + bi​$, $a​$ e $b​$ sono numeri reali, la parte reale è $a​$ mentre la parte immaginaria è $b​$.

Un numero complesso non è altro che l’insieme di una parte reale ed una parte immaginaria.

Per esempio:
$$
-1 -8i -4 -i = -(1+4) - (8i + i) = - (5 + 9i)
$$

$$
\sqrt{-9} + \sqrt{9} + 5 + 3i - \sqrt{-4} =
\sqrt{9}\sqrt{-1} + 3 + 5 + 3i - \sqrt{-1}\sqrt{4} =
3i + 8 + 3i - 2i = 8 + 4i
$$

$$
- \sqrt{-25} + 8i^3 + 2i - \sqrt{-4}\sqrt{4} + 3 \sqrt{-9} = 
-(\sqrt{-1}\sqrt{25}) - 8i + 2i - \sqrt{-1}\sqrt{16} + 3 \sqrt{-1}\sqrt{9} = \\
-5i -8i -4i +2i + 9i = 11i - 17i = -6i
$$

Dalla definizione abbiamo un ripetersi dei valori di $i$ in base alla potenza, infatti:

| Potenza | Risultato                         |
| ------- | --------------------------------- |
| $i^0$   | 1                                 |
| $i^1$   | $\sqrt{-1}$                       |
| $i^2$   | $\sqrt{-1}\sqrt{-1} = -1$         |
| $i^3$   | $i \times i^2 = - \sqrt{-1} = -i$ |

Questo significa che possiamo ricondurci sempre a questa tabella al fine di calcolare anche potenze di $i$ molto elevate, ad esempio: $i^{202} = i^{200} \times i^2 = -1 (i^4)^{50} = -1(1) = -1$

### Equazioni complesse (todo)

### Frazioni complesse (todo)

#### Razionalizzare un denominatore immaginario

Può essere utile a volte razionalizzare un denominatore dove compare $i​$, è utile a questo fine tenere presente i prodotti notevoli.

Esempio:
$$
\frac{3}{5+3i} =
\frac{3}{5+3i} \times \frac{5-3i}{5-3i} =
\frac{15 - 9i}{25 - 9i^2} =
\frac{15 - 9i}{25 -9(-1)} =
\frac{3(5 - 3i)}{25 + 9} =
\frac{3(5 - 3i)}{3(12)} =
\frac{5-3i}{12}
$$

### Piano complesso

Se si prende un numero complesso $z$ in forma standard $z = a + bi$ è possibile disegnare su un grafico il punto, in particolare l’asse delle $x$ è l’asse dei numeri reali $\R$, mentre l’asse delle $y$ è l’asse dei numeri reali $\R$ moltiplicata per $i$, quindi il piano immaginario descritto da $x, y$ può essere usato per rappresentare qualunque numero complesso e reale.

#### Operazioni sul piano complesso

Trattandosi quindi di un normale punto $z = (a,b)​$ possiamo trovare la sua distanza calcolando il modulo, possiamo anche costruire vettori somma, differenza, prodotto, etc.

Il modulo, cioè la distanza tra il punto e l’origine, di un numero complesso è definito come: $||z|| = \sqrt{(a)^2 + (b)^2}​$, nota che se la componente immaginaria è 0 il modulo è esattamente il valore assoluto del numero.

Possiamo trovare la distanza tra due punti usando la solita formula: $\sqrt{(a_1 - a_2)^2 + (b_1 - b_2)^2}$ con $a_1, b_1 = z_1$ e $a_2, b_2 = z_2$.

Lo stesso vale per trovare il punto medio, per esempio: $\displaystyle a_m = \frac{a_1 + a_2}{2}, b_m = \frac{b_1 + b_2}{2}$.

### Forma polare del numero complesso

Dato che è possibile esprimere il numero complesso graficamente, è possibile anche esprimerli in forma polare.

Supposto di avere un numero complesso $z$, $r = ||z||$, $\theta$ è invece l’angolo tra il punto e l’asse $x$.

Per convertire il numero $z$ in forma polare dobbiamo quindi trovare prima la distanza tra il punto e l’origine e poi l’angolo.

![1549823460309](assets/1549823460309.png)

$r = ||z|| = \sqrt{a^2+b^2}$

Dalla trigonometria abbiamo una regola per calcolare seno, coseno e tangente:

* $\displaystyle sin(x) = \frac{Opposto}{Ipotenusa}$
* $\displaystyle cos(x) = \frac{Adiacente}{Ipotenusa}$
* $\displaystyle tan(x) = \frac{Opposto}{Adiacente}$

Il lato opposto all’angolo $\theta$ è $b$, il lato adiacente a $\theta$ è $a$, l’ipotenusa è $r$.

Se abbiamo $a, b$ possiamo facilmente calcolare $\theta$:

$\displaystyle tan(\theta) = \frac{b}{a} \iff arctan(tan(\theta)) = arctan \left( \frac{b}{a} \right) \iff \theta = arctan \left( \frac{b}{a} \right)$

Se mancano $a$ o $b$ possiamo usare le seguenti equazioni:

* $\displaystyle cos(\theta) = \frac{a}{r} \iff a = r cos(\theta)​$
* $\displaystyle sin(\theta) = \frac{b}{r} \iff b = r \sin(\theta)$

$z = a + bi \iff z = r cos(\theta) + (r sin(\theta))i \iff z = r(cos(\theta) + sin(\theta) i)$

L’ultimo passaggio ci dà quindi il numero complesso in forma polare.

$z = r(cos(\theta) + sin(\theta)i)$

### Forma esponenziale del numero complesso

Dalla forma polare possiamo anche trovare un’altra forma per i numeri complessi, infatti: $(cos(\theta) + sin(\theta)i) = e^{i\theta} \implies z = re^{i\theta}$.

Questa forma può essere utile quando bisogna fare dei calcoli con gli esponenti.

## Operazioni tra numeri complessi in forma polare ed esponenziale

Supponiamo di avere due numeri complessi $z_1 = r_1(cos(\theta_1) + sin(\theta_1)i)$ e $z_2 = r_2(cos(\theta_2) + sin(\theta_2)i)$. 

### Moltiplicazione

$z_1 \times z_2 = r_1 \times r_2 \times [cos(\theta_1 + \theta_2) + sin(\theta_1 + \theta_2)i]$

Dimostrazione:
$$
\begin{align}
& z_1 z_2 = [r_1(cos(\theta_1) + sin(\theta_1)i)][r_2(cos(\theta_2) + sin(\theta_2)i] = \\
& r_1r_2(cos(\theta_1) + sin(\theta_1)i)(cos(\theta_2) + sin(\theta_2)i ) = \\
& r_1r_2(cos(\theta_1)cos(\theta_2) + sin(\theta_2)cos(\theta_1)i + sin(\theta_1)cos(\theta_2) i + sin(\theta_1)sin(\theta_2) i^2) = \\
& r_1r_2(cos(\theta_1)cos(\theta_2) + sin(\theta_2)cos(\theta_1)i + sin(\theta_1)cos(\theta_2) i + (-1) sin(\theta_1)sin(\theta_2)) = \\
& r_1r_2(cos(\theta_1)cos(\theta_2) + sin(\theta_2)cos(\theta_1)i + sin(\theta_1)cos(\theta_2) i - sin(\theta_1)sin(\theta_2)) = \\
& r_1r_2[(cos(\theta_1)cos(\theta_2) - sin(\theta_1)sin(\theta_2)) + (sin(\theta_2) cos(\theta_1) + sin(\theta_1) cos(\theta_2))i] = \\
& r_1r_2[cos(\theta_1 + \theta_2) + sin(\theta_1 + \theta_2)i]
\end{align}
$$

Forma esponenziale:

$z_1 z_2 = r_1 r_2 e^{(\theta_1 + \theta_2)i}$

### Divisione

$\displaystyle \frac{z_1}{z_2} = \frac{r_1(cos(\theta_1) + sin(\theta_1)i)}{r_2(cos(\theta_2) + sin(\theta_1)i)} = \frac{r_1 e^{\theta_1i}}{r_2e^{\theta_2i}} = \frac{r_1}{r_2} e^{(\theta_1 - \theta_2)i}$

Se non vogliamo usare la forma esponenziale possiamo invece lavorare con la forma polare ed il risultato è simile alla moltiplicazione, infatti, applicate le trasformazioni diventa:

$\displaystyle \frac{z_1}{z_2} = \frac{r_1}{r_2}[cos(\theta_1 - \theta_2) + sin(\theta_1 - \theta_2)i]$

### Elevazione a potenza

Partiamo dalla forma esponenziale:
$ z = re^{\theta i} \iff z^2 = (re^{\theta i})^2 \iff z^2 = r^2 e^{2\theta i} $
Invece nella forma polare è possibile usare il teorema o formula di De Moivre:

$z \times z = r \times r [cos(\theta + \theta) + sin(\theta + \theta) i] \iff z^2 = r^2[cos(2\theta) + \sin(2\theta)i]$

In generale quindi:

$z^n = r^n [cos(n\theta) + sin(n\theta)i]$

### Radice

Per trovare le radici di un numero complesso serve la forma polare, esistono sempre $n$ radici distinte date dalla seguente equazione:

$\sqrt[n]{z} = \sqrt[n]{r}\left[ cos \left( \frac{\theta + 2k\pi}{n} \right) + sin \left( \frac{\theta + 2k \pi}{n} \right) i \right]$

Quindi l’equazione va risolta per $k = 0, 1, 2, ..., n-1$.

Graficamente verrà frazionato un cerchio di raggio $\sqrt[n]{r}$ in $n$ parti.

# Fondamenti di calcolo infinitesimale (Fundamentals of calculus)

## Funzioni

Le funzioni sono equazioni particolari che assumono un solo valore $y​$ per ogni valore $x​$.

Per esempio $y = x+3​$ è una funzione perché nessun valore $x​$ dà almeno 2 valori $y​$.

Questo significa anche che $x^2+y^2 = 1$ non è una funzione, come controesempio prendiamo: $x = 0$.
$$
x = 0 \implies 0 + y^2 = 1 \iff
\begin{cases}
y = 1 \\
y = -1
\end{cases}
$$

### Dominio e immagine

L’insieme delle $x$ rappresenta il dominio della funzione, mentre l’insieme delle $y$ o $f(x)$ è l’immagine della funzione.

Questo vuol dire anche che potremmo considerare il Dominio come “input” della funzione e l’Immagine come “output”.

Esempi di non-funzioni:

|  1    | 2     |
| ---- | ---- |
|   ![nf1](assets/nf1.jpg)   |   ![nf2](assets/nf2.jpg)   |

La retta gialla interseca almeno due volte il grafico della equazione e pertanto non sono funzioni.

Alcuni esempi di funzioni con dominio ristretto:
$$
\begin{align}
& f(x) = \frac{1}{x} & D(f(x)) = (-\infin, 0) \cup (0, + \infin) = \{ x \in \R | x\ne 0 \} \\
& g(x) = \sqrt{x} & D(g(x)) = [0, +\infin) = \{ x \in \R | x \ge 0 \} \\
& h(x) = ln(x) & D(h(x)) = (0, +\infin) = \{ x \in \R | x > 0 \}
\end{align}
$$
Nota: Il dominio, essendo un insieme, può esser rappresentato in vari modi.

### Crescenza e decrescenza di una funzione

Una funzione si dice crescente in senso stretto quando: $f(n + 1) > f(n) \forall n \in D[f(x)]$.

O più semplicemente quando il valore $y$ cresce.

Una funzione decrescente ha semplicemente il verso della disequazione inverso.

Esistono anche casi particolari, cioè nel caso di $\ge$ e $\le$, in questo caso si parla di crescenza o decrescenza in senso lato.

### Funzioni pari e dispari (simmetrie)

Una funzione pari rispetta la seguente eguaglianza: $f(x)=f(-x)​$; ne consegue che una funzione pari ha un asse di simmetria corrispondente a $y=0​$.

Una funzione dispari invece rispetta la seguente eguaglianza: $-f(x)=f(-x)$ ed ha quindi una simmetria rispetto l’origine.

Esempi:
$$
\begin{align}
& f(x) = x^5 + x \\
& f(-x) = (-x)^5 + (-x) = -(x^5 + x) = -f(x) & f(x) \text{ dispari} \\
& g(x) = 1 - x^4 \\
& g(-x) = 1 - (-x)^4 = 1 - x^4 = g(x) & g(x) \text{ pari} \\
& h(x) = 2x - x^2 \\
& h(-x) = 2(-x) - (-x)^2 = -2x - x^2 = -(2x + x^2) & h(x) \text{ nessuna}
\end{align}
$$

L’unica funzione che è sia pari che dispari è la funzione identicamente nulla:
$$
f(x) = f(-x) = -f(x) \iff f(x) = 0 \quad \forall x \in D
$$
Le funzioni pari e dispari hanno alcune proprietà:

Per le funzioni pari (sia $f$ che $g​$):
$$
\begin{align}
& f(x) \pm g(x) = f\text{ pari} \\
& f(x) \times g(x) = f\text{ pari} \\
& \frac{f(x)}{g(x)} = f\text{ pari} \quad g(x) \ne 0
\end{align}
$$
Per le funzioni dispari (sia $f$ che $g$):
$$
\begin{align}
& f(x) \pm g(x) = f\text{ dispari} \\
& f(x) \times g(x) = f\text{ pari} \\
& \frac{f(x)}{g(x)} = f\text{ pari} \quad g(x) \ne 0
\end{align}
$$
Con $f$ pari e $g$ dispari:
$$
\begin{align}
& f(x) \pm g(x) = f\text{ nessuna delle due} \\
& f(x) \times g(x) = f\text{ dispari} \\
& \frac{f(x)}{g(x)} = f\text{ dispari} \quad g(x) \ne 0
\end{align}
$$

### Iniettiva e suriettiva

Sia $f: A \to B$ una funzione.

* La funzione si dice iniettiva se: $\forall a_1 \in A, \forall a_2 \in A \quad (a_1 \ne a_2 \implies f(a_1) \ne f(a_2))​$ ovvero gli elementi di $A​$ sono in relazione con elementi distinti di $B​$
* La funzione si dice suriettiva se: $\forall b \in B \quad \exists a \in A \quad f(a) = b$ ovvero tutti gli elementi di $B$ sono in relazione con almeno un elemento di $A$.

Nel caso le due condizioni sussistano contemporaneamente la funzione si dice “bigettiva”.

### Classificazioni

In base alla natura della funzione abbiamo delle principali classificazioni delle stesse:

* Polinomiale
	* Quadratica
	* Cubica
* Potenza
	* Radice
	* Reciproca
* Razionale
* Algebrica
* Trascendentale
	* Trigonometrica
	* Trigonometrica inversa
	* Esponenziale
	* Logaritmica

Alcuni esempi:

| Funzione $f(x) $                       | Classificata           |
| -------------------------------------- | ---------------------- |
| $\sqrt[5]x = x^{\frac{1}{5}}$          | Radice                 |
| $\displaystyle x^{-1} = \frac{1}{x}$   | Reciproca              |
| $\sqrt{1-x^2} = (1-x^2)^{\frac{1}{2}}$ | Algebrica              |
| $x^9 + x^4$                            | Polinomiale di grado 9 |
| $\displaystyle\frac{x^2+1}{x^3+x}$     | Razionale              |
| $tg(2x)$                               | Trigonometrica         |
| $ln(x)$                                | Logaritmica            |

### Funzioni definite a tratti

È possibile definire una funzione a tratti nel seguente modo:
$$
f(x) = 
\begin{cases}
x 	& 0 \le x \le 1 \\
x^2 & x > 1
\end{cases}
$$
Analizzando la funzione scopriamo che è continua, perché la retta $y = x​$ e la parabola $x^2​$ sono funzioni continue ed il punto $(1,1)​$ non è un punto di discontinuità perché facendo i limiti delle due funzioni verso $1​$ scopriamo che convergono allo stesso valore. Una volta affrontati i limiti e la continuità questo diventa semplice da risolvere.

Il grafico della funzione quindi è qualcosa di simile:

![1547307469468](assets/1547307469468.png)

### Alcune funzioni

#### Retta o lineare

Per definire una retta in $\R^2$ sono necessari (almeno) due punti, abbiamo però molti modi per esprimere l’equazione di una retta, per esempio $f(x) - f(x_0) = m(x-x_0)$ oppure $f(x) = mx+q$.

#### Parabola o quadratica

Le parabole sono definite nel seguente modo: $f(x) = a \cdot x^2 + b \cdot x + c​$. Questo è un qualunque polimonio di grado due.

Ne consegue che il vertice della parabola, ovvero il punto in cui raggiunge il minimo valore assoluto (se è una parabola positiva, ovvero $a > 0$) o massimo valore assoluto (se è una parabola negativa, ovvero $a < 0 $) nel proprio dominio, è $\displaystyle\left(-\frac{b}{2a}, f\left(-\frac{b}{2a}\right)\right)$, ma è possibile esprimere la parabola in un’altra maniera semplificando la ricerca del vertice: $f(x) = a(x-h)^2 + k$ ed il vertice diventa: $(h,k)​$.

Esempi:
$$
f(x) = -x^2 + 10x -1 \\
V = \left( -\frac{10}{2(-1)} = 5, f\left( 5 \right) \right) \\
f(5) = -5^2 + 10 \cdot 5 -1 = 50 - 25 -1 = 24 \\
V = (5, 24) \\
$$
Controlliamo il risultato tramite un grafico:

![1547309490101](assets/1547309490101.png)



Per finire l’analisi di questa funzione dobbiamo trovare gli zero, ovvero i due punti in cui la parabola si interseca con l’asse delle x.

Proviamo ora l’altra forma della parabola:
$$
\begin{align}
& -x^2 + 10x -1 = a(x-h)^2 + k \iff 
 -x^2 + 10x -1 = a(x^2 - 2xh + h^2) + k \iff \\
& -x^2 + 10x -1 = ax^2 - 2axh +ah^2 + k \iff 
 -x^2 +10x -1 = -x^2 + 2xh - h^2 + k \iff \\
& -x^2 +10x -1 = -x^2 + 2xh - (h^2 - k) \\ 
& 10 = 2h \iff h = 5 \\
& -1 = -(5^2 - k) \iff 1 = 25 - k \iff k = 24
\end{align}
$$

#### Trigonometriche (~todo)

Esempio semplice, le funzioni trigonometriche sono già state affrontate nel capitolo dedicato.

### Combinare funzioni e conseguenze sul dominio

#### Operazioni fondamentali

È possibile combinare le funzioni in vari modi.
$$
(f+g)(x) \quad (f-g)(x) \quad (fg)(x) \quad \left( \frac{f}{g} \right)(x)
$$
In tutti i casi il dominio risulterà essere $D(f(x)) \cap D(g(x))$ e nel caso della divisione si aggiunge la condizione: $g(x) \ne 0$ perché $\displaystyle D\left(\frac{f}{g}\right) = (D(f) \cap D(g)) - \{ x \in D(g) : g(x) = 0 \}$ a causa dell’impossibilità della divisione per 0.

### Funzioni composte

Comporre una funzione vuol dire inserire l’immagine di una funzione nel dominio di un’altra.

Questa operazione ha quindi una conseguenza immediata nell’immagine finale della funzione composta.

Ad esempio:
$$
\begin{align}
& f(x) = \frac{x}{x+1} \quad g(x) = x^{10} \quad h(x) = x+3 \\
& D(f(x)) = \R \backslash \{ -1 \} \quad D(g(x)) = \R = D(h(x)) \\
& I(f(x)) = \R \backslash \{ 1 \} \quad I(g(x)) = I(h(x)) = \R \\
& g(h(x)) = (h(x))^{10} = (x+3)^{10} \\
& p(x) = f(g(h(x))) = \frac{g(h(x))}{g(h(x)) + 1} =
\frac{(x+3)^{10}}{(x+3)^{10} + 1} \\
\end{align}
$$
Analizziamo ora gli effetti sul dominio:
$$
\begin{align}
& f(x) = x + \frac{1}{x} \quad g(x) = \frac{x+1}{x+2} \\
& f(g(x)) = \frac{x+1}{x+2} + \frac{1}{\frac{x+1}{x+2}} = \frac{x+1}{x+2} + \frac{x+2}{x+1} \\
& D(f(g(x)) = \{ x \in \R | x \ne -2 \and x \ne -1 \} \\
& g(f(x)) = \frac{x + \frac{1}{x} + 1}{x + \frac{1}{x} + 2} =
\frac{\frac{x^2 + 1 + x}{x}}{\frac{x^2 + 1 + 2x}{x}} = 
\frac{x^2+x+1}{x} \cdot \frac{x}{x^2+2x+1} = 
\frac{x^2+x+1}{x^2+2x+1} \\
& D(g(f(x))) = \{ x \in \R | x \ne -1 \}
\end{align}
$$

### Funzioni inverse

Per trovare la funzione inversa è possibile risolvere l’equazione rispetto a $y$ o costruire il grafico rispetto la retta $y=x$. Questo porta ad una conseguenza evidente infatti prendiamo due esempi:

| 1                                                            | 2                                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![](assets/2017-09-10_17-18-38-fbe7c903e842892804863c5c2dc7c9be.png) | ![](assets/2017-09-10_17-18-44-0a1d6faf16d99628457d9775fbb406cb.png) |

Nel caso 2 non abbiamo una funzione, in più la retta gialla interseca due volte il grafico e di conseguenza non esiste sicuramente l’inversa.

Ma nel caso 1 sarebbe possibile trovare l’inversa.

Troviamo l’inversa tramite la risoluzione di un’equazione.
$$
f(x) = \sqrt{-1-x}
$$
Analizziamo prima Dominio e Immagine: 

* $D(f(x)) = \{x \in \R | -1-x \ge 0 \iff -x \ge -1 \iff x \le 1\}$.
* $I(f(x)) = \{ y \in \R | y \ge 0 \}$

È possibile disegnare direttamente la funzione inversa usando la retta $f(x) = x$:

![](assets/inversef.png)

Invece dal procedimento algebrico:
$$
y = \sqrt{-1-x} \iff y^2 = -1 -x \iff y^2 +1 = -x \iff x = -(y^2+1) \implies \\ 
\implies f^{-1}(x) = -x^2 - 1
$$

#### Logaritmi

Il logaritmo è l’inversa della funzione esponenziale, infatti è definito come: $log_a(x) = y \implies a^y = x$.

Il logaritmo ha delle proprietà:

* Somma e differenza: 
	* $log_a x + log_a y = log(x \times y)$
	* $log_ax - log_ay = log(\frac{x}{y})$
* Esponente:
	* $log_a(x^y) = y \times log_a(x)$

Il logaritmo ha come condizioni per esistere, riscrivendo la definizione come $a^{log_a b} = b$:

1. La base $a$ deve essere diversa da 1
2. Sia $a$ che $b$ devono essere maggiori di 0

È possibile operare sulla base, infatti: $\displaystyle log_a(b) = \frac{log_c(b)}{log_c(a)}$

Questo vuole anche dire che $\displaystyle log_a(b) = \frac{1}{log_b(a)}$

## Equazioni 2o grado

Un’equazione di secondo grado è espressa come: $ax^2 + bx + c = 0$. Risolverla implica trovare la $x$.

Al fine di risolvere un’equazione di secondo grado esistono due principali metodi: uno è il metodo del completamento del quadrato e l’altro è con la formula quadratica o del delta.

### Formula quadratica (o delta)

$\displaystyle f(x) = ax^2 + bx + c \iff x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}$

Nell’insieme dei numeri reali deve avvenire che $b^2 - 4ac \ge 0​$.

La formula quadratica dà quindi sempre due soluzioni, che possono essere o identiche o distinte.

Esempi:
$\displaystyle f(x) = x^2 -x -1 \\ \displaystyle x= \frac{1 \pm \sqrt{ 1 - 4(1)(-1) } }{2} \iff x = \frac{1 \pm \sqrt{5}}{2} \\ \displaystyle f(x) = \left( x - \frac{1 + \sqrt 5}{2} \right) \left( x - \frac{1 - \sqrt 5}{2} \right)​$
$$
3x^2 + 2x -1 = 0 \iff x = \frac{-2 \pm \sqrt{2^2 - 4(3)(-1)}}{2 \times 3} =
\frac{-2 \pm \sqrt{4 \times 4}}{6} = \frac{-2 \pm 4}{6} \\
x_0 = \frac{2}{6} = \frac{1}{3} \\
x_1 = \frac{-6}{6} = -1
$$


### Completamento del quadrato

L’idea del completamento del quadrato sta nella costruzione del binomio $(a+b)^2 = a^2 + 2ab + b^2$.

Per risolvere l’equazione quindi possiamo usare il seguente procedimento:
$$
ax^2 + bx + c = 0 \iff ax^2 + bx = -c \iff 
x^2 + \frac{b}{a}x = -\frac{c}{a} \iff \\
x^2 + \frac{b}{a}x + \left( \frac{b}{2a} \right)^2 = \left( \frac{b}{2a} \right)^2 - \frac{c}{a} \iff
\left(x + \frac{b}{2a} \right)^2 = \frac{b^2 - 4ac}{4a}
$$



### Divisioni polinomiali

Quando si hanno due polinomi da dividere ma non si riescono a trovare dei prodotti notevoli o comunque a semplificare in altre maniere è possibile usare la divisione polinomiale:
$
\displaystyle f(x) = \frac{x^4 + 2 x^2 + x - 1}{x^2 + x + 1} \\
\begin{array}{c, c, c, c, c | c, c}
x^4 & 0 & 2x^2 & x & -1 & x^2 + x + 1 \\
\hline 
-x^4 & -x^3 & -x^2 & 0 & 0 & x^2 \\
0 & -x^3 & x^2 & x & -1 \\
\hline
0 & x^3 & x^2 & x & 0 & x^2 - x \\
0 & 0 & 2x^2 & 2x & -1 \\
\hline
0 & 0 & -2x^2 & -2x & -2 & x^2 - x + 2 \\
0 & 0 & 0 & 0 & -3
\end{array} \\
Q(x) = x^2 -x +2 \\
R(x) = -3 \\
P(x) = Q(x) D(x) + R(x) \\
x^4 + 2x^2 + x - 1 = (x^2 -x +2) (x^2 +x +1) + (-3)
​$

## Prodotti notevoli

I prodotti notevoli possono sempre tornare utili, per esempio fattorizzare un limite consente di risolverlo facilmente usando la sostituzione.

Questi sono alcuni che possono tornare utili:
$$
\begin{align}
& (a+b)^2 = a^2 + 2ab + b^2 \\
& (a-b)^2 = a^2 - 2ab + b^2 \\
& (a+b)(a-b) = a^2 - b^2 \\
& (a+b)^3 = a^3 + 3a^2b + 3ab^2 + b^3 \\
& (a-b)^3 = a^3 - 3a^2b + 3ab^2 - b^3 \\
& (a+b)(a^2 + ab + b^2) = a^3 + b^3 \\
& (a-b)(a^2 + ab + b^2) = a^3 - b^3
\end{align}
$$

# Calcolo infinitesimale 1 (Calculus 1)

## Limiti e continuità degli insiemi

### Definizione del limite

Supponiamo di avere una funzione $f(x)$ definita in un intervallo aperto, quindi non è detto che la funzione esista nel punto $a$:

$\displaystyle \lim_{x \to a} f(x) = L$

Questo significa che $\forall \epsilon > 0 \exists \delta > 0 : |f(x) - L| < \epsilon, 0 < |x-a| < \delta$. 

Graficamente:

![2017-08-28_17-52-01-4b3f024aa3deed127f1371053b403321](assets/2017-08-28_17-52-01-4b3f024aa3deed127f1371053b403321.png)

### Limiti di funzioni combinate e composte

#### Funzioni combinate

Il limite somma di due funzioni è uguale alla somma tra i limiti delle due funzioni:
$$
\lim_{x \to a} f(x) = L \quad \lim_{x \to a}g(x) = M \\
\lim_{x \to a} [f(x)+g(x)] = L + M
$$
Il limite prodotto di due funzioni è uguale al prodotto tra i limiti delle due funzioni:
$$
\lim_{x \to a} f(x) = L \quad \lim_{x \to a}g(x) = M \\
\lim_{x \to a} [f(x) \times g(x)] = L \times M
$$
Il caso della divisione del limite tra due funzioni è analogo al prodotto e vale se e solo se $M \ne 0$.

#### Funzioni composte

$$
\lim_{x \to a} f(g(x)) = f(\lim_{x \to a} g(x))
$$

### Teorema del confronto

Supponiamo di avere un limite particolarmente difficile, è possibile trovarlo usando questo teorema:
$$
f(x) := \text{funzione complicata} \\
\lim_{x \to c} g(x) = \lim_{x \to c} h(x) = L \\
g(x) \le f(x) \le h(x) \implies \lim_{x \to c} f(x) = L \\
$$
Per esempio:
$$
\lim_{x \to 0} x^2 sin\left( \frac{1}{x} \right) \\
-1 \le sin(x) \le 1 \iff 
-1 \le sin\left( \frac{1}{x} \right) \le 1 \iff
-x^2 \le x^2 sin\left( \frac{1}{x} \right) \le x^2 \\
\lim_{x \to 0} x^2 = 0 \implies \lim_{x \to 0} x^2 sin\left( \frac{1}{x} \right) = 0
$$


Questo teorema vale anche per le disequazioni.

### Tabella dei limiti notevoli (da convertire)

| Limite                                              | Risultato                       |
| --------------------------------------------------- | ------------------------------- |
| $\displaystyle \lim_{x \to 0} \frac{ln(1+x)}{x}$    | $1$                             |
| $\displaystyle \lim_{x \to 0} \frac{log_a(1+x)}{x}$ | $\displaystyle \frac{1}{ln(a)}$ |
|                                                     |                                 |


$$
\begin{align}
& \lim_{x \to 0} \frac{log_a(1+x)}{x} = \frac{1}{ln(a)} \\
& \lim_{x \to 0} \frac{e^x - 1}{x} = 1 \\
& \lim_{x \to 0} \frac{a^x - 1}{x} = \ln(a) \\
& \lim_{x \to \pm \infin} \left( 1 + \frac{1}{x} \right)^x = e \\
& \lim_{x \to 0} \frac{(1+x)^c - 1}{x} = c \\
& \lim_{x \to 0} sin(x) = 0 \\
& \lim_{x \to 0} cos(x) = 1 \\
& \lim_{x \to 0} \frac{sin(x)}{x} = 1 \\
& \lim_{x \to 0} \frac{1-cos(x)}{x^2} = \frac{1}{2} \\
& \lim_{x \to 0} \frac{tg(x)}{x} = 1 \\
& \lim_{x \to 0} \frac{arcsin(x)}{x} = 1 \\
& \lim_{x \to 0} \frac{arctg(x)}{x} = 1 \\
& \lim_{x \to 0} \frac{sinh(x)}{x} = 1 \\
& \lim_{x \to 0} \frac{cosh(x) - 1}{x^2} = \frac{1}{2} \\
& \lim_{x \to 0} \frac{tgh(x)}{x} = 1 \\
& \lim_{x \to 0^+} xln(x) = 0 \\
& \lim_{x \to 0} (1+x)^{\frac{1}{x}} = e \\
& \lim_{x \to +\infin} \frac{a^n}{n!} = 0 \\
& \lim_{x \to +\infin} \frac{1}{1+a^x} = \begin{cases}
1 & a < 1 \\
\frac{1}{2} & a = 1 \\
0 & a > 1
\end{cases}
\end{align}
$$



### Continuità

Partiamo da degli esempi di discontinuità.

#### Discontinuità in un punto

La funzione $\displaystyle f(x) = \frac{x^2+11x+28}{x+4}$ ha un punto di discontinuità per $x = -4$ perché non può esistere in quel punto.

#### Discontinuità a salti

Prendiamo una funzione definita a tratti:
$$
f(x) = \begin{cases}
0.2 & 0 < x < 1 \\
0.4 & 1 \le x < 2 \\
0.6 & 2 \le x < 3
\end{cases}
$$
In questo caso tutti i salti ($x = 0.2, 0.4, 0.6$) sono punti di discontinuità e nel grafico la funzione effettua dei “salti”.

#### Discontinuità essenziale o infinita

In questo caso abbiamo a che fare con asintoti verticali, questa situazione si verifica con la funzione iperbole ad esempio.
$$
f(x) = 2 + \frac{1}{x}
$$
Avrà un punto di discontinuità a $x = 2​$.

#### Altri casi

In generale trovare la discontinuità di una funzione si riduce al trovare il dominio, infatti se una funzione è definita in ogni punto del suo dominio è continua, ad esempio:
$$
f(x) = \sqrt[3]{ \frac{x+1}{x-1}} \\
x - 1 = 0 \iff x = 1 \\
D(f(x)) = (- \infin, 1) \cup (1, +\infin)
$$

#### Discontinuità rimovibile

Alcune discontinuità possono essere rimosse, prendiamo ad esempio:
$
\displaystyle f(x) = \frac{x-2}{x^2-4} \\
x^2 - 4 = 0 \iff x^2 = 4 \iff x = \pm 2 \\
\displaystyle f(x) = \frac{x-2}{x^2-4} = \frac{x-2}{(x+2)(x-2)} = \frac{1}{x+2} \\
D(f(x)) = (- \infin, -2) \cup (-2, + \infin)
$

#### Definizione di continuità in un punto

Ora è possibile dare la definizione di continuità in un punto grazie al limite:

* $f$ è continua in $c$ se $\displaystyle \lim_{x \to c} f(x) = f(c)$
* Il discorso è analogo per il limite destro e sinistro.

Esistono 3 condizioni affinché una funzione sia continua in un punto $c$:

* $f(c)$ esiste
* $\displaystyle \lim_{x \to c} f(x)$ esiste
* $\displaystyle \lim_{x \to c} f(x) = f(c)$

Espandendo $c$ a tutto il dominio si ha una funzione “continua”.

Le funzioni composte e composite sono ovviamente continue riferendosi alle proprietà dei limiti già elencate, per esempio la somma di due funzioni continue è continua, il problema sorge con la divisione eventualmente.

#### Rendere funzioni continue

È possibile intervenire trasformando delle funzioni in funzioni a tratti al fine di renderle continue, supponendo che esista un punto di discontinuità si calcola il limite sinistro e destro e se sono identici poniamo che la funzione assuma il valore del limite nel punto di discontinuità, per esempio:
$
\displaystyle f(x) = \frac{x^2 + 11x +28}{x+4} \\
\lim_{x \to -4} f(x) = 3 \\
f(x) = \begin{cases}
\displaystyle \frac{x^2 + 11x +28}{x+4} & x \ne -4 \\
3 & x = 4
\end{cases}
$


### Asintoti

Un asintoto è una retta alla quale si avvicina indefinitamente una funzione, questo vuol dire che esistono 3 tipi di asintoti: orizzontale, verticale ed obliquo.

#### Asintoto verticale

Gli asintoti verticali si trovano nelle funzioni razionali quando x tende al punto di discontinuità, quindi:

$\displaystyle \lim_{x \to c} f(x) = \pm \infin$

#### Asintoto orizzontale

Ci interessano ancora le funzioni razionali, in questo caso prendiamo l’elemento di grado massimo col suo coefficiente del numeratore e del denominatore, si prospettano 3 situazioni:

1. $\displaystyle N = D \implies y = \frac{c_1}{c_2}$
2. $N < D \implies y = 0$
3. $N > D \implies N.E.​$ 

#### Asintoto obliquo

Nel caso che il grado del numeratore sia maggiore del grado del denominatore di 1 abbiamo un asintoto obliquo, per trovarlo si fa ricorso alla divisione polinomiale.
$\displaystyle
f(x) = \frac{x^2 + x - 1}{x - 1} \\
\begin{array}{c, c, c | c, c}
x^2 & x & -1 & x & -1 \\
\hline 
x^2 & -x & / & x & / \\
0 & 2x & -1 & / & / \\
\hline 
0 & 2x & -2 & x & 2 \\
0 & 0 & 1
\end{array} \\
\displaystyle f(x) = x + 2 + \frac{1}{x-1} \\
​$
A questo punto l’asintoto obliquo è $x + 2​$, il quoziente, mentre il resto $x = 1​$ è l’asintoto verticale.

## Teorema degli zeri

Questo teorema prova l’esistenza di una radice (o zero) in un intervallo chiuso continuo.

Ipotesi:

* $f(x)$ è continua nell’intervallo $[a,b]$
* $y_0 = f(x_0) \in  [ f(a), f(b) ]$

Tesi:

* $f(a) < f(x_0) < f(b) \or f(b) < f(x_0) < f(a) \implies \exists c \in (a,b) : y_0 = f(c) = 0$



## Derivate

### Definizione

La Derivata non è altro che la tangente in un punto particolare della funzione.

Questo vuol dire che si può definire la derivata tramite il rapporto incrementale:
$$
f'(x) = \lim_{a \to b} \frac{f(b) - f(a)}{b-a} = \lim_{h \to 0} \frac{f(x+h) - f(x)}{h}
$$

Una funzione per essere derivabile deve essere continua, è possibile derivare una funzione anche più di una volta.

### Teorema di Rolle 

Il teorema di Rolle rimane molto simile al teorema degli zeri, in particolare:

* Si riferisce solo a funzioni in un intervallo chiuso
* La funzione dev’essere derivabile (o differenziabile che sono la stessa cosa) nell’intervallo aperto
* I due punti dell’intervallo devono avere lo stesso valore $f(x)$

Il teorema prova che in un certo punto incluso nell’intervallo la derivata è zero e quindi è una retta tangente parallela all’asse x.

### Regola di L’Hopital

La regola di Hopital è utile quando in un limite si incontra una forma d’indeterminazione come $\pm \infin / \pm \infin$ oppure $0 / 0$.

La regola di Hopital dice: 
$$
\begin{align}
\lim_{x \to c} \frac{f(x)}{g(x)} = L \implies
\lim_{x \to c} \frac{f'(x)}{g'(x)} = L 
\end{align}
$$
Ovviamente è possibile riapplicarla, basta che si presenti di nuovo una delle due forme d’indeterminazione.

### Tabella delle regole di derivazione

| Nome                             | Funzione                          | Derivata                                                     |
| -------------------------------- | --------------------------------- | ------------------------------------------------------------ |
| Regola della somma / Linearità   | $f(x) + g(x)$                     | $f'(x) + g'(x)$                                              |
| Regola del prodotto / di Leibniz | $f(x) g(x)$                       | $f(x) g'(x) + f'(x) g(x)$                                    |
| Regola del quoziente             | $\displaystyle \frac{f(x)}{g(x)}$ | $\displaystyle \frac{f'(x) g(x) - f(x)g'(x)}{[g(x)]^2}$      |
| Regola della funzione reciproca  | $\displaystyle \frac{a}{f(x)}$    | $\displaystyle -a \frac{f'(x)}{[f(x)]^2}$                    |
| Regola della funzione inversa    | $f^{-1}(y)$                       | $\displaystyle \frac{1}{f'(x)}$                              |
| Regola della catena              | $g(f(x))$                         | $g'(f(x)) \times f'(x)$                                      |
| Regola della potenza             | $[f(x)]^{g(x)}$                   | $\displaystyle f(x)^{g(x)} \left[ g'(x) ln(f(x)) + \frac{g(x)f'(x)}{f(x)} \right]$ |

La regola della catena la si applica anche a tutte le altre regole e diventa più semplice usando una sostituzione: $f(x) = u​$.

### Tabelle delle derivate

È già applicata anche la regola della catena in modo da generalizzare la situazione anche quando c’è una funzione interna e non solo $x​$, i calcoli vengono corretti considerando che: $\frac{d}{dx} x = 1 ​$.

#### Polinomiali

| Condizioni   | Funzione $f(x) $ | Derivata $f'(x) $                                            |
| ------------ | ---------------- | ------------------------------------------------------------ |
| $a \in \R$   | $a$              | $0$                                                          |
|              | $x$              | 1                                                            |
| $a \in \R$   | $ax$             | $a$                                                          |
| $a \in \R$   | $x^a $           | $a x^{a-1}$                                                  |
| $x \ne 0$    | $|x|$            | $\displaystyle \frac{|x|}{x} = \frac{x}{|x|}$                |
| $f(x) \ne 0$ | $|f(x)|$         | $\displaystyle f'(x) \frac{f(x)}{|f(x)|} = f'(x) \frac{|f(x)|}{f(x)}$ |

#### Logaritmiche ed esponenziali

| Funzione $f(x)$          | Derivata $f'(x)$                                       |
| ------------------------ | ------------------------------------------------------ |
| $log_b(x)$               | $\displaystyle \frac{log_b(e)}{x} = \frac{1}{x ln(b)}$ |
| $ln(x) $                 | $\displaystyle \frac{1}{x}$                            |
| $e^x$                    | $e^x$                                                  |
| $a^x$                    | $a^x ln(a)$                                            |
| $x^x$                    | $x^x(1+ln(x))$                                         |
| $ln(f(x))  = ln(|f(x)|)$ | $\displaystyle \frac{f'(x)}{f(x)}$                     |
| $e^{f(x)}$               | $f'(x) e^{f(x)}$                                       |
| $a^{f(x)}$               | $ln(a) f'(x) a^{f(x)}$                                 |

#### Trigonometriche

##### Goniometriche

| Funzione $f(x) $ | Derivata $f'(x)$                             |
| ---------------- | -------------------------------------------- |
| $sin[g(x)]$      | $cos[g(x)] \times g'(x)$                     |
| $cos[g(x)]$      | $-sin[g(x)] \times g'(x)$                    |
| $tg[g(x)]$       | $sec^2[g(x)] \times g'(x)$                   |
| $cotg[g(x)]$     | $-csec^2[g(x)] \times g'(x)$                 |
| $sec[g(x)]$      | $sec[g(x)] \times tg[g(x)] \times g'(x)$     |
| $csec[g(x)]$     | $-csec[g(x)] \times cotg[g(x)] \times g'(x)$ |

##### D’arco

Chiamate anche arco-nome classico (es. $sin^{-1}(x) = arcsin(x)$ e si legge arcoseno).

| Funzione $f(x) $   | Derivata $f'(x)$                                            |
| ------------------ | ----------------------------------------------------------- |
| $sin^{-1}[g(x)]$   | $\displaystyle \frac{g'(x)}{\sqrt{1-[g(x)]^2}}$             |
| $cos^{-1}[g(x)]$   | $\displaystyle - \frac{g'(x)}{\sqrt{1-[g(x)]^2}}$           |
| $tg^{-1}[g(x)]$    | $\displaystyle \frac{g'(x)}{1+[g(x)]^2}$                    |
| $cotg^{-1}[g(x)]$  | $\displaystyle - \frac{g'(x)}{1+[g(x)]^2}$                  |
| $sec^{-1}[g(x)]$   | $\displaystyle \frac{g'(x)}{|g(x)| \sqrt{[g(x)]^2 - 1}}$    |
| $cosec^{-1}[g(x)]$ | $\displaystyle - \frac{g'(x)}{|g(x)| \sqrt{[g(x)]^2 - 1} }$ |

##### Iperboliche

| Funzione $f(x) $ | Derivata $f'(x)$                             |
| ---------------- | -------------------------------------------- |
| $sinh[g(x)]$      | $cosh[g(x)] \times g'(x)$                     |
| $cosh[g(x)]$      | $-sinh[g(x)] \times g'(x)$                    |
| $tgh[g(x)]$       | $sech^2[g(x)] \times g'(x)$                   |
| $cotgh[g(x)]$     | $-csech^2[g(x)] \times g'(x)$                 |
| $sech[g(x)]$      | $sech[g(x)] \times tgh[g(x)] \times g'(x)$     |
| $csech[g(x)]$     | $-csech[g(x)] \times cotgh[g(x)] \times g'(x)$ |

##### Iperboliche inverse

| Funzione $f(x) $   | Derivata $f'(x)$                                            |
| ------------------ | ----------------------------------------------------------- |
| $sinh^{-1}[g(x)]$   | $\displaystyle \frac{g'(x)}{\sqrt{1-[g(x)]^2}}$             |
| $cosh^{-1}[g(x)]$   | $\displaystyle - \frac{g'(x)}{\sqrt{1-[g(x)]^2}}$           |
| $tgh^{-1}[g(x)]$    | $\displaystyle \frac{g'(x)}{1+[g(x)]^2}$                    |
| $cotgh^{-1}[g(x)]$  | $\displaystyle - \frac{g'(x)}{1+[g(x)]^2}$                  |
| $sech^{-1}[g(x)]$   | $\displaystyle \frac{g'(x)}{|g(x)| \sqrt{[g(x)]^2 - 1}}$    |
| $cosech^{-1}[g(x)]$ | $\displaystyle - \frac{g'(x)}{|g(x)| \sqrt{[g(x)]^2 - 1} }$ |

### Rette tangenti

Una retta tangente è descritta da: $y = f(a) + f'(a)(x-a)​$, dove $a​$ è un punto particolare già dato.

### Rendere funzioni a tratti derivabili

Supposto di avere una funzione a tratti con una serie di valori sconosciuti, per renderla differenziabile (derivabile) dobbiamo imporre il limite al punto delle funzioni uguale e anche il limite al punto delle derivate uguale.

### Casi di inderivabilità

Può capitare nella funzione di avere delle tangenti orizzontali e verticali, i punti che intersecano queste tangenti e il grafico sono punti in cui è impossibile calcolare la derivata.

## Ottimizzazione

L’ottimizzazione è il processo che consente di disegnare il grafico di una funzione con una buona precisione, ci sono diversi passaggi:

1. [Trovare i punti critici (o stazionari)](#Punti critici)
2. [Determinare dove $f(x)$ è crescente e decrescente](#Crescenza e decrescenza)
3. [Trovare i punti di flesso](#Punti di flesso)
4. [Determinare dove $f(x)$ è concava o convessa](#Concavità o convessità)
5. [Trovare i punti in cui $f(x)$ si interseca con gli assi](#Intersezione con gli assi)
6. [Determinare i punti di massimi e minimi locali](#Massimi e minimi)
7. [Trovare gli asintoti e determinare il comportamento di $f(x)​$ verso $\pm \infin​$](#Asintoti e limiti)
8. [Disegnare i punti critici, di flesso e di intersezione con gli assi, disegnare gli asintoti ed infine il grafico](#Disegno finale)

### Punti critici

Un punto critico è un punto $x​$ dove la derivata prima è $0​$ o è indefinita, questi sono gli unici punti in cui la funzione può cambiare direzione e descrivono i punti di massimo e minimo.

Esempio:

* $f(x) = x + \frac{4}{x} = x + 4x^{-1}$

* $f’(x) = 1 - 4x^{-2}$

* $0 = 1- 4x^{-2} \iff 4x^{-2} = 1 \iff x^{2} = 4 \iff x = \pm 2 ​$

### Crescenza e decrescenza

Se una funzione è crescente la derivata di questa dev’essere positiva, viceversa se è decrescente.

Grazie ai punti critici possiamo calcolare la derivata nell’intorno di questi punti determinando quindi se la derivata è crescente o decrescente.

Esempio:

* $f(x) = x + 4x^{-1}$
* $f'(x) = 1 - 4x^{-2}$

$$
pc_1 = -2 \quad pc_2 = 2 \\
-\infin < x < -2 \\
-2 < x < 2 \\
2 < x < +\infin \\
f'(-3) = 1 - \frac{4}{3^2} = 1 - \frac{4}{9} = \frac{5}{9} > 0 \\
f'(-1) = 1 - \frac{4}{1} = -3 < 0 \\
f'(3) = 1 - \frac{4}{3^2} = \frac{5}{9} > 0 \\
$$

![1548259325833](assets/1548259325833.png)

Quindi $f(x)$ è crescente per $-\infin < x < -2$, decrescente per $-2 < x < 2$ e crescente per $2 < x < \infin$.

### Punti di flesso

Per trovare i punti di flesso ci serve la derivata seconda e va risolta l’equazione per $x = 0$ o quando è indefinito.

Esempio:

* $f’(x) = 1 - 4x^{-2}​$
* $f''(x) = 8 x^{-3}$

A questo punto si deduce che l’unico punto di flesso è $x=0$ in quanto è indefinito per $\frac{8}{0}$.

### Concavità o convessità

La concavità è definita dai punti di flesso e dal segno della derivata seconda, cioè la funzione è convessa (o concava in su) quando la derivata seconda è positiva ed è concava (o concava in giù) quando la derivata seconda è negativa.

Esempio:

Abbiamo solo il punto $x = 0$, quindi l’intervallo che ci interessa è $- \infin, 0, + \infin$.

Analizziamo la derivata seconda per $x = -1$ e $x = 1$.

$f''(-1) = 8(-1) = -8 < 0$

$f''(1) = 8(1) = 8 > 0$

![1548259979702](assets/1548259979702.png)

Quindi la funzione è concava per $- \infin < x < 0$ e convessa per $0 < x < + \infin$.

### Intersezione con gli assi

Ci sono due casi di intersezioni con gli assi, ovvero quando $f(x) = 0​$ oppure $f(0) = ?​$, si tratta semplicemente dei punti in cui la funzione interseca l’asse $y​$ nel primo caso e $x​$ nel secondo.

Esempio:

* $f(0) = 0 + 4/0$ la funzione non si interseca mai con l’asse delle $y$.
* $0 = x + 4x^{-1} \iff 0 = x^2 + 4 \iff x^2 = -4$ la funzione non si interseca mai con l’asse delle $x$.

### Massimi e minimi

I massimi ed i minimi (locali/relativi o globali/assoluti) possono esistere solo ai punti critici, ma per esser sicuri di quali siano globali vanno provati tutti.

Esempio:

Grazie al test effettuato per la crescenza e decrescenza della funzione sappiamo di sicuro che $-2$ è un punto di massimo locale e $2$ è un punto di minimo locale, infatti possiamo dire che:

* Se la derivata prima è negativa a sinistra del punto critico e positiva a destra del punto critico, questo punto è di minimo locale
* Se la derivata prima è positiva a sinistra del punto critico e negativa a destra del punto critico, questo punto è di massimo locale

Se invece la derivata ha segno costante il punto non è né di minimo né di massimo.

Il punto di massimo assoluto o globale, indicato con $M$ è il punto tale che $f(M) \ge f(x) \forall x \in D[f(x)]$, viceversa il punto di minimo assoluto o globale, indicato con $m$ è il punto tale che $f(m) \le f(x) \forall x \in D[f(x)]$.

NB: Questi punti possono anche non esistere, per esempio nel caso di $y = x$, ma i limiti $sup$ e $inf$ esistono sempre invece.

Oltre alla derivata prima possiamo usare la derivata seconda:

* Se la derivata seconda è negativa nel punto critico allora il punto critico è un punto di massimo locale
* Se la derivata seconda è positiva nel punto critico allora il punto critico è un punto di minimo locale
* Se la derivata seconda è uguale a 0 non è possibile trarre conclusioni e bisogna usare la derivata prima

### Asintoti e limiti

#### Asintoti verticali

L’asintoto verticale esiste solo quando la funzione è indeterminata in uno (o più) punti, ergo quando c’è 0 a denominatore di una frazione o un valore negativo all'interno di una radice.

Esempio:

$\displaystyle f(x) = x + \frac{4}{x}$, per questa funzione è immediato determinarlo perché il denominatore è 0 solo quando $x = 0$.

#### Asintoti orizzontali

Gli asintoti orizzontali esistono anche loro solo con le funzioni razionali, ci interessa solo il grado più alto del denominatore e del numeratore.

* Se il grado del numeratore è minore del grado del denominatore, allora l’asse $x​$ è un asintoto orizzontale
* Se il grado del numeratore è maggiore del grado del denominatore, allora non esistono asintoti orizzontali
* Se i gradi si eguagliano allora il rapporto tra i coefficienti è l’asintoto orizzontale

Esempio:

$\displaystyle f(x) = \frac{x^2 + 4}{x}$

$N = 2, D = 1$

$N > D$

Quindi non esistono asintoti orizzontali.

#### Asintoti obliqui

Gli asintoti obliqui esistono solo quando il grado del numeratore è più grande di 1 del grado del denominatore.

Esempio:

$\displaystyle f(x) = \frac{x^2 + 4}{x}$, abbiamo già trovato che $N = D + 1$ quindi dobbiamo trovare gli asintoti obliqui, dividiamo il denominatore per il numeratore:



$\displaystyle f(x) = \frac{x^2 + 0x + 4}{x} \\
\begin{array}{c, c, c | c, c}
x^2 & 0x & 4 & x &  \\
\hline 
x^2 & 0x & / & x & / \\
0 & 0x & 4 & / & / \\
\hline 
0 & 0x & 4 & x \\
\end{array} \\ \displaystyle f(x) = x + \frac{4}{x}$

$P(x) = Q(x) D(x) + R(x) $

L’asintoto obliquo è la linea $y = x$.

#### Limiti

C’è semplicemente da calcolare i due limiti: $\displaystyle \lim_{x \to \pm \infin} f(x)$.

Esempio:

$\displaystyle f(x) = \frac{x^2 + 4}{x}​$

$\displaystyle \lim_{x \to - \infin} f(x) = \lim_{x \to -\infin} \frac{x^2(1 + \frac{4}{x^2})}{x} = - \infin$

$\displaystyle \lim_{x \to +\infin} f(x) = \lim_{x \to +\infin} \frac{x^2(1 + \frac{4}{x^2})}{x} = + \infin$

### Disegno finale

Disegniamo gli asintoti:

![1548262969998](assets/1548262969998.png)

Usiamo le informazioni sulla concavità per finire il disegno:

![1548263016955](assets/1548263016955.png)

# Calcolo infinitesimale 2 (Calculus 2)

## Integrali

### Integrali indefiniti

Un integrale indefinito è l’opposto della derivata, infatti l’integrale può essere chiamato anche anti-derivata.

Un integrale è definito da: $\int f(x) dx = F(x) + c$ dove $\frac{d}{dx}F(x) = f(x)$. $c$ è la costante d’integrazione, generata dal fatto che nelle derivate i numeri costanti “spariscono”.

La costante d’integrazione è possibile valutarla solo se ci viene dato un valore $y = c$ e la derivata della funzione $y'$.

### Integrali definiti

Un integrale definito è un integrale limitato ad un intervallo.

Per calcolare l’integrale definito si usa la seguente regola: $\int_a^b f(x) dx = F(b) - F(a)$.

In questo caso la costante d’integrazione non ci interessa più perché si semplifica.

L’integrale definito rappresenta l’area sottesa alla curva $f(x)​$.

![1548338138400](assets/1548338138400.png)

Però può capitare che parte dell’area sia “negativa”, infatti consideriamo, per esempio, l’intervallo $(-1, 2)$.

![1548338208525](assets/1548338208525.png)

In questo caso se ci interessa l’area vera e propria sottesa alla curva dobbiamo spezzare l’integrale.

Quindi calcolare l’integrale ci dà sempre l’area orientata sottesa dal grafico della funzione, se ci interessa l’area sottesa dobbiamo considerare l’eventualità in cui l’integrale sia negativo nell’intervallo prestabilito.

### Regole di approssimazione

Ha senso studiarle solo per lo scritto?

#### Approssimazione con Riemann (todo?)

#### Approssimazione con la regola dei trapezi (todo?)

#### Approssimazione con la regola di Simpson (parabole) (todo?)

### Teorema fondamentale del calcolo integrale

Se $r(x)​$ è continua in $[a,b]​$ allora: $\displaystyle f(x) = \int_a^x r(t) dt​$ è continua in $[a,b]​$, derivabile in $(a,b)​$ e $\displaystyle f'(x) = r(x)​$.

Questo significa che $f(x)​$ è derivabile per ottenere $r(x)​$, il Teorema ci consente di fare controlli di correttezza nel calcolo integrale.

Inoltre: $\displaystyle \int_a^b f(x) dx = F(x)|^b_a = F(b) - F(a)$ se $f(x)$ è continua in $[a,b]$ e $F(x)$ è l’integrale o anti-derivata di $f(x)$; questa seconda parte del teorema ci consente di calcolare esattamente l’area sottesa al grafico.

### Formula di riduzione

### Tabella delle regole di integrazione

| Eventuale condizione   | Funzione integrale                           | Funzione integrata                                       |
| ---------------------- | -------------------------------------------- | -------------------------------------------------------- |
|                        | $\displaystyle \int f(x) + g(x) dx$          | $\displaystyle \int f(x) dx +\displaystyle \int g(x) dx$ |
| $f(x)$ pari            | $\displaystyle \int_{-a}^a f(x) dx$          | $\displaystyle 2 \int_0^a f(x) dx$                       |
| $f(x)$ dispari         | $\displaystyle \int_{-a}^a f(x)$             | $0$                                                      |
| $c \in \R$             | $\displaystyle \int c f(x) dx$               | $\displaystyle c \int f(x) dx$                           |
| $u = g(x), du = g'(x)$ | $\displaystyle \int f(g(x)) \times g'(x) dx$ | $\displaystyle \int f(u) du$                             |
|                        | $\displaystyle \int f(x)g'(x) dx$            | $\displaystyle f(x)g(x) - \int f'(x)g(x)dx$              |

### Tabelle degli integrali (todo)

#### Generici

| Funzione integrale                         | Funzione integrata                       |
| ------------------------------------------ | ---------------------------------------- |
| $\displaystyle \int a dx$                  | $ax$                                     |
| $\displaystyle \int [f(x)]^n f'(x) dx$     | $\displaystyle \frac{[f(x)]^{n+1}}{n+1}$ |
| $\displaystyle \int \frac{f'(x)}{f(x)} dx$ | $\displaystyle ln(|f(x)|)$               |
| $\displaystyle \int e^{f(x)} f'(x) dx$     | $e^{f(x)}$                               |
| $\displaystyle \int a^{f(x)} f'(x) dx$     | $\displaystyle \frac{a^{f(x)}}{ln(a)}$   |
|                                            |                                          |
|                                            |                                          |
|                                            |                                          |

#### Trigonometrici

|      |      |
| ---- | ---- |
|      |      |
|      |      |
|      |      |



#### Trigonometrici inversi

$$
\int arctg(x) dx = x \times arctg(x) - \frac{1}{2} ln | x^2 + 1 |
$$



### Integrazione per frazioni parziali

Supposto di avere una funzione razionale, quindi: $\displaystyle f(x) = \frac{P(x)}{Q(x)}$, è possibile determinare $\displaystyle \int f(x)$ in 4 fasi:

1. [Divisione](#Divisione)
2. [Fattorizzazione](#Fattorizzazione)
3. [Sistema Lineare](#Sistema lineare)
4. [Integrazione](Integrazione)

#### Divisione

Nel caso che $deg P(x) > deg Q(x)​$ possiamo effettuare la divisione polinomiale.

$P(x) = A(x) Q(x) + R(x)$

$f(x) = \frac{P(x)}{Q(x)} = A(x) + \frac{R(x)}{Q(x)}​$

#### Fattorizzazione

Scomponi $Q(x)$ come prodotti di polinomi di primo o secondo grado irriducibili.

#### Sistema lineare

Si presentano quindi due possibilità, o le molteplicità sono tutte 1 oppure no.

$\displaystyle f(x) = \frac{P(x)}{Q(x)} = \frac{A}{p_1(x)} + \frac{B}{p_2(x)} + ...$

A denominatore ($p_1(x)$) ci sono i singoli fattori di $Q(x)$, come numeratore ci sono invece delle incognite, queste possono essere numeri (se il fattore ha grado 1) o polinomi di grado 1 su fattori di grado 2.

Se abbiamo un grado secondo allora si usa la formula di Hermite, dove al denominatore della derivata (vedi formula sotto) si mettono tutti i fattori di $Q(x)​$ con molteplicità scalata di uno ed al numeratore un generico polinomio con grado minore di uno rispetto al denominatore, dopo è possibile costruire il sistema lineare.

$\displaystyle f(x) = \frac{P(x)}{Q(x)} = \frac{A}{p_1(x)} + \frac{B}{p_2(x)} + ... + \frac{d}{dx}\left( \frac{P_1(x)}{Q(x)} \right)$

Con un esempio è tutto più chiaro:

$\displaystyle \frac{x^2+5}{(x+1)^3 (x+2) (x^2+1)^2} = \frac{A}{x+1} + \frac{B}{x+2} + \frac{Cx + D}{x^2+1} + \frac{d}{dx} \frac{Ex^3 + Fx^2 + Gx + H}{(x+1)^2(x^2+1)}​$

Si calcola la derivata col metodo del quoziente e si risolve il sistema lineare per poi integrarlo.

#### Integrazione

Una volta risolto il sistema lineare si tratta solo di integrare la funzione.

### Integrali impropri (todo)

## Serie e successioni

### Caratteristiche

#### Definizione

Una successione è una lista di numeri ordinati in un modo particolare (come $1, 2, 4, 8, ...$).

La serie è invece la somma di una sequenza.

Un esempio è: $\displaystyle \sum_{n = 1}^\infin \frac{1}{2^n} = \frac{1}{2} + \frac{1}{4} + ... + \frac{1}{2^{\infin}} = 1​$.

#### Dalla successione alla serie

Non esiste un metodo preciso per trovare la serie però possiamo identificare delle fasi:

1. Costruire una tabella con la successione ($n = 1, a_1 = b; n = 2, a_2 = c; ...​$)
2. Trovare cosa lega i due numeri

In particolare il segno ha anche un valore importante, infatti:

* Se tutti i termini della successione sono positivi $a_n​$ è positivo
* Se tutti i termini della successione sono negativi $a_n$ è negativo
* Se i segni sono alternati $a_n = (-1)^n \or a_n = (-1)^{n+1}​$, se i numeri pari sono positivi è il primo caso, altrimenti se i numeri dispari sono negativi si ha il secondo caso

Esempio:

$\displaystyle \left\{ -\frac{1}{4}, \frac{8}{5}, - \frac{27}{6}, \frac{64}{7} \right\}$

| $n$  | $a_n $                         |
| ---- | ------------------------------ |
| $1$  | $\displaystyle - \frac{1}{4}$  |
| $2$  | $\displaystyle \frac{8}{5}$    |
| $3$  | $\displaystyle - \frac{27}{6}$ |
| $4$  | $\displaystyle \frac{64}{7}$   |

Anzitutto la serie ha i numeri pari positivi quindi possiamo includere $(-1)^n$.

Il numeratore è costituto invece da cubi, quindi $N = n^3$.

Infine il denominatore incrementa di $1$ a partire da $4$ quindi $D = n + 3$ (perché $n = 1, 2 , 3,...$).

Quindi la serie è definita da: $\displaystyle \sum_{n=1}^{\infin} (-1)^n \frac{n^3}{n+3}$, questa è una serie a segni alterni.

#### Convergenza di una successione

Se una successione converge significa che il limite della successione esiste per $n \to \infin​$, se invece non converge allora la successione diverge, questo significa sempre che ogni successione o diverge o converge anche se può essere difficile capire il caso.

È possibile usare il teorema del confronto al fine di semplificare questo processo.

Ad esempio:

$\displaystyle a_n = \frac{sin^2(n)}{3^n} \implies -1 \le sin(n) \le 1 \iff 0 \le sin^2(n) \le 1 \iff 0 \le \frac{1}{3^n} sin^2(n) \le \frac{1}{3^n}$

A questo punto calcoliamo il limite: $\displaystyle \lim_{n \to \infin} \frac{1}{3^n} = 0$.

Quindi la successione converge a $0$.

#### Crescenza, decrescenza e monotonia

Una successione è sempre o monotona o non monotona, se è monotona vuol dire che è o sempre crescente o sempre decrescente.

Per dimostrare le caratteristiche il metodo è il seguente:

* Una successione è crescente se $a_n \le a_{n+1}$.
* Una successione è decrescente se $a_n \ge a_{n+1}$.
* Una successione non è monotona se $a_n \le a_{n+1} \ge a_{n+2} \or a_n \ge a_{n+1} \le a_{n+2}$.

Da qui possiamo già dedurre che tutte le serie a segni alterni sono non-monotone.

#### Successioni delimitate

Solo le successioni monotone possono essere delimitate, perché per esser delimitate devono crescere o decrescere.

In generale se una serie è convergente esiste il limite per $n \to \infin​$.

Possiamo avere 3 casi:

* La successione è delimitata “sopra” dal valore maggiore della successione.
* La successione è delimitata “sotto” dal minor valore della successione.
* La successione è delimitata sopra e sotto.

Se una successione è monotona crescente sarà delimitata da sotto da $a_1$, cioè dal suo primo elemento.

Viceversa per le successioni decrescenti, bisogna però determinare il limite per $n \to \infin $.

### Tipi di serie

#### Serie geometrica

Una serie geometrica è una serie definita in generale da: $\displaystyle \sum_{n=1}^{\infin} ar^{n-1} = a(1+r+r^2+...)$.

La convergenza di una serie geometrica è legata ad $r$, infatti possiamo dire che:

* Se $|r| < 1$ allora la serie converge.
* Se $|r| \ge 1$ allora la serie diverge.

Per esempio:
$$
\sum_{n=0}^{\infin} \frac{2^{n-1}}{3^n} = 
\sum_{n=0}^{\infin} \frac{2^{n}2^{-1}}{3^n} = 
\sum_{n=0}^{\infin} \frac{1}{2} \frac{2^{n}}{3^n} = 
\sum_{n=0}^{\infin} \frac{1}{2} \left(\frac{2}{3}\right)^n \iff 
\begin{cases}
a = \frac{1}{2} \\
r = \frac{2}{3}
\end{cases}
$$
Quindi la serie converge.

Per trovare il valore della somma usiamo: $\displaystyle \frac{a}{1-r}​$.

Nell'esempio precedente il valore del limite della somma vale: 
$$
\frac{\frac{1}{2}}{1-\frac{2}{3}} = 
\frac{\frac{1}{2}}{\frac{1}{3}} = 
\frac{1}{2} \frac{3}{1} = 
\frac{3}{2}
$$
Per trovare i valori tali per cui la serie è convergente sfruttiamo le proprietà elencate finora, infatti basta trovare $r​$, per esempio:
$$
\sum_{n=1}^{\infin} (-5)^n x^n = 
\sum_{n=1}^{\infin} (-5x)^n = 
(-5x)^1 + (-5x)^2 + (-5x)^3 + ... =
(-5x)[1 + (-5x) + (-5x)^2 + ...] \\
\text{Converge se: } \\
|r| < 1 \iff -1 < r < 1 \\
|-5x| < 1 \iff 
|5x| < 1 \iff 
-1 < 5x < 1 \iff 
- \frac{1}{5} < x < \frac{1}{5} \iff
\left| \frac{1}{5} \right| < x
$$

#### Serie armonica

Esistono molti tipi di serie armonica, la più semplice è: $\displaystyle \sum_{n=1}^{\infin} \frac{1}{n}$, questa serie è a termini positivi e pertanto diverge positivamente sempre.

La serie armonica generalizzata è della forma: $\displaystyle \sum_{n=1}^{\infin} \frac{1}{n^p}$.

La serie:

* Converge quando $p > 1$
* Diverge quando $p \le 1$

Esempio:
$$
\sum_{n = 1}^{\infin} \frac{1}{\sqrt{n}} = \sum_{n = 1}^{\infin} \frac{1}{n^{1/2}} 
$$
Dato che $p = 1/2$ la serie diverge.

#### Serie telescopica

La serie telescopica è una serie definita da: $\displaystyle \sum_{n=1}^{\infin} (a_n - a_{n+k})​$

È una serie di cui è facile trovare la convergenza perché tutti i termini della serie, eccetto il primo e l’ultimo, si cancellano tra loro; per verificarne la convergenza dobbiamo farne il limite per $n \to \infin$, se esiste la serie converge altrimenti diverge.

Esempio:
$$
\sum_{n=1}^{\infin} \frac{1}{n} - \frac{1}{n+1} \\
\begin{matrix}
n = 1 & \displaystyle \frac{1}{1} - \frac{1}{1+1} = 1 - \frac{1}{2} \\
n = 2 & \displaystyle \frac{1}{2} - \frac{1}{2+1} = \frac{1}{2} - \frac{1}{3} \\
n = 3 & \displaystyle \frac{1}{3} - \frac{1}{3+1} = \frac{1}{3} - \frac{1}{4} \\
\vdots & \vdots \\
\hline
\end{matrix} \\
s = \lim_{n \to \infin} s_n = 
\sum_{n=1}^{\infin} \frac{1}{n} - \frac{1}{n+1} = 
\lim_{n \to \infin} 
\left[ 
\left( 1 - \frac{1}{2} \right) + 
\left(\frac{1}{2} - \frac{1}{3} \right) + 
\left( \frac{1}{3} - \frac{1}{4} \right) + \dots +
\left(\frac{1}{n} - \frac{1}{n+1} \right) 
\right] = \\
\lim_{n \to \infin} 1 - \frac{1}{n+1} = 
\lim_{n \to \infin} 1 - \frac{1}{n} = 
\lim_{n \to \infin} 1 - 0^{+} = 1
$$
Per trovare la somma della serie il procedimento è identico.

#### Serie di potenze

#### Serie di Taylor

##### Limiti con Taylor

### Prove di convergenza o divergenza

#### Prova di divergenza con l’ennesimo termine

Questa prova di divergenza è semplice e si può usare immediatamente; in particolare si calcola: $\displaystyle \lim_{n \to \infin} a_n$.

* Se il limite è uguale a 0, la prova non vale e bisogna usare altri mezzi
* Se il limite è diverso da 0 allora la serie diverge, senza sapere però se positivamente o negativamente

Esempio:
$$
\sum_{n=1}^{\infin} \frac{4n^3 - 4}{3n^3 + 2} \\
\lim_{n \to \infin} \frac{4n^3 - 4}{3n^3 + 2} = 
\lim_{n \to \infin} \frac{n^3(4- \frac{4}{n^3})}{n^3 (3+ \frac{2}{n^3})} = 
\lim_{n \to \infin} \frac{4n^3}{3n^3} = \frac{4}{3}
$$
Quindi è stabilito che la serie diverge.

#### Prova di divergenza o convergenza con il confronto

È possibile usare il teorema del confronto per determinare la convergenza o divergenza, supponiamo di avere la serie $a_n​$ e la serie simile (ma più semplice) $b_n​$, questa seconda serie ha senso cercare di costruirla come geometrica o armonica perché per entrambe abbiamo un metodo preciso per determinare la convergenza.

La prova col confronto mostra che:

* La serie $a_n$ diverge se:
	* La serie $a_n$ è maggiore o uguale alla serie $b_n$ e se entrambe sono positive: $a_n \ge b_n \ge 0$ e se $b_n$ diverge
* La serie $a_n$ converge se:
	* La serie $a_n$ è minore o uguale alla serie $b_n$ e se entrambe sono positive: $0 \le a_n \le b_n$ e se $b_n$ converge

Esempio:
$$
a_n = \sum_{n=1}^{\infin} \frac{n}{\sqrt{n^5} + n} \\
b_n = \frac{n}{\sqrt{n^5}} = \frac{n}{n^{5/2}} = n^{1 - 5/2} = n^{-3/2} = \frac{1}{n^{3/2}} \\
b_n = \sum_{n=1}^{\infin} \frac{1}{n^{3/2}}
$$
$b_n$ è una serie armonica, essendo $p = 3/2$ sappiamo che $b_n$ converge; ora usiamo la disuguaglianza per la convergenza.
$$
0 \le a_n \le b_n \iff 0 \le \frac{n}{n^{5/2} + n} \le \frac{1}{n^{3/2}}
$$
Sia $a_n$ che $b_n$ sono sicuramente positivi quindi possiamo confrontare solo loro due.
$$
\frac{n \times n^{3/2}}{n^{5/2} + n} \le 1 \iff 
\frac{n^{5/2}}{n^{5/2} + n} \le 1 \iff
n^{5/2} \le n^{5/2} + n \iff n \ge 0
$$
Sappiamo quindi che la serie $a_n$ converge.

#### Prova di divergenza o convergenza con il confronto dei limiti

Un metodo simile a quello appena illustrato include l’uso dei limiti, supponiamo di avere una serie $a_n$ ed una simile $b_n$.

Questa prova dimostra che:

* La serie $a_n$ diverge se:
	* $a_n \ge 0$ e $b_n > 0$
	* $\displaystyle L = \lim_{n \to \infin} \frac{a_n}{b_n}$ e $0 < L < \infin$
	* La serie $b_n​$ diverge
* La serie $a_n$ converge se:
	* $a_n \ge 0$ e $b_n > 0 $
	* $\displaystyle L = \lim_{n \to \infin} \frac{a_n}{b_n}$ e $0 < L < \infin$
	* La serie $b_n​$ converge

Esempio:
$$
a_n = \sum_{n=1}^{\infin} \frac{6n}{2n^3 + 3} \\
b_n = \frac{n}{n^3} = \frac{1}{n^2}
$$
$b_n$ è una serie armonica con $p = 2$, quindi $b_n$ converge.
$$
a_n \ge 0 \iff \frac{6n}{2n^3 + 3} \ge 0\ \forall n > 0 \\
b_n > 0\ \forall n \ne 0
$$
Entrambe le serie sono positive perché $n$ è positivo.
$$
L = \lim_{n \to \infin} \frac{a_n}{b_n} = 
\lim_{n \to \infin} \frac{\frac{6n}{2n^3 + 3}}{\frac{1}{n^2}} =
\frac{6n}{2n^3 + 3} n^2 =
\frac{6n^3}{2n^3 + 3} =
\frac{n^3(6)}{n^3(2 + \frac{3}{n^3})} =
\frac{6}{2} = 
3
$$
Possiamo quindi dire che la serie $a_n​$ converge.

#### Prova di convergenza con l’integrale

Un metodo per testare la convergenza di una serie è usare un integrale improprio.

La serie ha 3 condizioni perché si possa usare questo test:

* **Positiva**: Tutti i termini della serie devono essere positivi ($a_n > 0\ \forall\ n \in \N$)
* **Decrescente**: Tutti i termini devono essere decrescenti ($a_n > a_{n+1}\ \forall\ n \in \N$)
* **Continua**: La serie è definita in tutto il suo dominio

Data una serie $\displaystyle \sum_{n=1}^{\infin} a_n$ si costruisce l’integrale $\displaystyle \int_1^{\infin} f(x) dx$ dove $f(x) = a_n$.

Calcolato il valore dell’integrale è possibile dire che:

* Se l’integrale converge ad un numero reale, anche la serie converge, NB: questo valore non è detto che sia lo stesso per l’integrale e per la serie
* Se l’integrale diverge a infinito, anche la serie diverge

Esempio:
$$
\sum_{n=1}^{\infin} \frac{3}{n^2}
$$

* Verifico che la serie è positiva

$\displaystyle \frac{3}{n^2} > 0 \iff n^2 > 0$, la condizione è verificata $\forall n > 0$, dato che la serie parte da $1$ la serie è positiva.

* Verifico la decrescenza dei termini

$$
\frac{3}{(n-1)^2} > \frac{3}{n^2} \iff n^2 > (n-1)^2 \iff n^2 - (n^2 - 2n + 1) > 0 \iff 2n -1 > 0 \iff n > \frac{1}{2}
$$

La decrescenza è verificata in tutto il dominio perché $n \ge 1$.

* Verifico la continuità

L’unico punto di discontinuità possibile è $n = 0$, non essendo nel dominio di $n$ la serie è continua.
$$
\int_{1}^{\infin} f(x)dx = 
\int_{1}^{\infin} \frac{3}{x^2} dx = 
3 \int_{1}^{\infin} x^{-2}dx = 
\left[ 3 \frac{1}{-1} x^{-1} \right]_1^{\infin} =
\left[ - \frac{3}{x} \right]_1^{\infin} =
- \frac{3}{\infin} - \left( - \frac{3}{1} \right) =
3 - \frac{3}{\infin} = 3
$$

### Errore o resto di una serie

Le serie, essendo infinite, in alcuni casi, non possono essere stimate ma è possibile trovare l’errore o resto dopo $n$ passaggi (quindi calcolando la serie in modo parziale).

Per trovare il resto di una serie bisogna:

1. Stimare la somma della serie con una somma parziale
2. Usare la prova del confronto per dire se la serie converge o diverge (se diverge non ha senso stimare il valore)
3. Usare la prova dell’integrale risolvendo per il resto

Esempio:

Usa i primi 6 termini per stimare il resto della serie:
$$
s = \sum_{n=1}^{\infin} \frac{n}{2n^4 + 3}
$$

* Calcolo $s_6$

$$
\begin{matrix}
n=1 & a_1 = \displaystyle \frac{1}{2(1)^4 + 3} = \frac{1}{5} \\
n=2 & a_2 = \displaystyle \frac{2}{2(2)^4 + 3} = \frac{2}{35} \\
n=3 & a_3 = \displaystyle \frac{3}{2(3)^4 + 3} = \frac{3}{162} = \frac{1}{54} \\
n=4 & a_4 = \displaystyle \frac{4}{2(4)^4 + 3} = \frac{4}{515} \\
n=5 & a_5 = \displaystyle \frac{5}{2(5)^4 + 3} = \frac{5}{1'253} \\
n=6 & a_6 = \displaystyle \frac{6}{2(6)^4 + 3} = \frac{6}{2'595} \\
\end{matrix} \\
s_6 = \frac{1}{5} + \frac{2}{35} + \frac{1}{54} + \frac{4}{515} + \frac{5}{1'253} + \frac{6}{2'595} \approx 0.2897
$$

* Prova del confronto

$$
b_n = \frac{n}{n^4} = \frac{1}{n^3}
$$

Dato che $p = 3$ la serie $b_n$ converge.
$$
0 \le a_n \le b_n \iff 0 \le \frac{n}{2n^4 + 3} \le \frac{1}{n^3}
$$
Entrambe le serie sono sempre maggiori o uguali di $0$ perché hanno tutti i membri positivi ($a_n$ è negativa solo quando $n < 0$ ma $n \ge 1$, $b_n$ è negativa solo quando $n<0$ ma $n \ge 1 $).
$$
\frac{n}{2n^4+3} \le \frac{1}{n^3} \iff \frac{1}{2n^4+3} \le \frac{1}{n^2} \iff n^2 \le 2n^4 + 3
$$
Ci possiamo già fermare nella disequazione perché, essendo $n \ge 1$, non ci sono punti comuni tra le due parabole, la seconda parabola parte infatti da $(1,5)$ mentre la prima da $(1,1)​$.

# Calcolo infinitesimale 3 (Calculus 3)

## Equazioni differenziali di primo ordine

### Problema di Cauchy

