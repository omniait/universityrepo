import numpy as np
import matplotlib.pyplot as plt

'''
# dipendenza
v1 = np.array([0, 0, 2, 4])
v2 = 2 * v1
'''

'''
#indipendenza
v1 = np.array([0,0,1,3])
v2 = np.array([0,0,5,3])
'''

# dipendenza

v1 = np.array([0,0,1,3])
v2 = np.array([0,0,5,3])
v3 = np.array([0,0,2,4])


soa = np.array([v1, v2, v3])

X, Y, U, V = zip(*soa)

plt.figure()
ax = plt.gca()

ax.quiver(X, Y, U, V, angles='xy', scale_units='xy',
          scale=1, color=['r', 'g', 'b'])

ax.set_xlim([-1, 10])
ax.set_ylim([-1, 10])
# Show the major grid lines with dark grey lines
plt.grid(b=True, which='major', color='#666666', linestyle='-')

# Show the minor grid lines with very faint and almost transparent grey lines
plt.minorticks_on()
plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)

x_axis = np.linspace(-10, 10)
y_axis = np.zeros(50)
plt.plot(x_axis, y_axis, color="black")

y_axis = np.linspace(-10, 10)
x_axis = np.zeros(50)
plt.plot(x_axis, y_axis, color="black")

plt.draw()
plt.show()
