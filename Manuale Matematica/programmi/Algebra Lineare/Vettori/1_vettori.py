import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

mpl.rcParams['legend.fontsize'] = 10

fig = plt.figure()

ax = fig.gca(projection='3d')

x_1 = np.linspace(0, 3, 100)
y_1 = np.linspace(0, -2, 100)

x_2 = np.linspace(0, 4, 100)
y_2 = np.linspace(0, -3, 100)
z_2 = np.linspace(0, 2, 100)

ax.plot(x_2, y_2, z_2, label='3d vector')

ax.plot(x_1, y_1, 0, label='2d vector')

ax.legend()

plt.show()
