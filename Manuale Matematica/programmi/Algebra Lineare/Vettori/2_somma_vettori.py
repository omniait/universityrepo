import numpy as np
import matplotlib.pyplot as plt

#soa = np.array([[0, 0, 4, 2], [0, 0, -2, 6], [0, 0, 2, 8], [4, 2, -2, 6], [-2, 6, 4, 2]])
soa = np.array([0,0,1,2])

X, Y, U, V = zip(soa)
plt.figure()
ax = plt.gca()
ax.quiver(X, Y, U, V, angles='xy', scale_units='xy', scale=1, color=['r', 'g', 'b', 'g', 'r'])
ax.set_xlim([-5, 10])
ax.set_ylim([-5, 10])
# Show the major grid lines with dark grey lines
plt.grid(b=True, which='major', color='#666666', linestyle='-')

# Show the minor grid lines with very faint and almost transparent grey lines
plt.minorticks_on()
plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)

x_axis = np.linspace(-10, 10)
y_axis = np.zeros(50)
plt.plot(x_axis, y_axis, color="black")

y_axis = np.linspace(-10, 10)
x_axis = np.zeros(50)
plt.plot(x_axis, y_axis, color="black")

plt.draw()
plt.show()
