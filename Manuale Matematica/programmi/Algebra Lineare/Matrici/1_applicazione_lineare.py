import numpy as np
import matplotlib.pyplot as plt


v1 = np.array([2, 3])
M1 = np.array([
    [2, -1],
    [1, 3]
])
M2 = np.array([
    [-2, 3],
    [1, -1]
])
v2 = np.matmul(M1, v1)
v3 = np.matmul(M2, v1)

soa = np.array([v1, v2, v3])

U, V = zip(*soa)

plt.figure()
ax = plt.gca()

ax.quiver(0, 0, U, V, angles='xy', scale_units='xy',
          scale=1, color=['r', 'g', 'b'])

ax.set_xlim([-1, 5])
ax.set_ylim([-2, 15])
# Show the major grid lines with dark grey lines
plt.grid(b=True, which='major', color='#666666', linestyle='-')

# Show the minor grid lines with very faint and almost transparent grey lines
plt.minorticks_on()
plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)

x_axis = np.linspace(-100, 100)
y_axis = np.zeros(50)
plt.plot(x_axis, y_axis, color="black")

y_axis = np.linspace(-100, 100)
x_axis = np.zeros(50)
plt.plot(x_axis, y_axis, color="black")

plt.draw()
plt.show()
