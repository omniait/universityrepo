import numpy as np 

# matrice con dipendenza lineare
A = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [2, 4, 6]
])

# base canonica R3
B = np.array([
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1]
])

# A ridotta con gauss
C = np.array([
    [1, 2, 3],
    [0, 1, 2],
    [0, 0, 0]
])

# Matrice indipendente
D = np.array([
    [0, 1, 2],
    [5, 0, 0],
    [0, 0, 1]
])

print(np.linalg.det(A))
print(np.linalg.det(B))
print(np.linalg.det(C))
print(np.linalg.det(D))
