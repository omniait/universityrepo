# Smettetela di usare le macchine virtuali

## Un manifesto a favore della virtualizzazione a "container"

Con la crescita esponenziale della virtualizzazione e del cloud computing (archiviazione, elaborazione di risorse informatiche on-demand), c'è stato un aumento corrispondente nel numero medio di macchine virtuali (MV) gestiste dallo stesso System Admin.

Creare manualmente una MV con Hyper-V, VMWare, Openbox, etc. è sempre un grosso lavoro perché è necessario creare uno snapshot completo (un'immagine potremmo dire) dell'intera configurazione della MV per poi replicarla su un altro computer, questo ha costi sia di tempo sia di spazio non indifferenti in quanto la macchina virtuale contiene un vero e proprio Sistema Operativo (SO), le dipendenze dell'applicazione o servizio che offre, e l'applicazione o servizio vero e proprio.

Questa metodologia di lavoro è diventata obsoleta, c'è stato bisogno di un miglioramento nel modello e nel work-flow e qui accorre in aiuto una tecnologia relativamente recente: Docker.

Docker è un ambiente virtuale basato su un ambiente virtuale Linux (VE) (attenzione, questo non vuol dire che non sia disponibile su Windows o Mac!), diventato open-source nel 2013, permette la creazione e l'esecuzione di più ambienti virtuali Linux sullo stesso host.

A differenza di una MV un VE non necessita di virtualizzare un'intera macchina, con un proprio SO, core dedicati della CPU ed emulazione Hardware, ma usa le possibilità già esistenti nel Kernel del proprio SO.

In pratica Docker crea dei "container" dove vengono eseguite le applicazioni sviluppate dal programmatore, ricrea anche un SO che viene considerata un'altra applicazione; questo vuol dire che l'host (quindi il computer su cui viene avviato l'ambiente virtuale) avrà molto meno carico di elaborazione e spazio fisico.

![art1](assets/art1.jpg)

Docker ha molti altri vantaggi: 

* Portable Deployment - Ovvero puoi usare Docker per creare un "oggetto" singolo che contiene tutte le tue applicazioni, questo oggetto può essere trasferito e installato velocemente in qualunque altro host che supporta Docker.
* Versioning - Docker include già delle capacità ispirate al sistema di versionamento git, infatti traccia le versioni successive di un container, ispaziona le differenze, può effettuare un roll-back (un undo), etc.
* Component Reuse - Grazie alla sua struttura non è necessario avere 10 istanze di Apache, nginx, MySQL, etc. infatti questi servizi sono condivisi tra tutte le applicazioni.
* Leggerezza - Docker è molto più leggero rispetto ad una MV, viene avviato in pochi secondi e non si riserva le risorse HW direttamente ma prende solo lo stretto necessario.

Da sviluppatore o programmatore potrai capire la potenzialità che ha Docker, considerando anche che molti servizi cloud (Microsoft Azure, AWS, Digital Ocean, ...) supportano direttamente la creazione e l'esecuzione di container Docker.

Ovviamente non avrai mai lo stesso isolamento che ti offre una MV ma questa necessità è diventata negli anni, grazie ai grandi passi avanti nella sicurezza, sempre più piccola.

