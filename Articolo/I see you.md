# I see you

## L'evoluzione della sorveglianza di massa con le reti neurali e la dissoluzione della privacy

La ricerca nel campo delle Intelligenze Artificiali (IA), o Artificial Intelligence (AI), ha fatto passi da gigante in quest'ultimo ventennio, sono nati nuovi termini e nuovi campi scientifici come la Data Science (l'estrazione di conoscenza dai dati), il Deep ed il Shallow Learning (sono algoritmi che "simulano" il comportamento dei neuroni). Questi strumenti sono estremamente utili e stanno portando una rivoluzione di cui è difficile vedere la portata.

Per esempio è possibile, mediante l'uso delle CNN (Convultional Neural Network, fa parte del Deep Learning), riconoscere oggetti e dettagli nelle immagini, questo campo informatico viene chiamato Computer Vision (CV); le nuove IA ci hanno portato, dopo un lungo periodo di training, o potremmo dire di "rodaggio", ad avere auto autonome o con assistenza alla guida, che riconoscono immediatamente pericoli come un bambino che corre in mezzo alla strada od una macchina che tira una frenata improvvisa. 

Ora però immaginiamo un attimo di "infilare" questo algoritmo nelle telecamere a circuito chiuso (CCTV); beh, non c'è neanche bisogno di immaginarlo a dire il vero, perché dall'altra parte del mondo l'hanno fatto, come ci riferisce Reuters.

Quello che sta succedendo è che in Cina hanno creato un enorme database dei propri cittadini e, sfruttando il riconoscimento facciale con le varie foto dei documenti, hanno "armato" le CCTV di queste informazioni e algoritmi in modo tale che in qualunque momento i vari computer della polizia possano rintracciare ogni movimento di un determinato cittadino in un attimo. Questo non era possibile prima dell'avvento delle IA perché era necessario guardarsi ore ed ore di video di sorveglianza.

Gli stessi poliziotti sono stati dotati di occhiali dotati di telecamera con cui riescono ad analizzare ogni persona che hanno di fronte, in un attimo possono prendere ogni dato: data di nascita, precedenti penali, parenti, etc.

Un'altra misura che sta adottando il governo cinese, come ci riferisce Business Insider, è la creazione del "Social Credit System", per sintetizzare tutto quello che fai tutti i giorni ti dà un punteggio, positivo quando ti comporti "bene" e negativo viceversa, che può darti privilegi o punizioni in base ad una classifica pubblica (un po' come il Financial Credit System). Alcune punizioni sono: il divieto di usare l'aereo o il treno, rallentamento della connessione internet, il divieto di iscriversi nelle scuole migliori (a te e ai tuoi figli) ed impedire promozioni nel lavoro.

Tutto questo non vi ricorda niente? A me ricorda un po' Nineteen Eighty Four (1984) di Orwell.

Seppur possiamo stare tranquilli, essendo dall'altra parte del mondo, la nostra privacy è minacciata in altri modi, da una parte siamo noi stessi a danneggiarla, infatti, quasi tutti hanno profili su un qualche social network, sui siti di e-commerce od un account Google e tutto questo crea una nostra identità digitale ed una presenza online che poi viene sfruttata per del marketing mirato, cosa che può tranquillamente andar bene, però come possiamo anche esser sicuri che le notizie che leggiamo non siano state "prioritizzate" in base al nostro profilo? Dobbiamo anche tener conto per anni i nostri profili sono circolati online da compagnia a compagnia fornendo un continuo flusso di denaro e dando a tutti gli effetti un valore in valuta alle persone.

Purtroppo non esiste quasi più uno spazio privato in quanto viviamo già in un mondo di Big Data; prendiamo ad esempio i concetti IoT (Internet of Things) dove ogni dispositivo è connesso, tra i quali di sicuro il più diffuso è lo smartphone che tiene traccia di ogni nostro movimento, dei nostri luoghi preferiti dove torniamo spesso e della musica che ascoltiamo, dati che poi vengono passati ad altri servizi magari a nostra insaputa. Come possiamo difenderci da tutto ciò?