/// Return the Greatest Common Divisor between two numbers
/// using Euclid's Algorithm
///
/// # Arguments
///
/// * a: u64 - The first number
///
/// * b: u64 - The second number
///
/// # Example
///
/// euclid_gcd(1804,328) -> 164
pub fn euclid_gcd(mut a: u64, mut b: u64) -> u64 {
	while a != b {
		if a > b {
			a = a - b;
		} else {
			b = b - a;
		}
	}
	return a;
}

fn main() {
	let a:u64 = 1804;
	let b:u64 = 328;
	// assert_eq!(euclid_gcd(a,b), 164);

	println!("The Greatest Common Divisor Between {} and {} is {}", a, b, gcd::euclid_gcd(a,b))
}