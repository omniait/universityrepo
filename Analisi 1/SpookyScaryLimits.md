Limite Parametrico
$$
f(x) = \lim_{y \rightarrow 0} \frac{x^2}{x^2 + y^2}
$$

$$
a^{[x]} \le a^x \le a^{[x]+1}
$$

$$
a > 1 \\
f(x) = \frac{a^x}{x^k} \\
\lim_{x \rightarrow +\infin} f(x) = +\infin
$$

Limite BRTTSSM
$$
sin \left( n\frac{1}{2}\pi \right) \text{Non ha limite}
$$

$$
\lim_{n \rightarrow +\infin} sin(n) \\
-1 \le sin(n) \le 1 \\
\text{Se esiste un limite allora:} -1 \le L \le 1 \\
sin(n+1) \rightarrow L \\
sin(n-1) \rightarrow L \\
0 \leftarrow sin(n+1) - sin(n-1) = sin(n)cos(1) + cos(n)sin(1)-sin(n)cos(1)+cos(n)sin(1) = \\
2 cos(n) sin(1) \\
If: sin(n) \rightarrow L \implies 2cos(n)sin(1) \rightarrow 0 \iff cos(n) \rightarrow 0
$$

$$
sin^2(n)[1] + cos^2(n)[0] = 1 \\
\lim_{n \rightarrow + \infin} sin^2(n) = 1 \text{ IMPOSSIBRU}
$$

$$
sin(n+1) - sin(n) \rightarrow 0 \\
sin(n)cos(1) + cos(n)sin(1) - sin(n) \\
\lim_{n \rightarrow + \infin} sin(n)(cos(1)-1) = 0 \text{ IMPOSSIBRU 2}
$$

FIBINICCI
$$
a_0 = 1 \\
a_1 = 1 \\
a_{n+1} = a_n + a_{n-1} \\
b_n = \frac{a_{n+1}}{a_n} \\
b_{n+1} = 1 + \frac{1}{b_n} \\

\text{Quale sarà mai il limite della successione } b_n? \\
L = \frac{1}{L} \iff L^2 = 1 + L \iff L^2 + L + 1 = 0 \implies \frac{1 + \sqrt 5}{2} = \phi \\
b_n < L \\
b_n \le b_{n+1} < L \iff b_n \le 1 + \frac{1}{b_n} < \frac{1 + \sqrt 5}{2} \text{Non si verifica}
$$

$$
f[0,1] \rightarrow [0,1] \implies \exists x : f(x) = x \\
if: f(0) = 0 \quad x = 0 \\
0 < f(0) \le 1 \\
if: f(1) = 1 \quad x = 1 \\
0 < f(x) < 1 \\
\Phi (x) = f(x) -x \\
\Phi (0) > 0 \quad \Phi (1) < 0
$$

