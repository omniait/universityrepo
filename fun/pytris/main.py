x_bound = 10
y_bound = 10
tick = 1000

class Tetronim:

    def __init__(self, args):
        # self.x = int(args[0])
        # self.y = int(args[1])
        self.tiles = list(args[0]) 
        self.rotation = int(args[1])
        self.type = chr(args[2])
        #self.t_list = args[4]

    def rotate(self):

        new_rotation = self.rotation
        cur_rotation = self.rotation

        new_rotation = cur_rotation + 1

        if new_rotation >= 4:
            new_rotation = 0

        # rotate now
        
        
        self.rotation = new_rotation
        #self.x 

    def translate(self, x, y):
        if self.tiles[] + x < 0 or self.x + x > x_bound:
            pass
        else:
            self.x += x
        if self.y + y < 0 or self.y + y > y_bound:
            pass
        else:
            self.y += y

def main():
    t_list = [
        Tetronim([0, 0, 0, 'o']),
        Tetronim([0, 0, 0, 'i']),
        Tetronim([0, 0, 0, 'l'])
    ]

if __name__ == "main":
    main()