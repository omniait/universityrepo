/**Guessing Game
 * Si tratta di un gioco di coding dove bisogna
 * Far vincere il computer contro una persona
 * -------
 * La persona pensa un numero a caso, tra 0 e 50 estremi inclusi
 * Il computer deve indovinarlo, la persona dice al computer ogni volta
 * Se il numero da indovinare è maggiore o minore
**/

#include <iostream>
// #include <math.h>

using namespace std;

const int Max = 50;
const int Min = 0;

// 10 -> 15
enum Palline {Bianca = 10, Nera = 12, Verde = 11, Rossa = 13, Blu = 15, Viola = 14};

int guess_a_number(int min, int max)
{
    int guess;
    guess = (max + min) / 2;
    return guess;
}

int main()
{
    bool game_over = false;
    int guess;
    int min = Min, max = Max;
    char input;
    int i = 0;

    Palline pallina = Bianca;
    pallina = Nera;

    cout << "Pensa un numero a caso tra " << Min << " e " << Max << endl;
    cout << "Io cerchero` di indovinarlo" << endl;

    guess = guess_a_number(min, max);

    
    switch (pallina)
    {
        case Bianca:
            cout << "La pallina e` bianca" << endl;
            break;
        case Nera:
            cout << "La pallina e` nera" << endl;
            break;
        default:
            break;
    }

    while(!game_over && i < 5)
    {
        cout << "Il numero a cui hai pensato e` " << guess << "? (S, s)" << endl;    
        cout << "Oppure e` minore (L, l) o maggiore? (G, g)" << endl;
        cin >> input;
        input = tolower(input);
        // input = s, l, g
        
        switch (input)
        {
            case 's':
                cout << "Ho vinto!" << endl;
                game_over = true;
                break;
            case 'l':
                cout << "Hai detto che il numero e` minore, fammi pensare..." << endl;
                max = guess;
                guess = guess_a_number(min, max);
                break;
            case 'g':
                cout << "Hai detto che il numero e` maggiore, fammi pensare..." << endl;
                min = guess;
                guess = guess_a_number(min, max);
                break;
            default:
                cout << "Non ho capito" << endl;
                i--;
                break;
        }        
        i++;
    }

    if (!game_over) {
        cout << "Ho perso! :(" << endl;
    } 
    
    return 0;
}
