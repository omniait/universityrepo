#include <iostream>
#include <thread>
#include <chrono>
#include "tetris.hpp"
#include "utilities/utilities.hpp"

Tetris t = Tetris();

int main()
{
    bool running = true;
    std::thread input_thread(&Tetris::input, &t);
    //std::thread display_thread(&Tetris::display, &t);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    while(running)
    {
        //std::thread input_thread(&Tetris::input, &t);
        //std::thread display_thread(&Tetris::display, &t);

        cout << "calling display \n";
        t.display();
        cout << "calling input \n";

        //input_thread.join();
        input_thread.join();

        clear_terminal();
    }

    return 0;
}
