#include <stdlib.h>
#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif
/**sleepcp
 * Description:
 * This function delays the execution 
 * by a given time in milliseconds
 * @params:
 * @int milliseconds - The time in milliseconds
 * @return void
**/
void sleepcp(int milliseconds)
{
#ifdef WIN32
    Sleep(milliseconds);
#else
    usleep(milliseconds * 1000);
#endif
}

void clear_terminal()
{
    #ifdef WIN32
        system("cls");
    #else
        system("clear");
    #endif
}