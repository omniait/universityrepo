#include "utilities.hpp"
#include <future>
#include <thread>
#include <assert.h>
#include <stdio.h>

/**async_test
 * Description:
 * Just a test function for the async methods
 * @params
 * @int a - first number to multiply
 * @int b - second number to multiply
 * @return int - the multiplied number
**/
int async_test(int a, int b)
{
    return a * b;
}

/**sleep_test
 * Description:
 * Just a test function for the sleep method
 * @params
 * @none
 * @return void
**/
void sleep_test(int milliseconds)
{
    sleepcp(milliseconds);
}

/**thread_test
 * Description:
 * Just a test function for the thread methods
 * @params
 * @int a - first number to multiply
 * @int b - second number to multiply
 * @return int - the multiplied number
**/
//int thread_test(int a, int b)
//{
//    return a * b;
//}

void test_all_functions()
{
    // std::thread t1 = std::thread(thread_test, 2, 2);
    // std::thread t2 = std::thread(thread_test, 4, 2);

    auto future = std::async(std::launch::async, async_test, 2, 2);
    assert(future.get() == 4);

    //t1.join();
    //t2.join();
    printf("Before Sleep \n");
    sleep_test(1000);
    printf("After Sleep \n");
    printf("Clear the terminal in 3 seconds \n");
    sleep_test(3000);
    clear_terminal();

}

int main(int argc, char const *argv[])
{
    test_all_functions();
    return 0;
}
