#ifndef TETRIS_HPP
#define TETRIS_HPP

#include <iostream>

using namespace std;

enum directions
{
    RIGHT = 0,
    LEFT,
    DOWN,
    SPACE
};
enum block_type
{
    I,
    O
};

struct blocco
{
    block_type t;
    int x, y;
};

class Tetris
{
     int r, c;
    // Matrice che rappresenta il campo di tetris
     char **field;
    // array per i movimenti in
     bool queue[3];
    // coordinate forma attuale
     int x, y;
    //
     void rotate();
    // In base all'input dell'utente modifica il campo di tetris
     void move(directions d);
    // Funzioni per la gestione del movimento
     void switch_on(directions d);
    bool inserisci(int);

  public:
    Tetris(int r_ = 10, int c_ = 10);
    // Funzione per inserire  una forma casuale
    void input();

    void display();
};

#endif