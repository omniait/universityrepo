# Algebra Lineare

## 26 - 09 - 2018

### Informazioni Generali

* Prof. Placido Longo
* Email: placido.longo@unipi.it
* Ricevimento: Mercoledì 9 - 12, Dip. di matematica (via Filippo Buonarroti n.1/C)
* Sito: http://alan.dma.unipi.it
* Libro: Alam Lang

------

### Coordinate

Il punto è individuato da tanti componenti quante le dimensioni dell'array
$$
P_1 = (a_0, a_1, a_2) - Tre\ Dimensioni \\
P_2 = (a_0, a_1) - Due\ Dimensioni
$$
Per risolvere il problema rappresentato dal fatto che le funzioni associano uno e un solo valore dal dominio al codominio 
$$
\gamma : [0,2\pi] \rightarrow \R^2 = \R \times \R \\
\gamma(t) = \left( \frac{cos(t)}{sin(t)} \right)
$$

### Prodotto Cartesiano

$$
A x B = \{ (a.b) : a \in A, b \in B \} \\
A_1 x A_2 ... A_n = \{ (a_1, a_2,...a_n) : a_n \in A_n \}
$$

$$
\R^n = \{ (a_1,...,a_n) : a_i \in \R \quad \forall i = 1 * n \}
$$

$$
f: \R^2 \rightarrow \R \\
f: [a,b] x[c,d] \rightarrow \R \\
f: [0,\pi]x[0,2\pi]\rightarrow \R^3 \\
$$

$$
Sfera \\
x = sin\theta * cos\gamma \\
y = sin\theta * sin\gamma \\
z = cos\theta
$$

$$
Retta \\
x=2t \\
y = t \\
z = 3t
$$

$$
Cilindro \\
x = cos \theta \\
y = sin \theta \\
z = t \\
t \in \R \\
\theta \in [0, 2\pi]
$$

Non ho quasi la più pallida idea di cos'ho scritto, iniziamo bene.

### Numeri Scalari e Vettoriali

Numeri reali e/o complessi.

I numeri scalari si riferiscono a R, li rappresentiamo con le lettere greche.

I numeri all'interno della nupla del vettore (R^n) sono chiamati componenti, i vettori sono rappresentati con lettere dell'alfabeto minuscole.

Gli insiemi ovviamente sono rappresentati con lettere dell'alfabeto maiuscole.

Moltiplicazione numero scalare per vettoriale (es 3 * [1,2,3] = [3,6,9])
$$
\lambda * a := (\lambda * a_1, \lambda * a_2, ... \lambda * a_n)
$$
Si moltiplicano tutte le componenti del numero vettoriale per il numero scalare.
$$
\lambda a = \binom{\lambda a_1}{\lambda a_2}
$$

Già fatto con il professore alle superiori, mi ricordo.

## 27-09-2018

### Equazione con vettore 0

$$
\lambda a = 0 \\
(\lambda a_0, \lambda a_1... \lambda a_n) = (0, 0, ... 0)
$$

O ogni elemento del vettore a è 0 o lambda è 0.

### Somma tra vettori

$$
a + b = (a_0 + b_0, a_1 + b_1, ... a_n + b_n) = b+a \\
a + 0 = a \quad \forall a \in \R^n \\
a+(b+c) = (a+b)+c = (a+c)+b
$$

### Opposto del vettore

$$
-a = (-a_0, -a_1, ...-a_n) => a + (-a) = 0
$$

Ovviamente assieme alla somma tra vettori è possibile definire quindi anche la differenza come somma tra un primo vettore e l'opposto di un secondo vettore.

### Prodotto tra scalare e vettori

$$
\lambda(a+b) = \lambda a+ \lambda b \quad \forall \lambda \in \R, \forall a,b \in \R^n
$$

### Sommario proprietà

$$
X \times X \rightarrow X : \R \times X \rightarrow X \\
\exist 0 : a + 0 = 0 + a = a \\
\forall a \in X \exist (-a) : a+(-a) = 0 \\
a+b = b+a \quad \forall a,b \in X \\
(a+b)+c = a+(b+c) \quad \forall a,b,c \in X \\
1a = a \quad \forall a \in X \\
\lambda(\mu a) = (\lambda \mu)a \quad \forall \mu,\lambda \in \R, \forall a \in X \\
(\lambda + \mu) a = a\lambda+a\mu \quad \forall \mu,\lambda \in \R, \forall a \in X \\
\lambda(a+b) = \lambda a + \lambda b \quad \forall \mu,\lambda \in \R, \forall a \in X \\
a \in \R^n \\
|a| \ge 0
|a| = o <=> a=0
$$

Il linguaggio matematico è comprensibile, il Longhenese no.

## 28-09-2018

|a| rappresenta la norma, ovvero la "quantità" di dimensioni dell'array.

### Spazio Normato

$$
|a| \ge 0 \\
|a| = 0 <=> a = 0 \\
|\lambda a| = |\lambda||a| \\
|x+y| \le |x|+|y| \\
||x|| = max_{i=0..n}|x_i|
$$

### Versore

Un array diverso da 0 con norma 1.
$$
u \ne 0 \\
\hat{u} = u\frac{1}{|u|} \\
|\hat{u}| = |u\frac{1}{|u|}| = |u||\frac{1}{|u|}|
$$
Attenzione: Si suppone che il vettore abbia origine nell'origine cartesiana, quindi (0,0, etc.).
$$
|u| = |(1,2,3,4)| = \sqrt{1^2+2^2+3^2+4^2} = \sqrt{1+4+9+16} \\
\hat{u} = u \frac{1}{\sqrt{1+4+9+16}}
$$

### Esempio gravità

La norma del quadrato di x è la distanza tra i due corpi.
$$
F = G \frac{Mm}{|x|^2} \\
\overline{F} = G Mm \left(- \frac{x}{|x|^3} \right)
$$

### Vettore Velocità

È possibile calcolare la velocità del vettore nel seguente metodo:
$$
\overline{V} = \frac{\Delta S}{\Delta t} = \frac{t_a-t_0 }{t-t_0} \\
\gamma(t) = V \\
<a> = \{ t_a : t \in \R \}
$$
Una retta verticale può tranquillamente venire definita con:
$$
a = \binom{0}{45}
$$
Problema che viene posto in altre situazioni, come con le funzioni.

Appunto: Ci sto capendo più dal libro e dai video di Strang che dalle spiegazioni in classe, mi devo ricordare di parlare col professore e chiedere se posso studiare direttamente dal libro, chiedendo magari anche gli appunti lezione per lezione.

## 03 - 10 - 2018

### Retta o Vettori

Riprendiamo l'esempio del vettore spostamento:

$$
x(t) = x_0 + tv = x_0 + a(vettore)
$$

### Combinazioni lineari

Due dimensioni
$$
\alpha
\begin{bmatrix}
a \\
b
\end{bmatrix} +
\beta
\begin{bmatrix}
c \\
d
\end{bmatrix} = 
\left\{ \alpha u + \beta v : \alpha, \beta \in \R \right\} \\
u, v = \R^2 = \begin{bmatrix}
a \\
b
\end{bmatrix}, \begin{bmatrix}
c \\
d
\end{bmatrix}
$$
I vettori a tre dimensioni diventano:
$$
\begin{bmatrix}
a \\
b \\
c
\end{bmatrix} = u \in \R^3 \\
\begin{bmatrix}
d \\
e \\
f
\end{bmatrix} = v \in \R^3 \\
Piano:
\left\{ \alpha u+ \beta v: \alpha, \beta \in \R \right\}
$$
Date X dimensioni:
$$
x_0, x_1, ..., x_k \in X \\
Span : <x_0, x_1, ...,x_k> = \left\{ \sum_{i=1}^k \alpha_i x_i : \alpha_1,...\alpha_k \in \R (coefficienti) \right\} \\
$$
La sommatoria è detta "Combinazioni Lineari".

Se esistono delle x tali che X = <x1...xn> allora X si dice di dimensione finita.

### Piano che non passa per l'origine

Rappresentiamo tutti i punti di un piano che non interseca l'origine in 3 dimensioni:
$$
\Psi (\alpha, \beta) = x_0 + ? = x_0 + \alpha u + \beta v \\
\alpha, \beta \in \R
$$
Dove x0 è il vettore dall'origine al punto comune dei due vettori u e v.

### Spazio R

$$
e_0 = (1,0,0) \\
e_1 = (0,1,0) \\
e_2 = (0,0,1) \\
Canonica : \R^3 = < e_0, e_1, e_2 > 
$$

Le "e" vengono chiamate generatori in quanto generano lo spazio.

### Prodotto scalare / dot product

Il prodotto scalare tra due vettori è 0 se i vettori sono ortogonali.
$$
x, y \in \R^n \\
x \cdot y = xy = \sum^n_{i=1} x_i y_i \\
$$
Esempio:
$$
(1,2,-1,7)(-1,0,2,0) = 1(-1) + 2 * 0 + 2(-1) + 7 * 0 = -3
$$
Proprietà:
$$
xx = \sum^n_{i=1} xi xi = |x|^2 \ge 0 \\
xx = 0 \iff x = 0\ (tutte\ componenti\ 0) \\
x \cdot y = y \cdot x\ (simmetria) \\
x(y+z) = x \cdot y + x \cdot z \implies x \cdot (\lambda \cdot y) = \lambda \cdot x \cdot y
$$
Esempio di "trasformazione" da funzione a sistema lineare:
$$
2x + y = 0 \\
\binom 2 1 \binom x y = 0
$$
Altro (piano con punto all'origine):
$$
2x + 3y + z = 0 \\
\begin{bmatrix}
2 \\
3 \\
1
\end{bmatrix} 
\begin{bmatrix}
x \\
y \\
z
\end{bmatrix}
= 0
$$
Per definire le rette perpendicolari al piano si usa:
$$
<
\begin{bmatrix}
2 \\
3 \\
1
\end{bmatrix} 
>
$$
