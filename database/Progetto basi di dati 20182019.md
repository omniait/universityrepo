# Progetto basi di dati 2018/2019

## Prima overview

* Area “Allevamento”
* Area “Healthcare”
* Area “Produzione”
* Area “Soggiorno”
* Area “Store(?)”
* Area “Analytics”

Tutto ciò che è venduto è prodotto internamente.

## Allevamento

- [ ] Anagrafica animali

- [ ] Organizzazione stalle (?)

- [ ] Alimentazione

- [ ] Igiene 

- [ ] Parametri di locale (temperatura, quantità cibo mangiatoie, acqua)

- [ ] Riproduzione (possibili accoppiamenti, presenti e passati, gestito da dei veterinari)

- [ ] Pascolo (Esistono delle zone, nuova tabella zone)

- [ ] C’è da gestire anche delle allerte per quando l’acqua, la stalla è sporca o la mangiatoia è vuota

## Healthcare

- [ ] Monitoraggio salute

- [ ] Terapie

## Produzione

- [ ] Mungiture e stoccaggio

- [ ] Procedura di produzione (per esempio tipi di formaggi)

## Ecommerce/Store

- [ ] Accounting

- [ ] Acquisti

- [ ] Consegne e resi

- [ ] Recensioni

## Soggiorno

I clienti visitano quindi in modo turistico l’azienda

- [ ] Prenotazioni
- [ ] Escursioni

## Analytics

- [ ] Comportamento degli animali (tracciare quindi la salute degli animali)
- [ ] Controllo di qualità di processo (problemi sulla produzione)
- [ ] Tracciabilita di filiera (risalire da un prodotto ai suoi ingredienti)
- [ ] Analisi delle vendite (?)

# Lista delle fasi

- [ ] Analisi delle specifiche
- [ ] Progettazione concettuale/costruzione ER
- [ ] Ristrutturazione diagramma ER: eliminazioni attributi composti/multipli per costruire il modello logico
- [ ] Operazioni necessarie sui dati
- [ ] Traduzione in modello logico
- [ ] Normalizzazione
- [ ] Popolazione db
- [ ] Implementazione analytics